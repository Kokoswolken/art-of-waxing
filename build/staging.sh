#!/bin/bash -e

echo $S_HOST;
echo $S_USER;
echo $S_DIR;


rsync -qrbliP --exclude-from="./build/build-ignore.txt" --delete-after --log-file=deploy.log --backup-dir=backup_staging --rsh="sshpass -p $S_PASS ssh -o StrictHostKeyChecking=no -l $S_USER" * S_HOST:S_DIR