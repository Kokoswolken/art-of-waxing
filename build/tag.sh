#!/bin/bash -e

echo $P_HOST;
echo $P_USER;
echo $P_DIR;


rsync -qrbliP --exclude-from="./build/build-ignore.txt" --delete-after --log-file=deploy.log --backup-dir=backup_production --rsh="sshpass -p $P_PASS ssh -o StrictHostKeyChecking=no -l $P_USER" * P_HOST:P_DIR