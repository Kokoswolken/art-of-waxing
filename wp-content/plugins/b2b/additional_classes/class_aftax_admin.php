<?php
if (! defined('WPINC') ) {
	die;
}

if (! class_exists('AF_Tax_Exempt_Admin') ) {

	class AF_Tax_Exempt_Admin {
	


		public function __construct() {

			add_action('admin_enqueue_scripts', array( $this, 'aftax_admin_assets' ));

			add_filter('manage_users_columns', array( $this, 'aftax_new_modify_user_table' ));
			add_filter('manage_users_custom_column', array( $this, 'aftax_new_modify_user_table_row' ), 10, 3);
			add_action('show_user_profile', array( $this, 'aftax_show_status_field_profile' ));
			add_action('edit_user_profile', array( $this, 'aftax_show_status_field_profile' ));
			add_action('personal_options_update', array( $this, 'aftax_save_status_field_profile' ));
			add_action('edit_user_profile_update', array( $this, 'aftax_save_status_field_profile' ));
			add_action('woocommerce_admin_order_data_after_order_details', array( $this, 'aftax_display_order_data_in_admin' ));

			add_action('wp_ajax_aftaxsearchUsers', array( $this, 'aftaxsearchUsers' ));
		}

		public function aftax_admin_assets() {

			wp_enqueue_style('thickbox');
			wp_enqueue_script('thickbox');
			wp_enqueue_script('media-upload');
			wp_enqueue_media();
			wp_enqueue_style('aftax_adminc', plugins_url('../assets/css/aftax_admin.css', __FILE__), false, '1.0');
		}

		public function aftax_custom_menu_admin() {

			add_submenu_page('woocommerce', esc_html__('Tax Exemption', 'addify_b2b'), esc_html__('Tax Exemption', 'addify_b2b'), 'manage_options', 'addify-tax-exemption', array( $this, 'aftax_exempt_settings' ));

		}

		public function aftax_exempt_settings() {

			?>

			<div id="addify_settings_tabs">
				<div class="addify_setting_tab_ulli">
					<div class="addify-logo">
						<img src="<?php echo esc_url(AFB2B_URL . '/assets/images/addify-logo.png'); ?>" width="200">
						
					</div>

					<ul>
						<li><a href="#tabs-1"><span class="dashicons dashicons-admin-tools"></span><?php echo esc_html__('General Settings', 'addify_b2b'); ?></a></li>
						<li><a href="#tabs-2"><span class="dashicons dashicons-admin-tools"></span><?php echo esc_html__('Exempt Customers & Roles', 'addify_b2b'); ?></a></li>
						<li><a href="#tabs-3"><span class="dashicons dashicons-admin-tools"></span><?php echo esc_html__('Tax Exemption Request Settings', 'addify_b2b'); ?></a></li>
						<li><a href="#tabs-4"><span class="dashicons dashicons-admin-tools"></span><?php echo esc_html__('Email & Notification Settings', 'addify_b2b'); ?></a></li>
						<li><a href="#tabs-5"><span class="dashicons dashicons-admin-tools"></span><?php echo esc_html__('Guest Users Settings', 'addify_b2b'); ?></a></li>
					</ul>
				</div>

				<div class="addify-tabs-content">
					<form id="addify_setting_form" action="" method="post">
			<?php wp_nonce_field('aftax_nonce_action', 'aftax_nonce_field'); ?>
						<div class="addify-top-content">
							<h1><?php echo esc_html__('Addify Tax Exemption Module Settings', 'addify_b2b'); ?></h1>
							<p><em><?php echo esc_html__('Note: This module will work when you have enabled tax from your WooCommerce settings and apply tax on products.', 'addify_b2b'); ?></em></p>
						</div>

						<div class="addify-singletab" id="tabs-1">

							<h2><?php echo esc_html__('General settings for tax exemption', 'addify_b2b'); ?></h2>
							<p><?php echo esc_html__('In general settings you can set auto/manual tax exemption choose which field(s) you want to show on the tax exemption request form.', 'addify_b2b'); ?></p>
							<table class="addify-table-optoin">
								<tbody>
									<tr class="addify-option-field">
										<th>
											<div class="option-head">
												<h3><?php echo esc_html__('Remove Tax Automatically', 'addify_b2b'); ?></h3>
											</div>
										</th>
										<td>
											<input type="checkbox" name="aftax_enable_auto_tax_exempttion" id="aftax_enable_auto_tax_exempttion" value="yes" <?php echo checked('yes', esc_attr(get_option('aftax_enable_auto_tax_exempttion'))); ?> />
											<p><?php echo esc_html__('Disable tax for tax-exempted and approved users', 'addify_b2b'); ?></p>
										</td>
									</tr>

									<tr class="addify-option-field">
										<th>
											<div class="option-head">
												<h3><?php echo esc_html__('Enable Text Field', 'addify_b2b'); ?></h3>
											</div>
										</th>
										<td>
											<input type="checkbox" name="aftax_enable_text_field" id="aftax_enable_text_field" value="yes" <?php echo checked('yes', esc_attr(get_option('aftax_enable_text_field'))); ?> />
											<p><?php echo esc_html__('This text field will be shown in tax form in user my account page. This field can be used to collect name, tax id etc.', 'addify_b2b'); ?></p>
										</td>
									</tr>

									<tr class="addify-option-field">
										<th>
											<div class="option-head">
												<h3><?php echo esc_html__('Text Field Label', 'addify_b2b'); ?></h3>
											</div>
										</th>
										<td>
											<input type="text" class="afrfq_input_class" name="aftax_text_field_label" id="aftax_text_field_label" value="<?php echo esc_attr(get_option('aftax_text_field_label')); ?>" />
											<p><?php echo esc_html__('Label of text field.', 'addify_b2b'); ?></p>
										</td>
									</tr>

									<tr class="addify-option-field">
										<th>
											<div class="option-head">
												<h3><?php echo esc_html__('Enable Textarea Field', 'addify_b2b'); ?></h3>
											</div>
										</th>
										<td>
											<input type="checkbox" name="aftax_enable_textarea_field" id="aftax_enable_textarea_field" value="yes" <?php echo checked('yes', esc_attr(get_option('aftax_enable_textarea_field'))); ?> />
											<p><?php echo esc_html__('This textarea field will be shown in tax form in user my account page. This field can be used to collect additional info etc.', 'addify_b2b'); ?></p>
										</td>
									</tr>

									<tr class="addify-option-field">
										<th>
											<div class="option-head">
												<h3><?php echo esc_html__('Textarea Field Label', 'addify_b2b'); ?></h3>
											</div>
										</th>
										<td>
											<input type="text" class="afrfq_input_class" name="aftax_textarea_field_label" id="aftax_textarea_field_label" value="<?php echo esc_attr(get_option('aftax_textarea_field_label')); ?>" />
											<p><?php echo esc_html__('Label of textarea field.', 'addify_b2b'); ?></p>
										</td>
									</tr>

									<tr class="addify-option-field">
										<th>
											<div class="option-head">
												<h3><?php echo esc_html__('Enable File Upload Field', 'addify_b2b'); ?></h3>
											</div>
										</th>
										<td>
											<input type="checkbox" name="aftax_enable_fileupload_field" id="aftax_enable_fileupload_field" value="yes" <?php echo checked('yes', esc_attr(get_option('aftax_enable_fileupload_field'))); ?> />
											<p><?php echo esc_html__('This file upload field will be shown in tax form in user my account page. This field can be used to collect tax certificate etc.', 'addify_b2b'); ?></p>
										</td>
									</tr>

									<tr class="addify-option-field">
										<th>
											<div class="option-head">
												<h3><?php echo esc_html__('File Upload Field Label', 'addify_b2b'); ?></h3>
											</div>
										</th>
										<td>
											<input type="text" class="afrfq_input_class" name="aftax_fileupload_field_label" id="aftax_fileupload_field_label" value="<?php echo esc_attr(get_option('aftax_fileupload_field_label')); ?>" />
											<p><?php echo esc_html__('Label of file upload field.', 'addify_b2b'); ?></p>
										</td>
									</tr>

									<tr class="addify-option-field">
										<th>
											<div class="option-head">
												<h3><?php echo esc_html__('Allowed File Types', 'addify_b2b'); ?></h3>
											</div>
										</th>
										<td>
											<input type="text" class="afrfq_input_class" name="aftax_allowed_file_types" id="aftax_allowed_file_types" value="<?php echo esc_attr(get_option('aftax_allowed_file_types')); ?>" />
											<p><?php echo esc_html__('Specify allowed file types. Add comma(,) separated values like doc,pdf , etc. to allow multiple file types.', 'addify_b2b'); ?></p>
										</td>
									</tr>

								</tbody>
							</table>

						</div>
						<div class="addify-singletab" id="tabs-2">

							<h2><?php echo esc_html__('Exempt Customers & Roles', 'addify_b2b'); ?></h2>
							<p><?php echo esc_html__('In this section, you can specify the customers and user roles who are exempted from tax. These customers and roles are not required to fill the tax form from "My Account" page.', 'addify_b2b'); ?></p>
							<table class="addify-table-optoin">
								<tbody>
									<tr class="addify-option-field">
										<th>
											<div class="option-head">
												<h3><?php echo esc_html__('Choose Customers', 'addify_b2b'); ?></h3>
											</div>
										</th>
										<td>
											<select name="aftax_exempted_customers[]" id="aftax_exempted_customers" multiple="multiple">
												<?php
													$afcustomers = unserialize(get_option('aftax_exempted_customers'));

												if (! empty($afcustomers) ) {
													foreach ( $afcustomers as $usr ) {
														$author_obj = get_user_by('id', $usr);
														?>

												<option value="<?php echo intval($usr); ?>" selected="selected"><?php echo esc_attr($author_obj->display_name); ?>(<?php echo esc_attr($author_obj->user_email); ?>)</option>

														<?php
													}
												}
												?>

											</select>
											<p><?php echo esc_html__('Choose customers whom you want to give tax exemption.', 'addify_b2b'); ?></p>
										</td>
									</tr>


									<tr class="addify-option-field">
										<th>
											<div class="option-head">
												<h3><?php echo esc_html__('Select User Roles', 'addify_b2b'); ?></h3>
											</div>
										</th>
										<td>
											<div class="all_cats">
												<ul>

			<?php
			$aftax_exempted_user_roles = unserialize(get_option('aftax_exempted_user_roles'));
			global $wp_roles;
			$roles = $wp_roles->get_names();
			foreach ( $roles as $key => $value ) {
				?>

														<li class="par_cat">
															<input type="checkbox" class="parent" name="aftax_exempted_user_roles[]" id="aftax_exempted_user_roles" value="<?php echo esc_attr($key); ?>" 
				<?php
				if (! empty($aftax_exempted_user_roles) && in_array($key, $aftax_exempted_user_roles) ) {
					echo 'checked';
				}
				?>
															/>
				<?php echo esc_attr($value); ?>
														</li>

			<?php } ?>

												</ul>
											</div>
											<p><?php echo esc_html__('Choose user roles to grant them tax exemption status.', 'addify_b2b'); ?></p>
										</td>
									</tr>

								</tbody>
							</table>

						</div>
						<div class="addify-singletab" id="tabs-3">

							<h2><?php echo esc_html__('Tax Exemption Request Settings', 'addify_b2b'); ?></h2>
							<p><?php echo esc_html__('Select user roles for whom you want to display tax exemption form in "My Account" page and a checkbox in the checkout page to notify them that tax exemption is available.', 'addify_b2b'); ?></p>
							<table class="addify-table-optoin">
								<tbody>


								<tr class="addify-option-field">
									<th>
										<div class="option-head">
											<h3><?php echo esc_html__('Select User Roles', 'addify_b2b'); ?></h3>
										</div>
									</th>
									<td>
										<div class="all_cats">
											<ul>

			<?php
			$afuserroles = unserialize(get_option('aftax_requested_roles'));
			global $wp_roles;
			$roles = $wp_roles->get_names();
			foreach ( $roles as $key => $value ) {
				?>

													<li class="par_cat">
														<input type="checkbox" class="parent" name="aftax_requested_roles[]" id="aftax_requested_roles" value="<?php echo esc_attr($key); ?>" 
				<?php
				if (! empty($afuserroles) && in_array($key, $afuserroles) ) {
					   echo 'checked';
				}
				?>
														/>
				<?php echo esc_attr($value); ?>
													</li>

			<?php } ?>

											</ul>
										</div>
										
									</td>
								</tr>

								<tr class="addify-option-field">
									<th>
										<div class="option-head">
											<h3><?php echo esc_html__('Auto Approve Tax Exempt Request', 'addify_b2b'); ?></h3>
										</div>
									</th>
									<td>
										<input type="checkbox" name="aftax_enable_auto_tax_exempt" id="aftax_enable_auto_tax_exempt" value="yes" <?php echo checked('yes', esc_attr(get_option('aftax_enable_auto_tax_exempt'))); ?> />
										<p><?php echo esc_html__('If this option is checked then tax exempt requests will be auto-approved and users of above selected user roles will be eligible for tax exempt right after submitting the info.', 'addify_b2b'); ?></p>
									</td>
								</tr>

								<tr class="addify-option-field">
										<th>
											<div class="option-head">
												<h3><?php echo esc_html__('Show Tax Exemption Message on Checkout Page', 'addify_b2b'); ?></h3>
											</div>
										</th>
										<td>
											<input type="checkbox" name="aftax_enable_tax_exm_msg" id="aftax_enable_tax_exm_msg" value="yes" <?php echo checked('yes', esc_attr(get_option('aftax_enable_tax_exm_msg'))); ?> />
											<p><?php echo esc_html__('If this option is checked then a message will be displayed for the above selected user role users about tax exemption.', 'addify_b2b'); ?></p>
										</td>
									</tr>

									<tr class="addify-option-field">
										<th>
											<div class="option-head">
												<h3><?php echo esc_html__('Message Text', 'addify_b2b'); ?></h3>
											</div>
										</th>
										<td>
			<?php

			$content   = get_option('aftax_role_message_text');
			$editor_id = 'aftax_role_message_text';
			$settings  = array( 'textarea_name' => 'aftax_role_message_text' );

			wp_editor($content, $editor_id, $settings);

			?>
											<p><?php echo esc_html__('This will be visible for the user roles customer has selected above.', 'addify_b2b'); ?></p>
										</td>
									</tr>

								</tbody>
							</table>

						</div>

						<div class="addify-singletab" id="tabs-4">
							<h2><?php echo esc_html__('Email & Notification Settings', 'addify_b2b'); ?></h2>

							<table class="addify-table-optoin">
								<tbody>

								<tr class="addify-option-field">
									<th>
										<div class="option-head">
											<h3><?php echo esc_html__('Admin/Shop Manager Email', 'addify_b2b'); ?></h3>
										</div>
									</th>
									<td>
										<input type="email" class="afrfq_input_class" name="aftax_admin_email" id="aftax_admin_email" value="<?php echo esc_attr(get_option('aftax_admin_email')); ?>" />
										<p><?php echo esc_html__('All admin emails that are related to our module will be sent to this email address.', 'addify_b2b'); ?></p>
									</td>
								</tr>

								<tr class="addify-option-field">
									<th>
										<div class="option-head">
											<h3><?php echo esc_html__('Add/Update Tax Info Message', 'addify_b2b'); ?></h3>
										</div>
									</th>
									<td>
										<textarea class="afrfq_input_class" name="aftax_add_tax_info_message" id="aftax_add_tax_info_message" rows="7"><?php echo esc_attr(get_option('aftax_add_tax_info_message')); ?></textarea>
										<p><?php echo esc_html__('This message will be shown when user add or update tax info in my account.', 'addify_b2b'); ?></p>
									</td>
								</tr>

								<tr class="addify-option-field">
									<th>
										<div class="option-head">
											<h3><?php echo esc_html__('Admin Email Subject', 'addify_b2b'); ?></h3>
										</div>
									</th>
									<td>
										<input type="text" class="afrfq_input_class" name="aftax_admin_email_subject" id="aftax_admin_email_subject" value="<?php echo esc_attr(get_option('aftax_admin_email_subject')); ?>" />
										<p><?php echo esc_html__('This subject will be used when a user add or update tax info from my account.', 'addify_b2b'); ?></p>
									</td>
								</tr>

								<tr class="addify-option-field">
									<th>
										<div class="option-head">
											<h3><?php echo esc_html__('Admin Email Message', 'addify_b2b'); ?></h3>
										</div>
									</th>
									<td>
			<?php

			$content   = get_option('aftax_admin_email_message');
			$editor_id = 'aftax_admin_email_message';
			$settings  = array( 'textarea_name' => 'aftax_admin_email_message' );

			wp_editor($content, $editor_id, $settings);

			?>
										<p><?php echo esc_html__('This message will be used when a user add or update tax info from my account.', 'addify_b2b'); ?></p>
									</td>
								</tr>

								<tr>
									<td>
										<h3><?php echo esc_html__('Settings for Emails Sent to Customers', 'addify_b2b'); ?></h3>
									</td>
								</tr>

								<tr class="addify-option-field">
									<th>
										<div class="option-head">
											<h3><?php echo esc_html__('Approve Tax Info Email Subject', 'addify_b2b'); ?></h3>
										</div>
									</th>
									<td>
										<input type="text" class="afrfq_input_class" name="aftax_approve_tax_info_email_subject" id="aftax_approve_tax_info_email_subject" value="<?php echo esc_attr(get_option('aftax_approve_tax_info_email_subject')); ?>" />
										<p><?php echo esc_html__('This subject will be used when admin approves submitted tax info.', 'addify_b2b'); ?></p>
									</td>
								</tr>

								<tr class="addify-option-field">
									<th>
										<div class="option-head">
											<h3><?php echo esc_html__('Approve Tax Info Email Message', 'addify_b2b'); ?></h3>
										</div>
									</th>
									<td>
			<?php

			$content   = get_option('aftax_approve_tax_info_email_message');
			$editor_id = 'aftax_approve_tax_info_email_message';
			$settings  = array( 'textarea_name' => 'aftax_approve_tax_info_email_message' );

			wp_editor($content, $editor_id, $settings);

			?>
										<p><?php echo esc_html__('This message will be used when admin approves submitted tax info.', 'addify_b2b'); ?></p>
									</td>
								</tr>

								<tr class="addify-option-field">
									<th>
										<div class="option-head">
											<h3><?php echo esc_html__('Disapprove Tax Info Email Subject', 'addify_b2b'); ?></h3>
										</div>
									</th>
									<td>
										<input type="text" class="afrfq_input_class" name="aftax_disapprove_tax_info_email_subject" id="aftax_disapprove_tax_info_email_subject" value="<?php echo esc_attr(get_option('aftax_disapprove_tax_info_email_subject')); ?>" />
										<p><?php echo esc_html__('This subject will be used when admin disapprove submitted tax info.', 'addify_b2b'); ?></p>
									</td>
								</tr>

								<tr class="addify-option-field">
									<th>
										<div class="option-head">
											<h3><?php echo esc_html__('Disapprove Tax Info Email Message', 'addify_b2b'); ?></h3>
										</div>
									</th>
									<td>
			<?php

			$content   = get_option('aftax_disapprove_tax_info_email_message');
			$editor_id = 'aftax_disapprove_tax_info_email_message';
			$settings  = array( 'textarea_name' => 'aftax_disapprove_tax_info_email_message' );

			wp_editor($content, $editor_id, $settings);

			?>
										<p><?php echo esc_html__('This message will be used when admin disapprove submitted tax info.', 'addify_b2b'); ?></p>
									</td>
								</tr>


								</tbody>
							</table>

						</div>

						<div class="addify-singletab" id="tabs-5">
							<h2><?php echo esc_html__('Guest User Settings', 'addify_b2b'); ?></h2>
							<p><?php echo esc_html__('Show tax exemption message for guest users.', 'addify_b2b'); ?></p>
							<table class="addify-table-optoin">
								<tbody>
									<tr class="addify-option-field">
										<th>
											<div class="option-head">
												<h3><?php echo esc_html__('Show tax exemption message', 'addify_b2b'); ?></h3>
											</div>
										</th>
										<td>
											<input type="checkbox" name="aftax_enable_guest_message" id="aftax_enable_guest_message" value="yes" <?php echo checked('yes', esc_attr(get_option('aftax_enable_guest_message'))); ?> />
											<p><?php echo esc_html__('If this option is checked then a message will be displayed for guest user about tax exemption.', 'addify_b2b'); ?></p>
										</td>
									</tr>

									<tr class="addify-option-field">
										<th>
											<div class="option-head">
												<h3><?php echo esc_html__('Message Text', 'addify_b2b'); ?></h3>
											</div>
										</th>
										<td>
			<?php

			$content   = get_option('aftax_guest_message_text');
			$editor_id = 'aftax_guest_message_text';
			$settings  = array( 'textarea_name' => 'aftax_guest_message_text' );

			wp_editor($content, $editor_id, $settings);

			?>
											<p><?php echo esc_html__('This message will be displayed for guest users.', 'addify_b2b'); ?></p>
										</td>
									</tr>

								</tbody>
							</table>
						</div>

			<?php submit_button(esc_html__('Save Settings', 'addify_b2b'), 'primary', 'aftax_save_settings'); ?>

					</form>
				</div>

			</div>

			<?php
		}

		public function aftax_save_data() {

			global $wp;

			if (! empty($_REQUEST['aftax_nonce_field']) ) {

				$retrieved_nonce = sanitize_text_field($_REQUEST['aftax_nonce_field']);
			} else {
				$retrieved_nonce = 0;
			}

			if (! wp_verify_nonce($retrieved_nonce, 'aftax_nonce_action') ) {

				die('Failed security check');
			}

			if (! empty($_POST['aftax_enable_text_field']) ) {

				update_option('aftax_enable_text_field', sanitize_text_field($_POST['aftax_enable_text_field']));
			} else {
				update_option('aftax_enable_text_field', 'no');
			}
			if (isset($_POST['aftax_enable_auto_tax_exempttion']) && ! empty($_POST['aftax_enable_auto_tax_exempttion']) ) {

				update_option('aftax_enable_auto_tax_exempttion', sanitize_text_field($_POST['aftax_enable_auto_tax_exempttion']));
			} else {
				update_option('aftax_enable_auto_tax_exempttion', 'no');
			}

			if (isset($_POST['aftax_text_field_label']) ) {
				update_option('aftax_text_field_label', sanitize_text_field($_POST['aftax_text_field_label']));
			}

			if (! empty($_POST['aftax_enable_textarea_field']) ) {
				update_option('aftax_enable_textarea_field', sanitize_text_field($_POST['aftax_enable_textarea_field']));
			} else {
				update_option('aftax_enable_textarea_field', 'no');
			}

			if (isset($_POST['aftax_textarea_field_label']) ) {
				update_option('aftax_textarea_field_label', sanitize_text_field($_POST['aftax_textarea_field_label']));
			}

			if (! empty($_POST['aftax_enable_fileupload_field']) ) {
				update_option('aftax_enable_fileupload_field', sanitize_text_field($_POST['aftax_enable_fileupload_field']));
			} else {
				update_option('aftax_enable_fileupload_field', 'no');
			}

			if (isset($_POST['aftax_fileupload_field_label']) ) {
				update_option('aftax_fileupload_field_label', sanitize_text_field($_POST['aftax_fileupload_field_label']));
			}

			if (isset($_POST['aftax_exempted_customers']) ) {
				update_option('aftax_exempted_customers', serialize(sanitize_meta('aftax_exempted_customers', $_POST['aftax_exempted_customers'], '')));
			} else {
				update_option('aftax_exempted_customers', '');
			}

			if (isset($_POST['aftax_exempted_user_roles']) ) {
				update_option('aftax_exempted_user_roles', serialize(sanitize_meta('aftax_exempted_user_roles', $_POST['aftax_exempted_user_roles'], '')));
			} else {
				update_option('aftax_exempted_user_roles', '');
			}

			if (isset($_POST['aftax_requested_roles']) ) {
				update_option('aftax_requested_roles', serialize(sanitize_meta('aftax_requested_roles', $_POST['aftax_requested_roles'], '')));
			} else {
				update_option('aftax_requested_roles', '');
			}

			if (isset($_POST['aftax_allowed_file_types']) ) {
				update_option('aftax_allowed_file_types', sanitize_text_field($_POST['aftax_allowed_file_types']));
			}

			if (isset($_POST['aftax_add_tax_info_message']) ) {
				update_option('aftax_add_tax_info_message', sanitize_meta('aftax_add_tax_info_message', $_POST['aftax_add_tax_info_message'], ''));
			}

			if (isset($_POST['aftax_admin_email_subject']) ) {
				update_option('aftax_admin_email_subject', sanitize_text_field($_POST['aftax_admin_email_subject']));
			}

			if (isset($_POST['aftax_admin_email_message']) ) {
				update_option('aftax_admin_email_message', sanitize_meta('aftax_admin_email_message', $_POST['aftax_admin_email_message'], ''));
			}

			if (isset($_POST['aftax_approve_tax_info_email_subject']) ) {
				update_option('aftax_approve_tax_info_email_subject', sanitize_text_field($_POST['aftax_approve_tax_info_email_subject']));
			}

			if (isset($_POST['aftax_approve_tax_info_email_message']) ) {
				update_option('aftax_approve_tax_info_email_message', sanitize_meta('aftax_approve_tax_info_email_message', $_POST['aftax_approve_tax_info_email_message'], ''));
			}

			if (isset($_POST['aftax_disapprove_tax_info_email_subject']) ) {
				update_option('aftax_disapprove_tax_info_email_subject', sanitize_text_field($_POST['aftax_disapprove_tax_info_email_subject']));
			}

			if (isset($_POST['aftax_disapprove_tax_info_email_message']) ) {
				update_option('aftax_disapprove_tax_info_email_message', sanitize_meta('aftax_disapprove_tax_info_email_message', $_POST['aftax_disapprove_tax_info_email_message'], ''));
			}

			if (! empty($_POST['aftax_enable_guest_message']) ) {
				update_option('aftax_enable_guest_message', sanitize_text_field($_POST['aftax_enable_guest_message']));
			} else {
				update_option('aftax_enable_guest_message', 'no');
			}

			if (isset($_POST['aftax_guest_message_text']) ) {
				update_option('aftax_guest_message_text', sanitize_meta('aftax_guest_message_text', $_POST['aftax_guest_message_text'], ''));
			}

			if (! empty($_POST['aftax_enable_tax_exm_msg']) ) {
				update_option('aftax_enable_tax_exm_msg', sanitize_text_field($_POST['aftax_enable_tax_exm_msg']));
			} else {
				update_option('aftax_enable_tax_exm_msg', 'no');
			}

			if (isset($_POST['aftax_role_message_text']) ) {
				update_option('aftax_role_message_text', sanitize_meta('aftax_role_message_text', $_POST['aftax_role_message_text'], ''));
			}

			if (isset($_POST['aftax_admin_email']) ) {
				update_option('aftax_admin_email', sanitize_text_field($_POST['aftax_admin_email']));
			}

			if (! empty($_POST['aftax_enable_auto_tax_exempt']) ) {
				update_option('aftax_enable_auto_tax_exempt', sanitize_text_field($_POST['aftax_enable_auto_tax_exempt']));
			} else {
				update_option('aftax_enable_auto_tax_exempt', 'no');
			}

		}

		public function aftax_author_admin_notice() {
			?>
			<div class="updated notice notice-success is-dismissible">
				<p><?php echo esc_html__('Settings saved successfully.', 'addify_b2b'); ?></p>
			</div>
			<?php
		}


		public function aftax_new_modify_user_table( $column ) {

			$column['tax_exemption_status'] = esc_html__('Tax Exemption Status', 'addify_b2b');
			return $column;
		}

		public function aftax_new_modify_user_table_row( $val, $column_name, $user_id ) {
			$tax_status            = get_the_author_meta('aftax_tax_exemption_status', $user_id);
			$user_info             = get_userdata($user_id);
			$aftax_tax_expire_date = $user_info->aftax_tax_expire_date;
			$current_date          = gmdate('Y-m-d');
			if (! empty($aftax_tax_expire_date) ) {

				$exp_date = $aftax_tax_expire_date;
			} else {

				$exp_date = '';
			}

			if ('' == $exp_date && 'pending' != $tax_status && 'disapproved' != $tax_status && 'expired' != $tax_status ) {
				$tax_status = 'approved';
			} elseif ('' != $exp_date && $current_date > $exp_date ) {

				$tax_status = 'expired';
			} else {

				$tax_status = $user_info->aftax_tax_exemption_status;
			}

			switch ( $column_name ) {
				case 'tax_exemption_status':
					if (! empty(get_the_author_meta('aftax_tax_exemption_status', $user_id)) ) {
						if ('pending' == $tax_status ) {
							return "<span class='aftax_pending'>" . esc_attr($tax_status) . '</span>';
						} elseif ('approved' == $tax_status ) {
							return "<span class='aftax_approved'>" . esc_attr($tax_status) . '</span>';
						} elseif ('disapproved' == $tax_status ) {
							return "<span class='aftax_disapproved'>" . esc_attr($tax_status) . '</span>';
						} elseif ('expired' == $tax_status ) {
							return "<span class='aftax_expired'>" . esc_attr($tax_status) . '</span>';
						}
					}

					// no break
				default:
			}
			return $val;

		}

		public function aftax_show_status_field_profile( $user ) {
			wp_nonce_field('aftax_nonce_action', 'aftax_nonce_field');
			$user_info                  = get_userdata($user->ID);
			$aftax_text_field           = $user_info->aftax_text_field;
			$aftax_textarea_field       = $user_info->aftax_textarea_field;
			$aftax_fileupload_field     = $user_info->aftax_fileupload_field;
			$aftax_tax_exemption_status = $user_info->aftax_tax_exemption_status;
			$aftax_tax_expire_date      = $user_info->aftax_tax_expire_date;

			// fields
			$text_enable       = is_array(get_option('aftax_enable_text_field')) ? get_option('aftax_enable_text_field') : array();
			$textarea_enable   = is_array(get_option('aftax_enable_textarea_field')) ? get_option('aftax_enable_textarea_field') : array();
			$fileupload_enable = is_array(get_option('aftax_enable_fileupload_field')) ? get_option('aftax_enable_fileupload_field') : array();

			?>
			<h2><?php echo esc_html__('Tax Exempt', 'addify_b2b'); ?></h2>
			<table class="form-table">

			<?php if (in_array('enable', $text_enable) ) { ?>

					<tr>
						<th><label for="anu_additional_info"><?php echo esc_html__(esc_attr(get_option('aftax_text_field_label')), 'addify_b2b'); ?></label>
						</th>
						<td>

							<input type="text" name="aftax_text_field" id="aftax_text_field" class="regular-text" value="<?php echo esc_attr($aftax_text_field); ?>">

						</td>
					</tr>

			<?php } ?>

			<?php if (in_array('enable', $textarea_enable) ) { ?>

					<tr>
						<th><label for="anu_additional_info"><?php echo esc_html__(esc_attr(get_option('aftax_textarea_field_label')), 'addify_b2b'); ?></label>
						</th>
						<td>
							<textarea name="aftax_textarea_field" id="aftax_textarea_field" class="input-text" cols="5" rows="5" ><?php echo esc_attr($aftax_textarea_field); ?></textarea>
						</td>
					</tr>

			<?php } ?>

			<?php if (in_array('enable', $fileupload_enable) ) { ?>

					<tr>
						<th><label for="anu_additional_info"><?php echo esc_html__(esc_attr(get_option('aftax_fileupload_field_label')), 'addify_b2b'); ?></label>
						</th>
						<td>
							<input type="file" name="aftax_fileupload_field" id="aftax_fileupload_field">
							<small><?php echo esc_html__('Allowed file types:', 'addify_b2b') . ' ' . esc_attr(get_option('aftax_allowed_file_types')); ?></small>
							<input type="hidden" name="aftax_current_file" id="aftax_current_file" value="<?php echo esc_attr($aftax_fileupload_field); ?>">
						</td>
					</tr>

			<?php } ?>

			<?php if (! empty($aftax_fileupload_field) ) { ?>

					<tr>
						<th><label for="anu_additional_info"><?php echo esc_html__(esc_attr(get_option('aftax_fileupload_field_label') . ' Link'), 'addify_b2b'); ?></label>
						</th>
						<td>
							<span>
							<a target="_blank" href="<?php echo esc_url(AFTAX_MEDIA_URL . $aftax_fileupload_field); ?>"><?php echo esc_html__('Click here to view', 'addify_b2b'); ?></a>
						</span>
						</td>
					</tr>

			<?php } ?>

				<tr>
					<th><label for="anu_new_user_status"><?php echo esc_html__('Tax Exemption Status', 'addify_b2b'); ?></label>
					</th>
					<td>
						<select id="aftax_tax_exemption_status" name="aftax_tax_exemption_status">
							<option value=""><?php echo esc_html__('Select Tax Exemption Status', 'addify_b2b'); ?></option>
							<option value="approved" <?php echo selected('approved', esc_attr($aftax_tax_exemption_status)); ?>><?php echo esc_html__('Approved', 'addify_b2b'); ?></option>
							<option value="disapproved" <?php echo selected('disapproved', esc_attr($aftax_tax_exemption_status)); ?>><?php echo esc_html__('Disapproved', 'addify_b2b'); ?></option>
							<option value="expired" <?php echo selected('expired', esc_attr($aftax_tax_exemption_status)); ?>><?php echo esc_html__('Expired', 'addify_b2b'); ?></option>
						</select>

					</td>
				</tr>

				<tr>
					<th><label for="anu_new_user_status"><?php echo esc_html__('Tax Exemption Expire Date', 'addify_b2b'); ?></label>
					</th>
					<td>
						<input type="date" name="aftax_tax_expire_date" id="aftax_tax_expire_date" value="<?php echo esc_attr($aftax_tax_expire_date); ?>" />
					</td>
				</tr>

			</table>
			<?php
		}

		public function aftax_save_status_field_profile( $user_id ) {

			if (! empty($_REQUEST['aftax_nonce_field']) ) {

				$retrieved_nonce = sanitize_text_field($_REQUEST['aftax_nonce_field']);
			} else {
				$retrieved_nonce = 0;
			}

			if (! current_user_can('edit_user', $user_id) ) {
				return false;
			}

			if (! wp_verify_nonce($retrieved_nonce, 'aftax_nonce_action') ) {

				die('Failed security check');
			}

			if (isset($_POST['aftax_text_field']) && '' != $_POST['aftax_text_field'] ) {

				update_metadata('user', $user_id, 'aftax_text_field', sanitize_text_field($_POST['aftax_text_field']), '');
			}

			if (isset($_POST['aftax_textarea_field']) && '' != $_POST['aftax_textarea_field'] ) {

				update_metadata('user', $user_id, 'aftax_textarea_field', sanitize_text_field($_POST['aftax_textarea_field']), '');
			}

			if ( !empty( $_FILES['aftax_fileupload_field']['name'] ) && current_user_can('upload_files') ) {

				if (! empty($_FILES['aftax_fileupload_field']['name']) ) {
					$ffname = sanitize_text_field($_FILES['aftax_fileupload_field']['name']);
				} else {
					$ffname = '';
				}

				if (! empty($_FILES['aftax_fileupload_field']['tmp_name']) ) {
					$ftempname = sanitize_text_field($_FILES['aftax_fileupload_field']['tmp_name']);
				} else {
					$ftempname = '';
				}

				$file          = time() . $ffname;
				$target_path   = AFTAX_MEDIA_PATH . $file;
				$allowed_types = explode(',', esc_attr(get_option('aftax_allowed_file_types')));
				$file_type     = wp_check_filetype( basename( $file['name'] ) );
				$ext           = isset( $file_type['ext'] ) ? $file_type['ext'] : '';

				if (in_array($ext, $allowed_types) ) {
					$temp = move_uploaded_file($ftempname, $target_path);
					update_metadata('user', $user_id, 'aftax_fileupload_field', $file, '');

				} else {

					$this->aftax_errors()->add('aftax_fileupload_field_error', esc_html__('This file type is not allowed.', 'addify_b2b'));

					return;
				}
			} else {

				if (! empty($_POST['aftax_current_file']) ) {
					$curr_fiel = sanitize_text_field($_POST['aftax_current_file']);
				} else {
					$curr_fiel = '';
				}

				update_metadata('user', $user_id, 'aftax_fileupload_field', $curr_fiel, '');
			}

			$user_info                  = get_userdata($user_id);
			$aftax_tax_exemption_status = $user_info->aftax_tax_exemption_status;

			if (! empty($_POST['aftax_tax_exemption_status']) && $aftax_tax_exemption_status != $_POST['aftax_tax_exemption_status'] ) {
				if ('approved' == $_POST['aftax_tax_exemption_status'] ) {

					// Send email to user to inform that tax status is approved.
					$user       = new WP_User($user_id);
					$user_login = stripslashes($user->data->user_login);
					$user_email = stripslashes($user->data->user_email);

					$to      = $user_email;
					$subject = get_option('aftax_approve_tax_info_email_subject');
					$message = get_option('aftax_approve_tax_info_email_message');

					// Always set content-type when sending HTML email
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type:text/html;charset=UTF-8' . "\r\n";

					$admin_email = get_option('admin_email');
					if (empty($admin_email) ) {
						if (! empty($_SERVER['SERVER_NAME']) ) {
							$server_name = sanitize_text_field($_SERVER['SERVER_NAME']);
						} else {
							$server_name = '';
						}
						$admin_email = 'support@' . $server_name;
					}

					$fromname = get_option('blogname');

					// More headers
					$headers .= 'From: ' . $fromname . ' < ' . $admin_email . ' > ' . "\r\n";

					wp_mail($to, $subject, $message, $headers);
				}

				if ('disapproved' == $_POST['aftax_tax_exemption_status'] ) {

					// Send email to user to inform that tax status is disapproved.
					$user       = new WP_User($user_id);
					$user_login = stripslashes($user->data->user_login);
					$user_email = stripslashes($user->data->user_email);

					$to      = $user_email;
					$subject = get_option('aftax_disapprove_tax_info_email_subject');
					$message = get_option('aftax_disapprove_tax_info_email_message');

					// Always set content-type when sending HTML email
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type:text/html;charset=UTF-8' . "\r\n";

					$admin_email = get_option('admin_email');
					if (empty($admin_email) ) {
						if (! empty($_SERVER['SERVER_NAME']) ) {
							$server_name = sanitize_text_field($_SERVER['SERVER_NAME']);
						} else {
							$server_name = '';
						}
						$admin_email = 'support@' . $server_name;
					}

					$fromname = get_option('blogname');

					// More headers
					$headers .= 'From: ' . $fromname . ' < ' . $admin_email . ' > ' . "\r\n";

					wp_mail($to, $subject, $message, $headers);
				}
			}

			if (! empty($_POST['aftax_tax_exemption_status']) ) {
				update_metadata('user', $user_id, 'aftax_tax_exemption_status', sanitize_text_field($_POST['aftax_tax_exemption_status']), '');
			} else {
				update_metadata('user', $user_id, 'aftax_tax_exemption_status', '', '');
			}

			if (! empty($_POST['aftax_tax_expire_date']) ) {
				update_metadata('user', $user_id, 'aftax_tax_expire_date', sanitize_text_field($_POST['aftax_tax_expire_date']), '');
			} else {
				update_metadata('user', $user_id, 'aftax_tax_expire_date', '', '');
			}

		}

		public function aftax_errors() {
			static $wp_error; // Will hold global variable safely

			$wp_error = new WP_Error(null, null, null);

			return $wp_error;
		}

		public function aftax_show_error_messages() {
			$codess = $this->aftax_errors();
			if (! empty($codess) ) {

				$codes = $codess->get_error_codes();
			} else {
				$codes = '';
			}
			if (! empty($codes) ) {
				// Loop error codes and display errors
				foreach ( $codes as $code ) {
					if ('aftax_success_m' == $code ) {
						echo '<ul class="woocommerce-info">';
						$message = $this->aftax_errors()->get_error_message($code);
						echo '<li>' . esc_attr($message) . '</li>';
						echo '</ul>';

					} else {
						echo '<ul class="woocommerce-error">';
						$message = $this->aftax_errors()->get_error_message($code);
						echo '<li>' . esc_attr($message) . '</li>';
						echo '</ul>';

					}
				}
			}
		}

		public function aftax_display_order_data_in_admin( $order ) {
			?>
			<?php
			if ('Yes' == esc_attr(get_post_meta($order->get_id(), 'tax_exemption_checkbox', true)) ) {
				$uploaded_file = get_post_meta($order->get_id(), 'aftax_fileupload_field', true);
				?>
				<div class="order_data_column">
					<h4><?php echo esc_html__('Tax Exempt', 'addify_b2b'); ?></h4>
					<div class="address aftax_order_details">
						<p>
							<strong><?php echo esc_html__('Is Tax Exempt?', 'addify_b2b'); ?></strong>
				<?php echo esc_html__('Yes ', 'addify_b2b'); ?>
						</p>
						<p>
							<strong><?php echo esc_html__(esc_attr(get_option('aftax_text_field_label')), 'addify_b2b'); ?></strong>
				<?php echo esc_attr(get_post_meta($order->get_id(), 'aftax_text_field', true)); ?>
						</p>
						<p>
							<strong><?php echo esc_html__(esc_attr(get_option('aftax_textarea_field_label')), 'addify_b2b'); ?></strong>
				<?php echo esc_attr(get_post_meta($order->get_id(), 'aftax_textarea_field', true)); ?>
						</p>
						<p>
							<strong><?php echo esc_html__(esc_attr(get_option('aftax_fileupload_field_label') . ' Link'), 'addify_b2b'); ?></strong>
				<?php
				if (! empty($uploaded_file) ) {
					?>
								<a target="_blank" href="<?php echo esc_url(AFTAX_MEDIA_URL . $uploaded_file); ?>"><?php echo esc_html__('Click here to view', 'addify_b2b'); ?></a>
						  <?php
				} else {
					?>
								<p><?php echo esc_html__('No file has been uploaded', 'addify_b2b'); ?></p>
					<?php
				}
				?>
						</p>
					</div>

				</div>
				<?php
			}
		}


		public function aftaxsearchUsers() {

			$nonce = isset($_POST['nonce']) ? sanitize_text_field($_POST['nonce']) : '';

			if (! wp_verify_nonce($nonce, 'aftax-ajax-nonce') ) {
				die('Failed ajax security check!');
			}

			$search = isset($_POST['q']) ? sanitize_text_field($_POST['q']) : '';

			$data_array  = array();
			$users       = new WP_User_Query(
				array(
				'search'         => '*' . esc_attr($search) . '*',
				'search_columns' => array(
						'user_login',
						'user_nicename',
						'user_email',
						'user_url',
				),
				)
			);
			$users_found = $users->get_results();

			if (! empty($users_found) ) {

				foreach ( $users_found as $user ) {

					$title        = $user->display_name . '(' . $user->user_email . ')';
					$data_array[] = array( $user->ID, $title ); // array( User ID, User name and email )
				}
			}

			echo json_encode($data_array);

			die();

		}

	}

	new AF_Tax_Exempt_Admin();

}

