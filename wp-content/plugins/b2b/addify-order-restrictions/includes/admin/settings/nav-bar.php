<?php

defined('ABSPATH') || exit;

?>
<div class="wrap">
	<h2><?php echo esc_html__('Order Restriction Settings', 'addify_b2b'); ?></h2>
	<?php settings_errors(); ?> 

	<h2 class="nav-tab-wrapper">  
	
		<a href="?page=addify-b2b" class="nav-tab <?php echo esc_attr($active_tab) == '' ? 'nav-tab-active' : ''; ?>"><?php echo esc_html__('Settings', 'addify_b2b'); ?></a>

		<a href="<?php echo esc_url( admin_url( 'edit.php?post_type=af_order_rule' ) ); ?>" class="nav-tab"><?php echo esc_html__(' Restriction Rules', 'addify_b2b'); ?></a>
	</h2>

	<form method="post" action="options.php" class="afb2b_options_form">
		<?php
			settings_fields('addify-order-restrictions-fields');
			do_settings_sections('addify-or-general');
		?>
		<?php submit_button(); ?>
	</form>
</div>
