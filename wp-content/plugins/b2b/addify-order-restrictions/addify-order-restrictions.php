<?php
/**
 * Plugin Name: WooCommerce Order Restrictions
 * Plugin URI: http://www.addifypro.com
 * Description: Restrict your orders.
 * Version: 1.0.0
 * Author: Addify
 * Author URI: http://www.addifypro.com
 * Text Domain: addify_or
 * Domain Path: /languages/
 * Requires at least: 5.3
 * Requires PHP: 7.0
 *
 * @package addify-order-restrictions
 */

defined( 'ABSPATH' ) || exit;

// Check the installation of WooCommerce module if it is not a multi site.
if ( ! is_multisite() ) {

	if ( ! in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ), true ) ) {

		function afrfb_admin_notice() {

			// Deactivate the plugin.
			deactivate_plugins( __FILE__ );

			?>
			<div id="message" class="error">
				<p>
					<strong> 
						<?php esc_html_e( 'WooCommerce Order Restrictions plugin is inactive. WooCommerce plugin must be active in order to activate it.', 'addify_b2b'); ?>
					</strong>
				</p>
			</div>;
			<?php
		}

		add_action( 'admin_notices', 'afrfb_admin_notice' );
	}
}

if ( ! defined( 'AFOR_URL' ) ) {
	define( 'AFOR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'AFOR_PLUGIN_DIR' ) ) {
	define( 'AFOR_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}

// Include the main WooCommerce Registration form Builder class.
if ( ! class_exists( 'AF_Order_Restrictions', false ) ) {
	include_once AFOR_PLUGIN_DIR . 'includes/class-af-order-restrictions.php';
}
