��    |     �      �      �     �     �     �               !     3     P     R     Y     g     t     }     �     �     �     �     �     �       
        %  
   5     @     X  	   u       S   �  �   �     }  S  �     �       \        m  "   �     �     �     �     �          )     I     M     ^     k       <   �  <   �  <   
  1   G  /   y  ;   �  1   �  .     /   F  :   v     �  5   �     �                 (      ;   
   J      U      ^      m      {      �      �      �   (   �   n   �   !   J!  !   l!     �!     �!  ?   �!  B   �!     B"  	   Z"     d"     y"     �"     �"     �"     �"  6   �"  6   #  6   B#  '   y#  $   �#     �#  (   �#     $     $  1   +$     ]$     s$  /   �$     �$     �$     �$     �$  ;   �$     8%  :   Q%     �%  1   �%     �%  .   �%     &  /   '&     W&     i&     &     �&     �&     �&  4   �&  1   '  $   G'  C   l'  (   �'  ?   �'  N   (  �   h(  K   )  !   [)  �  })  �   �*  -   �+  �   �+  7   8,     p,     �,  '   �,     �,     �,  /   �,     �,     -     %-  �   =-     �-     �-     �-     .     .     $.     4.  
   O.     Z.     t.     z.     �.     �.  
   �.     �.  _   �.  ^   /  v   }/  �   �/     �0  �   �0  �   F1     �1     2     )2  ,   <2  2   i2     �2     �2     �2     �2  &   �2  &   3  &   :3     a3     }3  %   �3     �3     �3     �3  3   4  +   @4      l4  !   �4  )   �4  6   �4  k   5  �   |5     ?6     G6     ]6     k6  )   x6  7   �6  -   �6  6   7     ?7     G7  
   ]7     h7     7     �7     �7     �7     �7  $   �7     8     8     *8     =8     F8     Z8     n8  �   z8  �   $9     �9     �9     :      :     (:     4:  �   H:  �   �:     s;     |;     �;     �;     �;     �;     �;     �;     �;     �;     �;  "   <      <<     ]<     r<     y<     �<     �<  C   �<  ;   =  b   B=     �=     �=     �=     �=     �=     �=     >  .   $>  &   S>     z>     �>     �>     �>     �>     �>     �>     ?     &?     =?     L?     _?      x?  !   �?     �?     �?     �?  	   �?     �?     �?     @  
   @     #@  �   5@  %   �@      A     A  &   (A     OA     hA  ?   zA  9   �A  +   �A  !    B  W   BB  ~   �B  �   C     �C     �C     �C     �C  +   �C     �C  +   D  	   ED     OD  !   XD  
   zD  +   �D  +   �D  +   �D      	E     *E  *   IE      tE     �E     �E     �E     �E  
   �E     �E     F  �   F  H   �F  �    G  �   �G  s   H  q   �H  d    I  j   eI  i   �I  i   :J  �   �J  �   uK  g   L  J   xL  /   �L  J   �L  J   >M  M   �M  A   �M  C   N  M   ]N  A   �N  C   �N  M   1O  v   O  J   �O  }   AP  �   �P  D   CQ  3   �Q  �   �Q  O   AR  L   �R  .   �R  >   S     LS  	   fS     pS     �S     �S     �S  
   �S     �S  	   �S     �S     �S     �S     T  H   7T     �T     �T  &   �T     �T     �T     �T      U  5   U     OU     \U  �  sU     W     W     3W     KW     aW     cW  (   xW     �W     �W     �W     �W     �W     �W     �W     X      X  %   @X     fX     �X  +   �X     �X     �X     �X     Y  >   +Y     jY     zY  j   �Y  �    Z  ,   �Z  �  �Z     �\     �\  �   �\     n]  3   �]  &   �]  2   �]  9   ^     Q^     i^  =   ^     �^     �^     �^     �^     �^  C   _  C   U_  C   �_  9   �_  7   `  N   O`  ;   �`  6   �`  9   a  Q   Ka     �a  H   �a     �a     b  !   b  $   =b     bb     {b     �b     �b     �b     �b     �b     �b     �b  7   �b  �   /c  H   �c  5   d     Sd     pd  Y   �d  V   �d      =e     ^e     ne     �e     �e     �e     �e     �e  B   �e  B   <f  B   f  9   �f  8   �f     5g  7   Rg     �g     �g  ;   �g     �g     h  9   h     Uh     eh     uh     �h  Q   �h     �h  D   i     Xi  =   oi     �i  :   �i     �i  <   j     Oj     gj     j  /   �j     �j     �j  K   �j  J   Fk  -   �k  W   �k  9   l  G   Ql  h   �l  �   m  T   �m  (   n  �  Cn  �   �o  9   �p  �   �p  C   �q     �q     r  5   #r     Yr     ar  @   ir     �r  $   �r  $   �r  �   s     �s     �s     �s  	   �s     �s     t  $   t     At      Vt     wt     t     �t  )   �t     �t     �t  k   �t  o   Yu  �   �u  �   Wv     >w  �   Ow  �   x  -   �x     �x     y  I   %y  Q   oy  	   �y  -   �y     �y     z  )   7z  (   az  (   �z     �z     �z  1   �z     {     ?{     Z{  A   x{  =   �{  (   �{  .   !|  5   P|  E   �|  �   �|  �   h}     X~      e~     �~     �~  6   �~  O   �~  @   0  F   q     �     �  
   �     �     �     �  (   "�  (   K�  %   t�  1   ��     ̀  "   �     �  
   �     (�     @�     T�  �   d�  �   2�  !   	�     +�     I�  	   _�     i�     y�  �   ��  �   F�  	   �     ��     �     �     �  '   -�     U�     r�     ��  &   ��  (   ͅ  1   ��  -   (�     V�  -   r�  -   ��  !   Ά  '   ��  f   �  U   �  �   Շ     _�     p�     ��     ��  
   ��  !   ��     �  H   ��  ?   B�      ��      ��     ĉ  $   ܉     �      �     9�  !   R�  !   t�     ��     ��  "   ��  '   �  *   �     7�     G�     _�     u�     ��     ��     ��     ��     ǋ  �   ۋ  /   Č     �     �  &   '�      N�     o�  1   ��  >   ��  %   ��  $   �  |   B�  �   ��  �   B�     �     �     *�  	   6�  C   @�  &   ��  @   ��     �     ��  *   �     7�  *   E�  *   p�  *   ��     Ƒ     �  3   �  !   8�     Z�     w�     ��     ��     ��     ƒ     ��  �   ��  n   ̓  �   ;�  �   �  �   �  �   f�  �   �  {   v�  �   �  �   ��  �   �  �   ��  y   ך  o   Q�  <   ��  <   ��  r   ;�  q   ��  d    �  h   ��  o   �  b   ^�  f   ��  n   (�  �   ��  l   M�  �   ��  �   z�  N   $�  A   s�  �   ��  _   f�  O   ƣ  >   �     U�  1   a�     ��     ��     ¤     ܤ  
   �     �  $   ��     #�     5�     P�  -   n�  )   ��  ^   ƥ     %�     '�  /   +�  '   [�     ��  $   ��     ��  5   ٦     �      �    View Quote  is a required field!  items in quote  was placed on  # ---Choose Menu--- ---Select Adjustment Type--- : Action Add New Field Add New Rule Add Rule Addify Additional Fields Section Title Adjustment Type Admin Email Address Admin Email Message Admin Email Subject Admin Email Text Admin/Shop Manager Email All Fields All Quote Rules All Quotes All Registration Fields All Role Based Pricing Rules All Rules All Submitted Quotes All admin emails that are related to our module will be sent to this email address. All admin emails that are related to our module will be sent to this email address. If this email is empty then default admin email address is used. Allow Search Engines to Index Allow search engines to crawl and index hidden products, categories and other pages. While using global option when you hide products from guest users they will stay hidden for search engines as well i.e. Google won’t be able to rank those pages in search results. Please check this box if you want Google to crawl and rank hidden pages. Allowed File Types Allowed Types Allowed file upload types. e.g (png,jpg,txt). Add comma separated, please do not use dot(.). Apply on All Products Approve New User Messages Settings Approve New User Settings Approve Tax Info Email Message Approve Tax Info Email Subject Approved Email Subject Approved Email Text Auto Approve Tax Exempt Request B2B B2B Registration B2B Settings B2B for WooCommerce Captcha Settings Check if you want to make Additional Field 1 field required. Check if you want to make Additional Field 2 field required. Check if you want to make Additional Field 3 field required. Check if you want to make Company field required. Check if you want to make Email field required. Check if you want to make File/Image Upload field required. Check if you want to make Message field required. Check if you want to make Name field required. Check if you want to make Phone field required. Check this if you want to apply this rule on all products. Choose Customers Choose user roles to grant them tax exemption status. Click to View Company Field Custom Button Label Custom Button Link Custom Message Custom URL Customer Customer Email Customer Name Customers and Roles Date Date Published Default Fields Default Fields for Registration Settings Disable this checkbox will show a checkbox in the checkout page to notify them that tax exemption is available Disapprove Tax Info Email Message Disapprove Tax Info Email Subject Disapproved Email Subject Disapproved Email Text Display the above label on custom button, e.g "Request a Quote" Display the above text when price is hidden, e.g "Price is hidden" Edit Registration Field Edit Rule Email & Notification Email & Notification Settings Email Field Email Response Text Email Settings Email Subject Enable Additional Field 1 on the Request a Quote Form. Enable Additional Field 2 on the Request a Quote Form. Enable Additional Field 3 on the Request a Quote Form. Enable Ajax add to Quote (Product Page) Enable Ajax add to Quote (Shop Page) Enable Approve New User Enable Approve New User at Checkout Page Enable Captcha Enable Company Field Enable Company field on the Request a Quote Form. Enable Default Fields Enable Email Field Enable Email field on the Request a Quote Form. Enable Field 1 Enable Field 2 Enable Field 3 Enable File Upload Field Enable File/Image Upload field on the Request a Quote Form. Enable Global Visibility Enable Google reCaptcha field on the Request a Quote Form. Enable Message Field Enable Message field on the Request a Quote Form. Enable Name Field Enable Name field on the Request a Quote Form. Enable Phone Field Enable Phone field on the Request a Quote Form. Enable Text Field Enable Textarea Field Enable User Role Selection Enable admin email notification Enable for Guest Enable for this Role Enable or Disable Ajax add to quote on product page. Enable or Disable Ajax add to quote on shop page. Enable or Disable global visibility. Enable or Disable new user notification to admin from this module.  Enable or Disable quote for guest users. Enable or Disable welcome email notification from this module.  Enable this checkbox will disable tax for all tax-exempted and approved users. Enable this if you want to include a copy of quote details in the email sent to customer. The quote details will be embedded along the with the above email body text. Enable this if you want to send email to customer after submitting a Quote. Enable welcome email notification Enable/Disable Approve new user at the Checkout page. If you enable it, the order of the customer with registration will be placed and pending status is assigned to the user. Once the user logout from the site, he will not able to log in again until the administrator approves the user. If you disable it, the user will be approved automatically when registered from the checkout page. Enable/Disable Approve new user. When this option is enabled all new registered users will be set to Pending until admin approves Enable/Disable Default Fields of WooCommerce. Enable/Disable User Role selection on registration page. If this is enable then a user role dropdown will be shown on registration page. Enable/Disable tax for tax-exempted and approved users. Exclude User Roles Exemption Request Failed! Unable to process your request. Field 1 Field 2 Field label for user role selection select box. Fields Settings File Upload Field Label File/Image Upload Field First message that will be displayed to user when he/she completes the registration process, this message will be displayed only when manual approval is required.  Fixed Decrease Fixed Increase Fixed Price General General Settings Global Settings Global Visibility Settings Go to shop Google reCaptcha Settings Guest Guest Users Hide Hide Add to Cart Button Hide Price Hide Price Text If more than one rule is applied on same customer then rule that is added last will be applied. If this option is checked then a message will be displayed for guest user about tax exemption. If this option is checked then a message will be displayed for the above selected user role users about tax exemption. If this option is checked then tax exempt requests will be auto-approved and users of above selected user roles will be eligible for tax exempt right after submitting the info. Important Notes: In general settings you can enable/disable tax for specific users and choose which field(s) you want to show on the tax exemption request form. In this section, you can specify the customers and user roles who are exempted from tax. These customers and roles are not required to fill the tax form from "My Account" page. Include Copy of Quote in Email Invalid file type! Invalid reCaptcha! Keep Add to Cart and add a new custom button Keep Add to Cart button and add a new Quote Button Label Label of file upload field. Label of text field. Label of textarea field. Label of the Additional Field 1 field. Label of the Additional Field 2 field. Label of the Additional Field 3 field. Label of the Company field. Label of the Email field. Label of the File/Image Upload field. Label of the Message field. Label of the Name field. Label of the Phone field. Link for custom button e.g "http://www.example.com" Manage Approve new user settings from here. Manage Email Settings from here. Manage Google reCaptcha settings. Manage module general settings from here. Manage registration module general settings from here. Manage the redirect to quote page after Add to Quote and redirect to any page after quote form submission . Manage user role settings from here. Choose whether you want to show user role dropdown on registration page or not and choose which user roles you want to show in dropdown on registration page. Max Qty Max Qty Error Message Message Field Message Text Message for Users when Account is Created Message for Users when Account is Disapproved By Admin. Message for Users when Account is disapproved Message for Users when Account is pending for approval Min Qty Min Qty Error Message Name Field New Registration Field New Rule No No products in quote basket. No quote has been made yet. No registration field found No registration field found in trash No rule found No rule found in trash Non LoggedIn/Guest Payments Percentage Decrease Percentage Increase Phone Field Please note that Visibility by User Roles have high priority. If configurations are active for any user role – the global settings won’t work for that specific role. Please note that Visibility by User Roles have high priority. If following configurations are active for any user role – the global settings won’t work for that specific role. Price Specific to a Customer Price Specific to a Role Pricing Priority: Product Product SKU Products Visibility Provide number between 0 and 100, If more than one rules are applied on same item then rule with higher priority will be applied. 1 is high and 100 is low. Provide value from high priority 1 to Low priority 10. If more than one rule are applied on same item rule with high priority will be applied. Quantity Quote Quote  Quote # Quote #: Quote Basket Placement Quote Basket Style Quote Details Quote Info: Quote Rule for Guest Users Quote Rule for Registered Users Quote Rule for Selected Categories Quote Rule for Selected Products Quote for User Roles Quotes Redirect After Quote Submission Redirect Settings Redirect to Quote Page Redirect to Quote page after a product added to Quote successfully. Redirect to any page after Quote is submitted successfully. Redirect to this custom URL when user try to access restricted catalog. e.g http://www.example.com Redirection Mode Registration Field Registration Fields Regular Product Price Remove Remove Tax Automatically Remove this item Replace Add to Cart button with a Quote Button Replace Add to Cart with custom button Replace Original Price? Replace Orignal Price? Request a Quote Request a Quote Form Fields Request a Quote Rules Request a quote! Request for Quote Request for Quote Rules Request for QuotevRule Return To Shop Role Based Pricing Role Based Pricing Rules Role Based Pricing(By Customers) Role Based Pricing(By User Roles) Rule Details Rule Priority Rule Settings Rule Type SKU Search Registration Field Search Rule Secret Key Select Categories Select Menu where you want to show Mini Quote Basket. If there is no menu then you have to create menu in WordPress menus otherwise mini quote basket will not show. Select Payement Method for User Roles Select Payment Methods: Select Products Select Shipping Methods for User Roles Select Shipping Methods: Select User Roles Select categories on which products on which you want to apply. Select either you want to show products or hide products. Select products on which you want to apply. Select the design of Quote Basket Select user roles for whom you want to display tax exemption form in "My Account" page. Select which user roles users you want to exclude from manual approval. These user roles users will be automatically approved. Select which user roles you want to show in dropdown on registration page. Note: Administrator role is not available for show in dropdown. Send Email to Customer Settings Shipping Show Show Tax Exemption Message on Checkout Page Show tax exemption message Show tax exemption message for guest users. Show/Hide Site Key Sorry, your nonce did not verify. Sort Order Sort Order of the Additional Field 1 field. Sort Order of the Additional Field 2 field. Sort Order of the Additional Field 3 field. Sort Order of the Company field. Sort Order of the Email field. Sort Order of the File/Image Upload field. Sort Order of the Message field. Sort Order of the Name field. Sort Order of the Phone field. Submit Success Message Tax-Exempt Text Field Label Textarea Field Label This email address will be used for getting new user email notification for admin, if this is empty then default WordPress admin email address will be used. This email subject is used when new user notification is sent to admin.  This email text will be used when new user notification is sent to admin. If Approve new user is active then you can write text about new user approval. This file upload field will be shown in tax form in user my account page. This field can be used to collect tax certificate etc. This is Google reCaptcha secret key, you can get this from Google. Without this key Google reCaptcha will not work. This is Google reCaptcha site key, you can get this from Google. Without this key Google reCaptcha will not work. This is the approved email message, this message is used when account is approved by administrator.  This is the approved email subject, this subject is when used when account is approved by administrator.   This is the disapproved email message, this message is used when account is disapproved by administrator. This is the disapproved email subject, this subject is used when account is disapproved by administrator. This is the email body; when a new customer registers this email be automatically sent and the custom fields will be included in that email. This body text will be included along with the default fields data. This is the email subject; this subject is used when the email is sent to the user on account creation to include fields data and manual approval message. This is the title for the section where additional fields are displayed on front end registration form. This message will appear on quote submission page, when user submit quote. This message will be displayed for guest users. This message will be displayed when user try to access restricted catalog. This message will be shown when user add or update tax info in my account. This message will be used when a user add or update tax info from my account. This message will be used when admin approves submitted tax info. This message will be used when admin disapprove submitted tax info. This subject will be used when a user add or update tax info from my account. This subject will be used when admin approves submitted tax info. This subject will be used when admin disapprove submitted tax info. This subject will be used when email is sent to user when quote is submitted. This text field will be shown in tax form in user my account page. This field can be used to collect name, tax id etc. This text will be used when email is sent to user when quote is submitted. This textarea field will be shown in tax form in user my account page. This field can be used to collect additional info etc. This will be displayed when user will attempt to login after registration and his/her account is still pending for admin approval.  This will be visible for the user roles customer has selected above. This will help you to add fields on the quote form. This will help you to hide price and add to cart and show request a quote button for selected user roles including the guests users. This will help you to show or hide products for all customers including guests. This will only work for Fixed Price, Fixed Decrease and Percentage Decrease. URL of Page to Redirect after Quote Submission URL of page to redirect after Quote is submitted successfully. Update Cart Error Message User Role User Role Field Label User Role Settings Value View View Quote View Registration Field View Rule View this item Visibility By User Roles Welcome/Pending Email Body Text Welcome/Pending Email Subject WooCommerce B2b Plugin. (PLEASE TAKE BACKUP BEFORE UPDATING THE PLUGIN). X Yes You have recieved a new quote request. Your quote is currently empty. af-product-visibility has been added to your quote. http://www.addifypro.com https://woocommerce.com/products/b2b-for-woocommerce/ is Required? reCaptcha is required! Project-Id-Version: WooCommerce B2B Plugin
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-08-26 06:36+0000
PO-Revision-Date: 2020-08-27 11:42+0000
Last-Translator: 
Language-Team: Italian
Language: it_IT
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.2; wp-5.5 Vedi preventivo è un campo obbligatorio!
 articoli in preventivo
 è stato posizionato
 # ---Menu di scelta--- --- Seleziona il tipo di regolazione --- : Aziona Aggiungi nuovo campo Aggiungi nuova regola Aggiungi regola Addify Titolo sezione campi aggiuntivi Tipo di regolazione Indirizzo e-mail amministratore Messaggio e-mail dell'amministratore
 Amministratore Oggetto email Testo e-mail amministratore
 Email amministratore / responsabile negozio Tutti i campi Tutte le regole di citazione
 Tutte le citazioni Tutti i campi di registrazione
 Tutte le regole di determinazione del prezzo basate sui ruoli
 Tutte le regole Tutte le citazioni inviate Tutte le e-mail di amministrazione correlate al nostro modulo verranno inviate a questo indirizzo e-mail.  Tutte le e-mail di amministrazione correlate al nostro modulo verranno inviate a questo indirizzo e-mail. Se questa e-mail è vuota, viene utilizzato l'indirizzo e-mail amministratore predefinito.  Consenti ai motori di ricerca di indicizzare Consenti ai motori di ricerca di eseguire la scansione e l'indicizzazione di prodotti, categorie e altre pagine nascoste. Quando si utilizza l'opzione globale quando si nascondono i prodotti dagli utenti ospiti, questi rimarranno nascosti anche per i motori di ricerca, ad esempio Google non sarà in grado di classificare tali pagine nei risultati di ricerca. Seleziona questa casella se desideri che Google esegua la scansione e classifichi le pagine nascoste. Tipi di file consentiti  Tipi consentiti Tipi di caricamento file consentiti. ad esempio (png, jpg, txt). Aggiungi una virgola di separazione, non utilizzare il punto (.). Applica a tutti i prodotti Approvare le impostazioni dei nuovi messaggi utente Approvare le nuove impostazioni utente Approva messaggio e-mail con informazioni fiscali
 Approva l'oggetto dell'email con le informazioni fiscali
 Oggetto email approvato Testo email approvato Approvazione automatica della richiesta di esenzione fiscale
 B2B Registrazione B2B
 Impostazioni B2B
 B2B per WooCommerce
 Impostazioni captcha
 Controlla se vuoi rendere obbligatorio il campo Campo aggiuntivo 1. Controlla se vuoi rendere obbligatorio il campo Campo aggiuntivo 2. Controlla se vuoi rendere obbligatorio il campo Campo aggiuntivo 3. Controlla se vuoi rendere obbligatorio il campo Azienda.
 Controlla se vuoi rendere obbligatorio il campo Email.
 Controlla se vuoi rendere obbligatorio il campo Caricamento file / immagini.

 Controlla se vuoi rendere obbligatorio il campo Messaggio.
 Controlla se vuoi rendere obbligatorio il campo Nome.
 Controlla se vuoi rendere obbligatorio il campo Telefono. Seleziona questa opzione se desideri applicare questa regola su tutti i prodotti. Scegli clienti
 Scegli i ruoli utente per concedere loro lo stato di esenzione fiscale.
 Clicca per vedere Campo dell'azienda Etichetta pulsante personalizzata Collegamento pulsante personalizzato Messaggio personalizzato URL personalizzato Cliente
 Email cliente
 Nome del cliente Clienti e ruoli
 Data	 Data di pubblicazione Campi predefiniti
 Campi predefiniti per le impostazioni di registrazione
 Disabilita questa casella di controllo verrà visualizzata una casella di controllo nella pagina di pagamento per informarli che è disponibile l'esenzione fiscale
 Messaggio e-mail per la mancata approvazione delle informazioni fiscali
 Oggetto email con informazioni fiscali non approvate
 Oggetto email non approvato
 Testo e-mail non approvato
 Visualizza l'etichetta sopra sul pulsante personalizzato, ad es. "Richiedi un preventivo" Visualizza il testo sopra quando il prezzo è nascosto, ad es. "Il prezzo è nascosto" Modifica campo di registrazione
 Modifica regola Notifica per email
 Impostazioni email e notifiche
 Campo email

 Testo di risposta via email
 Impostazioni dell 'email Oggetto dell'email Abilitare il campo aggiuntivo 1 nel modulo Richiedi un preventivo. Abilitare il campo aggiuntivo 2 nel modulo Richiedi un preventivo. Abilitare il campo aggiuntivo 3 nel modulo Richiedi un preventivo. Abilita Ajax Aggiungi al preventivo (Pagina del prodotto) Abilita Ajax Aggiungi al preventivo (Pagina del negozio) Abilita Approva nuovo utente Abilita Approva nuovo utente nella pagina di pagamento
 Abilita Captcha Abilita campo azienda Abilita il campo Azienda nel modulo Richiedi un preventivo. Abilita campi predefiniti Abilita campo e-mail Abilita il campo Email nel modulo Richiedi un preventivo. Abilita campo 1 Abilita campo 2 Abilita campo 3 Abilita campo caricamento file Abilitare il campo Caricamento file / immagini nel modulo Richiedi un preventivo. Abilita visibilità globale
 Abilita il campo Google reCaptcha nel modulo Richiedi un preventivo. Abilita campo messaggi Abilita il campo Messaggio nel modulo Richiedi un preventivo. Abilita campo Nome Abilitare il campo Nome nel modulo Richiedi un preventivo. Abilita campo telefono Abilita il campo Telefono nel modulo Richiedi un preventivo. Abilita campo di testo
 Abilita campo Textarea
 Abilita selezione ruolo utente Abilita la notifica e-mail dell'amministratore
 Abilita per Ospite Abilita per questo ruolo Abilita o disabilita Ajax Aggiungi al preventivo nella pagina del prodotto. Abilita o disabilita Ajax Aggiungi al preventivo nella pagina del negozio. Abilita o disabilita la visibilità globale.
 Abilita o disabilita la notifica del nuovo utente all'amministratore da questo modulo.
 Abilita o disabilita il preventivo per gli utenti ospiti. Abilita o disabilita la notifica e-mail di benvenuto da questo modulo.
 Abilitare questa casella di controllo disabiliterà le tasse per tutti gli utenti esentati e approvati.
 Abilitalo se desideri includere una copia dei dettagli del preventivo nell'e-mail inviata al cliente. I dettagli del preventivo verranno incorporati insieme al testo del corpo dell'email sopra.
 Abilitalo se desideri inviare un'e-mail al cliente dopo aver inviato un preventivo.
 Abilita la notifica e-mail di benvenuto
 Abilita / Disabilita Approva nuovo utente nella pagina Checkout. Se lo abiliti, verrà effettuato l'ordine del cliente con registrazione e all'utente verrà assegnato lo stato in sospeso. Una volta che l'utente si disconnette dal sito, non sarà in grado di accedere nuovamente fino a quando l'amministratore non avrà approvato l'utente. Se lo disabiliti, l'utente verrà approvato automaticamente quando registrato dalla pagina di checkout.
 Abilita / Disabilita Approvazione nuovo utente. Quando questa opzione è abilitata, tutti i nuovi utenti registrati verranno impostati su In sospeso fino all'approvazione dell'amministratore Abilita / Disabilita i campi predefiniti di WooCommerce.
 Abilita / Disabilita la selezione del ruolo utente nella pagina di registrazione. Se è abilitato, verrà visualizzato un menu a discesa del ruolo utente nella pagina di registrazione.
 Abilita / disabilita le tasse per gli utenti esentati e approvati.
 Escludi ruoli utente Richiesta di esenzione
 Non riuscito! Impossibile elaborare la tua richiesta. Campo 1 Campo 2 Casella di selezione etichetta campo per selezione ruolo utente. Impostazioni dei campi Etichetta campo di caricamento file
 Campo di caricamento file / immagini Primo messaggio che verrà mostrato all'utente al termine del processo di registrazione, questo messaggio verrà visualizzato solo quando è richiesta l'approvazione manuale.
 Riduzione fissa Aumento fisso Prezzo fisso Generale
 Impostazioni generali Impostazioni globali
 Impostazioni di visibilità globale
 Vai a fare la spesa
 Impostazioni di Google reCaptcha ospite
 Utenti ospiti
 Nascondere
 Nascondi il pulsante Aggiungi al carrello Nascondi il prezzo Nascondi testo prezzo Se viene applicata più di una regola sullo stesso cliente, verrà applicata la regola aggiunta per ultima. Se questa opzione è selezionata, verrà visualizzato un messaggio per l'utente ospite sull'esenzione fiscale.
 Se questa opzione è selezionata, verrà visualizzato un messaggio per gli utenti con ruolo utente sopra selezionato sull'esenzione fiscale.
 Se questa opzione è selezionata, le richieste di esenzione fiscale verranno approvate automaticamente e gli utenti dei ruoli utente selezionati sopra saranno idonei all'esenzione fiscale subito dopo aver inviato le informazioni.
 Note importanti: Nelle impostazioni generali è possibile abilitare / disabilitare le tasse per utenti specifici e scegliere quali campi visualizzare nel modulo di richiesta di esenzione fiscale.
 In questa sezione è possibile specificare i clienti e i ruoli utente esentati dalle tasse. Questi clienti e ruoli non sono tenuti a compilare il modulo fiscale dalla pagina "Account personale".
 Includi una copia del preventivo nell'e-mail
 Tipo di file non valido!
 ReCaptcha non valido! Mantieni Aggiungi al carrello e aggiungi un nuovo pulsante personalizzato Mantieni il pulsante Aggiungi al carrello e aggiungi un nuovo pulsante Preventivo Etichetta Etichetta del campo di caricamento del file.
 Etichetta del campo di testo.
 Etichetta del campo textarea.
 Etichetta del campo Campo aggiuntivo 1.

 Etichetta del campo Campo aggiuntivo 2.
 Etichetta del campo Campo aggiuntivo 3.
 Etichetta del campo Azienda.
 Etichetta del campo Email.
 Etichetta del campo Caricamento file / immagine.
 Etichetta del campo Messaggio.
 Etichetta del campo Nome.
 Etichetta del campo Telefono. Link per pulsante personalizzato, ad es. "Http://www.example.com" Gestisci Approvazione delle nuove impostazioni utente da qui. Gestisci le impostazioni e-mail da qui.
 Gestisci le impostazioni di Google reCaptcha.
 Gestisci le impostazioni generali del modulo da qui.
 Gestisci le impostazioni generali del modulo di registrazione da qui. Gestisci il reindirizzamento alla pagina del preventivo dopo Aggiungi a preventivo e reindirizza a qualsiasi pagina dopo l'invio del modulo di preventivo.
 Gestisci le impostazioni del ruolo utente da qui. Scegli dove vuoi mostrare il menu a discesa del ruolo utente nella pagina di registrazione o meno e scegli quali ruoli utente vuoi mostrare nel menu a discesa nella pagina di registrazione. Qtà massima Messaggio di errore Qtà massima Campo messaggi Messaggio di testo
 Messaggio per gli utenti quando viene creato l'account Messaggio per gli utenti quando l'account non è approvato dall'amministratore. Messaggio per gli utenti quando l'account non è stato approvato Messaggio per gli utenti quando l'account è in attesa di approvazione La mia quantità
 Messaggio di errore Qtà minima Nome campo Nuovo campo di registrazione Nuova regola No Nessun prodotto nel carrello preventivo. Nessun preventivo è stato ancora fatto. Nessun campo di registrazione trovato Nessun campo di registrazione trovato nel cestino Nessuna regola trovata Nessuna regola trovata nel cestino Non registrato / Guest Pagamenti
 Diminuzione percentuale Aumento percentuale Campo telefono
 Tieni presente che la visibilità per ruoli utente ha la massima priorità. Se le configurazioni sono attive per qualsiasi ruolo utente, le impostazioni globali non funzioneranno per quel ruolo specifico.
 Tieni presente che la visibilità per ruoli utente ha la massima priorità. Se le seguenti configurazioni sono attive per qualsiasi ruolo utente, le impostazioni globali non funzioneranno per quel ruolo specifico.
 Prezzo specifico per un cliente

 Prezzo specifico per un ruolo Priorità dei prezzi: Prodotto
 Codice prodotto Visibilità dei prodotti
 Fornire un numero compreso tra 0 e 100. Se più di una regola viene applicata allo stesso articolo, verrà applicata la regola con priorità più alta. 1 è alto e 100 è basso.
 Fornire un valore da priorità alta 1 a priorità bassa 10. Se più di una regola viene applicata allo stesso elemento, verrà applicata la regola con priorità alta.
 Quantità Preventivo
 Preventivo
 Preventivo #
 Preventivo #:
 Posizionamento del carrello preventivi
 Citare lo stile del cestino
 Dettagli preventivo
 Informazioni sulle quotazioni: Regola di preventivo per utenti ospiti Regola preventivo per utenti registrati
 Regola di preventivo per le categorie selezionate Regola di preventivo per prodotti selezionati Citazione per ruoli utente
 Regola di preventivo per prodotti selezionati Reindirizzamento dopo l'invio del preventivo
 Impostazioni di reindirizzamento
 Reindirizza alla pagina del preventivo
 Reindirizza alla pagina Preventivo dopo che un prodotto è stato aggiunto correttamente a Preventivo.
 Reindirizza a qualsiasi pagina dopo che il preventivo è stato inviato con successo.
 Reindirizza a questo URL personalizzato quando l'utente tenta di accedere al catalogo con restrizioni. ad esempio http://www.example.com
 reindirizzamento Campo di registrazione Campi di registrazione Regular Product Price Rimuovere
 Rimuovi automaticamente le tasse
 Rimuovi questo articolo Sostituisci il pulsante Aggiungi al carrello con un pulsante preventivo
 Sostituisci Aggiungi al carrello con il pulsante personalizzato Sostituire il prezzo originale?
 Sostituire il prezzo originale?
 Richiedi un preventivo
 Campi modulo Richiedi un preventivo
 Richiedi un preventivo Regole
 Richiedi un preventivo!
 Richiesta di preventivo
 Richiesta di regole di preventivo Richiesta di regole di preventivo Ritorna al negozio Prezzi basati sul ruolo Regole dei prezzi basate sul ruolo Prezzi basati sui ruoli (per cliente)

 Prezzi basati sui ruoli (per ruoli utente) Dettagli regola Priorità alle regole

 Salva le impostazioni Tipo di regola
 SKU Cerca campo di registrazione Regola di ricerca Chiave segreta Seleziona categorie Selezionare il menu in cui si desidera mostrare il Mini carrello delle quotazioni. Se non è presente alcun menu, è necessario creare un menu nei menu di WordPress, altrimenti il mini cestino dei preventivi non verrà visualizzato. Seleziona Metodo di pagamento per ruoli utente
 Seleziona metodi di pagamento:
 Seleziona prodotti Select Shipping Methods for User Roles Seleziona metodi di spedizione:
 Seleziona ruoli utente Seleziona le categorie dei prodotti che vuoi.

	
 Seleziona se vuoi mostrare i prodotti o nascondere i prodotti. Seleziona i prodotti che desideri.
	
 Seleziona il design di Quote Basket
 Seleziona i ruoli utente per i quali desideri visualizzare il modulo di esenzione fiscale nella pagina "Account personale".
 Seleziona quali ruoli utente desideri escludere dall'approvazione manuale. Questi ruoli utente verranno automaticamente approvati. Seleziona i ruoli utente che desideri mostrare nel menu a discesa nella pagina di registrazione. Nota: il ruolo di amministratore non è disponibile per la visualizzazione nel menu a discesa.
 Invia e-mail al cliente
 impostazioni
 spedizione
 Mostrare
 Mostra il messaggio di esenzione fiscale nella pagina di pagamento
 Mostra messaggio di esenzione fiscale
 Mostra il messaggio di esenzione fiscale per gli utenti ospiti.
 Mostra/Nascondi Chiave del sito Spiacenti, il tuo nonce non è verificato. Ordinamento

 Ordinamento del campo Campo aggiuntivo 1.
 Ordinamento del campo Campo aggiuntivo 2.
 Ordinamento del campo Campo aggiuntivo 3.
 Ordinamento del campo Azienda.
 Ordinamento del campo Email.
 Ordinamento del campo Caricamento file / immagini.
 Ordinamento del campo Messaggio.
 Ordinamento del campo Nome.
 Ordinamento del campo Telefono. Invia Messaggio di successo Esente da imposte
 Etichetta campo di testo
 Etichetta campo di testo
 Questo indirizzo e-mail verrà utilizzato per ricevere la notifica e-mail del nuovo utente per l'amministratore, se è vuoto, verrà utilizzato l'indirizzo e-mail dell'amministratore di WordPress predefinito.
 Questo oggetto e-mail viene utilizzato quando la notifica di un nuovo utente viene inviata all'amministratore. Questo testo di posta elettronica verrà utilizzato quando la notifica di un nuovo utente viene inviata all'amministratore. Se Approva nuovo utente è attivo, è possibile scrivere un testo sulla nuova approvazione dell'utente. Questo campo di caricamento del file verrà mostrato nel modulo fiscale nella pagina del mio account dell'utente. Questo campo può essere utilizzato per raccogliere il certificato fiscale, ecc.
 Questa è la chiave segreta di Google reCaptcha, puoi ottenerla da Google. Senza questa chiave, Google reCaptcha non funzionerà.
 Questa è la chiave del sito di Google reCaptcha, puoi ottenerla da Google. Senza questa chiave, Google reCaptcha non funzionerà.
 Questo è il messaggio di posta elettronica approvato, questo messaggio viene utilizzato quando l'account è approvato dall'amministratore. Questo è l'oggetto e-mail approvato, questo oggetto viene utilizzato quando l'account è approvato dall'amministratore.
	
 Questo è il messaggio di posta elettronica non approvato, questo messaggio viene utilizzato quando l'account non è approvato dall'amministratore. Questo è l'oggetto e-mail non approvato, questo oggetto viene utilizzato quando l'account non è approvato dall'amministratore. Questo è il corpo dell'email; quando un nuovo cliente registra questa e-mail verrà automaticamente inviata e i campi personalizzati saranno inclusi in quella e-mail. Questo testo del corpo verrà incluso insieme ai dati dei campi predefiniti. Questo è l'oggetto dell'e-mail; questo argomento viene utilizzato quando l'e-mail viene inviata all'utente al momento della creazione dell'account per includere i dati dei campi e il messaggio di approvazione manuale. Questo è il titolo della sezione in cui vengono visualizzati campi aggiuntivi nel modulo di registrazione del front-end. Questo messaggio verrà visualizzato nella pagina di invio del preventivo, quando l'utente invia il preventivo. Questo messaggio verrà visualizzato per gli utenti ospiti.
 Questo messaggio verrà visualizzato per gli utenti ospiti.
 Questo messaggio verrà visualizzato quando l'utente aggiunge o aggiorna le informazioni fiscali nel mio account.
 Questo messaggio verrà utilizzato quando un utente aggiunge o aggiorna le informazioni fiscali dal mio account.
 Questo messaggio verrà utilizzato quando l'amministratore approva le informazioni fiscali inviate.
 Questo messaggio verrà utilizzato quando l'amministratore non approva le informazioni fiscali inviate.
 Questo oggetto verrà utilizzato quando un utente aggiunge o aggiorna le informazioni fiscali dal mio account.
 Questo oggetto verrà utilizzato quando l'amministratore approva le informazioni fiscali inviate.
 Questo oggetto verrà utilizzato quando l'amministratore non approva le informazioni fiscali inviate.
 Questo oggetto verrà utilizzato quando l'e-mail viene inviata all'utente quando viene inviato il preventivo.
 Questo campo di testo verrà visualizzato nel modulo fiscale nella pagina del mio account dell'utente. Questo campo può essere utilizzato per raccogliere nome, codice fiscale ecc.
 Questo testo verrà utilizzato quando l'e-mail viene inviata all'utente quando viene inviato il preventivo.
 Questo campo dell'area di testo verrà mostrato nel modulo fiscale nella pagina del mio account dell'utente. Questo campo può essere utilizzato per raccogliere informazioni aggiuntive, ecc.
 Questo verrà visualizzato quando l'utente tenterà di accedere dopo la registrazione e il suo account è ancora in attesa di approvazione da parte dell'amministratore.
 Questo sarà visibile per i ruoli utente che il cliente ha selezionato sopra.
 Questo ti aiuterà ad aggiungere campi nel modulo di preventivo.
 Questo ti aiuterà a nascondere il prezzo e aggiungere al carrello e mostrare il pulsante di richiesta di preventivo per i ruoli utente selezionati, inclusi gli utenti ospiti.
 Questo ti aiuterà a mostrare o nascondere i prodotti per tutti i clienti, inclusi gli ospiti.
 Funzionerà solo per Prezzo fisso, Diminuzione fissa e Diminuzione percentuale. URL della pagina da reindirizzare dopo l'invio del preventivo
 preventivo  Messaggio di errore del carrello di aggiornamento Ruolo dell'utente Etichetta campo ruolo utente Impostazioni ruolo utente Valore Visualizza Vedi preventivo Visualizza il campo di registrazione Visualizza regola Visualizza questo articolo Visibilità per ruoli utente
 Testo corpo e-mail di benvenuto / in attesa

 Oggetto e-mail di benvenuto / in sospeso
 Plugin B2b per WooCommerce. (SI PREGA DI EFFETTUARE IL BACKUP PRIMA DI AGGIORNARE IL PLUGIN).
 X sì Hai ricevuto una nuova richiesta di preventivo. Il tuo preventivo è attualmente vuoto. visibilità prodotto-af è stato aggiunto al tuo preventivo. http://www.addifypro.com https://woocommerce.com/products/b2b-for-woocommerce/ è obbligatorio? reCaptcha è richiesto! 