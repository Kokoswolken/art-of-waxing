��    |     �      �      �     �     �     �               !     3     P     R     Y     g     t     }     �     �     �     �     �     �       
        %  
   5     @     X  	   u       S   �  �   �     }  S  �     �       \        m  "   �     �     �     �     �          )     I     M     ^     k       <   �  <   �  <   
  1   G  /   y  ;   �  1   �  .     /   F  :   v     �  5   �     �                 (      ;   
   J      U      ^      m      {      �      �      �   (   �   n   �   !   J!  !   l!     �!     �!  ?   �!  B   �!     B"  	   Z"     d"     y"     �"     �"     �"     �"  6   �"  6   #  6   B#  '   y#  $   �#     �#  (   �#     $     $  1   +$     ]$     s$  /   �$     �$     �$     �$     �$  ;   �$     8%  :   Q%     �%  1   �%     �%  .   �%     &  /   '&     W&     i&     &     �&     �&     �&  4   �&  1   '  $   G'  C   l'  (   �'  ?   �'  N   (  �   h(  K   )  !   [)  �  })  �   �*  -   �+  �   �+  7   8,     p,     �,  '   �,     �,     �,  /   �,     �,     -     %-  �   =-     �-     �-     �-     .     .     $.     4.  
   O.     Z.     t.     z.     �.     �.  
   �.     �.  _   �.  ^   /  v   }/  �   �/     �0  �   �0  �   F1     �1     2     )2  ,   <2  2   i2     �2     �2     �2     �2  &   �2  &   3  &   :3     a3     }3  %   �3     �3     �3     �3  3   4  +   @4      l4  !   �4  )   �4  6   �4  k   5  �   |5     ?6     G6     ]6     k6  )   x6  7   �6  -   �6  6   7     ?7     G7  
   ]7     h7     7     �7     �7     �7     �7  $   �7     8     8     *8     =8     F8     Z8     n8  �   z8  �   $9     �9     �9     :      :     (:     4:  �   H:  �   �:     s;     |;     �;     �;     �;     �;     �;     �;     �;     �;     �;  "   <      <<     ]<     r<     y<     �<     �<  C   �<  ;   =  b   B=     �=     �=     �=     �=     �=     �=     >  .   $>  &   S>     z>     �>     �>     �>     �>     �>     �>     ?     &?     =?     L?     _?      x?  !   �?     �?     �?     �?  	   �?     �?     �?     @  
   @     #@  �   5@  %   �@      A     A  &   (A     OA     hA  ?   zA  9   �A  +   �A  !    B  W   BB  ~   �B  �   C     �C     �C     �C     �C  +   �C     �C  +   D  	   ED     OD  !   XD  
   zD  +   �D  +   �D  +   �D      	E     *E  *   IE      tE     �E     �E     �E     �E  
   �E     �E     F  �   F  H   �F  �    G  �   �G  s   H  q   �H  d    I  j   eI  i   �I  i   :J  �   �J  �   uK  g   L  J   xL  /   �L  J   �L  J   >M  M   �M  A   �M  C   N  M   ]N  A   �N  C   �N  M   1O  v   O  J   �O  }   AP  �   �P  D   CQ  3   �Q  �   �Q  O   AR  L   �R  .   �R  >   S     LS  	   fS     pS     �S     �S     �S  
   �S     �S  	   �S     �S     �S     �S     T  H   7T     �T     �T  &   �T     �T     �T     �T      U  5   U     OU     \U  �  sU     W      W     ;W     VW     gW     iW  $   }W     �W     �W     �W     �W     �W     �W  )   �W     X  3   "X  1   VX  0   �X  /   �X  <   �X     &Y      8Y     YY     pY  -   �Y     �Y     �Y  �   �Y    wZ  -   y[  �  �[     N]     `]  n   q]     �]  5   �]  '   5^  ?   ]^  ;   �^  &   �^  %    _  ?   &_     f_     j_     x_     �_     �_  I   �_  I   `  I   P`  =   �`  H   �`  N   !a  <   pa  ;   �a  =   �a  :   'b     bb  L   rb     �b     �b      �b     c     "c     8c     Jc     Sc     sc     �c     �c     �c     �c  :   �c  �   d  @   �d  <   �d  '   /e  &   We  a   ~e  ^   �e     ?f     Xf  %   ef  7   �f     �f  *   �f      g     ,g  J   >g  I   �g  I   �g  ?   h  >   ]h     �h  5   �h     �h     i  G   !i      ii  &   �i  S   �i     j     j     +j  %   ?j  Z   ej      �j  P   �j     2k  G   Mk     �k  G   �k     �k  I   l     bl     |l  ,   �l  E   �l     m     $m  E   <m  D   �m  ,   �m  ^   �m  =   Sn  ^   �n  ~   �n  �   oo  d   ap  A   �p  �  q  �   �r  @   fs  �   �s  S   dt     �t     �t  ,   �t  	   u      u  P   (u  =   yu  '   �u  "   �u  �   v     �v     �v     �v     �v     �v     �v  %   w     *w  "   :w     ]w     fw  	   yw  #   �w     �w     �w  g   �w  q   7x  �   �x    Gy     Zz  �   mz  �   /{  4   |     ;|     W|  C   l|  O   �|      }  )   	}     3}     Q}  +   o}  *   �}  *   �}     �}  (   ~  .   7~     f~     �~     �~  G   �~  +     A   1  2   s  >   �  I   �  �   /�  �   ۀ     ؁  %   �     �     !�  2   3�  O   f�  8   ��  I   �     9�  %   K�     q�     ��     ��     ��  .   ��  &   ؃  #   ��  0   #�     T�  .   t�     ��     ��     ��     ܄     �  �   �  �   �  "   ц     �     �  	   '�     1�     ?�  �   Y�  �   ��  
   ��     ��     ��     ��     ��  '   ��     �     �     "�  -   @�  /   n�  4   ��  2   Ӊ  "   �     )�  0   /�  %   `�  &   ��  s   ��  V   !�  �   x�     ��     �     +�     @�  	   ]�  $   g�     ��  D   ��  9   �     "�     A�     ]�  2   s�  !   ��     ȍ     �  "   ��  "   �     A�     T�  "   o�  +   ��  /   ��     �     �     �     1�     ?�     C�     \�     p�     �  �   ��  8   u�     ��     ͐  5   �      �     :�  7   W�  :   ��  2   ʑ  &   ��  ~   $�  �   ��  �   *�  %   �     
�     �     #�  @   ,�  *   m�  C   ��     ܔ     �  (   ��     %�  9   >�  9   x�  9   ��  +   �  7   �  =   P�  +   ��  *   ��  -   �     �     �     -�     B�     ^�    z�  t   �  �   ��  �   ͙  y   ��  {    �  r   |�  {   �  w   k�  r   �  !  V�  �   x�  l   K�  h   ��  4   !�  V   V�  e   ��  f   �  [   z�  ^   ֡  e   5�  Z   ��  ]   ��  k   T�  �   ��  h   ~�  �   �  �   ��  [   6�  C   ��  �   ֦  `   ��  W   �  K   G�  Y   ��  ,   ��     �  #   *�      N�     o�     v�     {�     ��  	   ��     ��      ��  D   ߩ  4   $�  g   Y�     ��     ê  ,   Ǫ  (   ��     �  "   <�     _�     x�     ��     ��    View Quote  is a required field!  items in quote  was placed on  # ---Choose Menu--- ---Select Adjustment Type--- : Action Add New Field Add New Rule Add Rule Addify Additional Fields Section Title Adjustment Type Admin Email Address Admin Email Message Admin Email Subject Admin Email Text Admin/Shop Manager Email All Fields All Quote Rules All Quotes All Registration Fields All Role Based Pricing Rules All Rules All Submitted Quotes All admin emails that are related to our module will be sent to this email address. All admin emails that are related to our module will be sent to this email address. If this email is empty then default admin email address is used. Allow Search Engines to Index Allow search engines to crawl and index hidden products, categories and other pages. While using global option when you hide products from guest users they will stay hidden for search engines as well i.e. Google won’t be able to rank those pages in search results. Please check this box if you want Google to crawl and rank hidden pages. Allowed File Types Allowed Types Allowed file upload types. e.g (png,jpg,txt). Add comma separated, please do not use dot(.). Apply on All Products Approve New User Messages Settings Approve New User Settings Approve Tax Info Email Message Approve Tax Info Email Subject Approved Email Subject Approved Email Text Auto Approve Tax Exempt Request B2B B2B Registration B2B Settings B2B for WooCommerce Captcha Settings Check if you want to make Additional Field 1 field required. Check if you want to make Additional Field 2 field required. Check if you want to make Additional Field 3 field required. Check if you want to make Company field required. Check if you want to make Email field required. Check if you want to make File/Image Upload field required. Check if you want to make Message field required. Check if you want to make Name field required. Check if you want to make Phone field required. Check this if you want to apply this rule on all products. Choose Customers Choose user roles to grant them tax exemption status. Click to View Company Field Custom Button Label Custom Button Link Custom Message Custom URL Customer Customer Email Customer Name Customers and Roles Date Date Published Default Fields Default Fields for Registration Settings Disable this checkbox will show a checkbox in the checkout page to notify them that tax exemption is available Disapprove Tax Info Email Message Disapprove Tax Info Email Subject Disapproved Email Subject Disapproved Email Text Display the above label on custom button, e.g "Request a Quote" Display the above text when price is hidden, e.g "Price is hidden" Edit Registration Field Edit Rule Email & Notification Email & Notification Settings Email Field Email Response Text Email Settings Email Subject Enable Additional Field 1 on the Request a Quote Form. Enable Additional Field 2 on the Request a Quote Form. Enable Additional Field 3 on the Request a Quote Form. Enable Ajax add to Quote (Product Page) Enable Ajax add to Quote (Shop Page) Enable Approve New User Enable Approve New User at Checkout Page Enable Captcha Enable Company Field Enable Company field on the Request a Quote Form. Enable Default Fields Enable Email Field Enable Email field on the Request a Quote Form. Enable Field 1 Enable Field 2 Enable Field 3 Enable File Upload Field Enable File/Image Upload field on the Request a Quote Form. Enable Global Visibility Enable Google reCaptcha field on the Request a Quote Form. Enable Message Field Enable Message field on the Request a Quote Form. Enable Name Field Enable Name field on the Request a Quote Form. Enable Phone Field Enable Phone field on the Request a Quote Form. Enable Text Field Enable Textarea Field Enable User Role Selection Enable admin email notification Enable for Guest Enable for this Role Enable or Disable Ajax add to quote on product page. Enable or Disable Ajax add to quote on shop page. Enable or Disable global visibility. Enable or Disable new user notification to admin from this module.  Enable or Disable quote for guest users. Enable or Disable welcome email notification from this module.  Enable this checkbox will disable tax for all tax-exempted and approved users. Enable this if you want to include a copy of quote details in the email sent to customer. The quote details will be embedded along the with the above email body text. Enable this if you want to send email to customer after submitting a Quote. Enable welcome email notification Enable/Disable Approve new user at the Checkout page. If you enable it, the order of the customer with registration will be placed and pending status is assigned to the user. Once the user logout from the site, he will not able to log in again until the administrator approves the user. If you disable it, the user will be approved automatically when registered from the checkout page. Enable/Disable Approve new user. When this option is enabled all new registered users will be set to Pending until admin approves Enable/Disable Default Fields of WooCommerce. Enable/Disable User Role selection on registration page. If this is enable then a user role dropdown will be shown on registration page. Enable/Disable tax for tax-exempted and approved users. Exclude User Roles Exemption Request Failed! Unable to process your request. Field 1 Field 2 Field label for user role selection select box. Fields Settings File Upload Field Label File/Image Upload Field First message that will be displayed to user when he/she completes the registration process, this message will be displayed only when manual approval is required.  Fixed Decrease Fixed Increase Fixed Price General General Settings Global Settings Global Visibility Settings Go to shop Google reCaptcha Settings Guest Guest Users Hide Hide Add to Cart Button Hide Price Hide Price Text If more than one rule is applied on same customer then rule that is added last will be applied. If this option is checked then a message will be displayed for guest user about tax exemption. If this option is checked then a message will be displayed for the above selected user role users about tax exemption. If this option is checked then tax exempt requests will be auto-approved and users of above selected user roles will be eligible for tax exempt right after submitting the info. Important Notes: In general settings you can enable/disable tax for specific users and choose which field(s) you want to show on the tax exemption request form. In this section, you can specify the customers and user roles who are exempted from tax. These customers and roles are not required to fill the tax form from "My Account" page. Include Copy of Quote in Email Invalid file type! Invalid reCaptcha! Keep Add to Cart and add a new custom button Keep Add to Cart button and add a new Quote Button Label Label of file upload field. Label of text field. Label of textarea field. Label of the Additional Field 1 field. Label of the Additional Field 2 field. Label of the Additional Field 3 field. Label of the Company field. Label of the Email field. Label of the File/Image Upload field. Label of the Message field. Label of the Name field. Label of the Phone field. Link for custom button e.g "http://www.example.com" Manage Approve new user settings from here. Manage Email Settings from here. Manage Google reCaptcha settings. Manage module general settings from here. Manage registration module general settings from here. Manage the redirect to quote page after Add to Quote and redirect to any page after quote form submission . Manage user role settings from here. Choose whether you want to show user role dropdown on registration page or not and choose which user roles you want to show in dropdown on registration page. Max Qty Max Qty Error Message Message Field Message Text Message for Users when Account is Created Message for Users when Account is Disapproved By Admin. Message for Users when Account is disapproved Message for Users when Account is pending for approval Min Qty Min Qty Error Message Name Field New Registration Field New Rule No No products in quote basket. No quote has been made yet. No registration field found No registration field found in trash No rule found No rule found in trash Non LoggedIn/Guest Payments Percentage Decrease Percentage Increase Phone Field Please note that Visibility by User Roles have high priority. If configurations are active for any user role – the global settings won’t work for that specific role. Please note that Visibility by User Roles have high priority. If following configurations are active for any user role – the global settings won’t work for that specific role. Price Specific to a Customer Price Specific to a Role Pricing Priority: Product Product SKU Products Visibility Provide number between 0 and 100, If more than one rules are applied on same item then rule with higher priority will be applied. 1 is high and 100 is low. Provide value from high priority 1 to Low priority 10. If more than one rule are applied on same item rule with high priority will be applied. Quantity Quote Quote  Quote # Quote #: Quote Basket Placement Quote Basket Style Quote Details Quote Info: Quote Rule for Guest Users Quote Rule for Registered Users Quote Rule for Selected Categories Quote Rule for Selected Products Quote for User Roles Quotes Redirect After Quote Submission Redirect Settings Redirect to Quote Page Redirect to Quote page after a product added to Quote successfully. Redirect to any page after Quote is submitted successfully. Redirect to this custom URL when user try to access restricted catalog. e.g http://www.example.com Redirection Mode Registration Field Registration Fields Regular Product Price Remove Remove Tax Automatically Remove this item Replace Add to Cart button with a Quote Button Replace Add to Cart with custom button Replace Original Price? Replace Orignal Price? Request a Quote Request a Quote Form Fields Request a Quote Rules Request a quote! Request for Quote Request for Quote Rules Request for QuotevRule Return To Shop Role Based Pricing Role Based Pricing Rules Role Based Pricing(By Customers) Role Based Pricing(By User Roles) Rule Details Rule Priority Rule Settings Rule Type SKU Search Registration Field Search Rule Secret Key Select Categories Select Menu where you want to show Mini Quote Basket. If there is no menu then you have to create menu in WordPress menus otherwise mini quote basket will not show. Select Payement Method for User Roles Select Payment Methods: Select Products Select Shipping Methods for User Roles Select Shipping Methods: Select User Roles Select categories on which products on which you want to apply. Select either you want to show products or hide products. Select products on which you want to apply. Select the design of Quote Basket Select user roles for whom you want to display tax exemption form in "My Account" page. Select which user roles users you want to exclude from manual approval. These user roles users will be automatically approved. Select which user roles you want to show in dropdown on registration page. Note: Administrator role is not available for show in dropdown. Send Email to Customer Settings Shipping Show Show Tax Exemption Message on Checkout Page Show tax exemption message Show tax exemption message for guest users. Show/Hide Site Key Sorry, your nonce did not verify. Sort Order Sort Order of the Additional Field 1 field. Sort Order of the Additional Field 2 field. Sort Order of the Additional Field 3 field. Sort Order of the Company field. Sort Order of the Email field. Sort Order of the File/Image Upload field. Sort Order of the Message field. Sort Order of the Name field. Sort Order of the Phone field. Submit Success Message Tax-Exempt Text Field Label Textarea Field Label This email address will be used for getting new user email notification for admin, if this is empty then default WordPress admin email address will be used. This email subject is used when new user notification is sent to admin.  This email text will be used when new user notification is sent to admin. If Approve new user is active then you can write text about new user approval. This file upload field will be shown in tax form in user my account page. This field can be used to collect tax certificate etc. This is Google reCaptcha secret key, you can get this from Google. Without this key Google reCaptcha will not work. This is Google reCaptcha site key, you can get this from Google. Without this key Google reCaptcha will not work. This is the approved email message, this message is used when account is approved by administrator.  This is the approved email subject, this subject is when used when account is approved by administrator.   This is the disapproved email message, this message is used when account is disapproved by administrator. This is the disapproved email subject, this subject is used when account is disapproved by administrator. This is the email body; when a new customer registers this email be automatically sent and the custom fields will be included in that email. This body text will be included along with the default fields data. This is the email subject; this subject is used when the email is sent to the user on account creation to include fields data and manual approval message. This is the title for the section where additional fields are displayed on front end registration form. This message will appear on quote submission page, when user submit quote. This message will be displayed for guest users. This message will be displayed when user try to access restricted catalog. This message will be shown when user add or update tax info in my account. This message will be used when a user add or update tax info from my account. This message will be used when admin approves submitted tax info. This message will be used when admin disapprove submitted tax info. This subject will be used when a user add or update tax info from my account. This subject will be used when admin approves submitted tax info. This subject will be used when admin disapprove submitted tax info. This subject will be used when email is sent to user when quote is submitted. This text field will be shown in tax form in user my account page. This field can be used to collect name, tax id etc. This text will be used when email is sent to user when quote is submitted. This textarea field will be shown in tax form in user my account page. This field can be used to collect additional info etc. This will be displayed when user will attempt to login after registration and his/her account is still pending for admin approval.  This will be visible for the user roles customer has selected above. This will help you to add fields on the quote form. This will help you to hide price and add to cart and show request a quote button for selected user roles including the guests users. This will help you to show or hide products for all customers including guests. This will only work for Fixed Price, Fixed Decrease and Percentage Decrease. URL of Page to Redirect after Quote Submission URL of page to redirect after Quote is submitted successfully. Update Cart Error Message User Role User Role Field Label User Role Settings Value View View Quote View Registration Field View Rule View this item Visibility By User Roles Welcome/Pending Email Body Text Welcome/Pending Email Subject WooCommerce B2b Plugin. (PLEASE TAKE BACKUP BEFORE UPDATING THE PLUGIN). X Yes You have recieved a new quote request. Your quote is currently empty. af-product-visibility has been added to your quote. http://www.addifypro.com https://woocommerce.com/products/b2b-for-woocommerce/ is Required? reCaptcha is required! Project-Id-Version: WooCommerce B2B Plugin
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-08-26 06:36+0000
PO-Revision-Date: 2020-08-27 08:11+0000
Last-Translator: 
Language-Team: Spanish (Spain)
Language: es_ES
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.2; wp-5.5 Ver cotización ¡es un campo obligatorio! artículos en cotización
 fue colocado en
 # --- Elija Menú --- --- Seleccione el tipo de ajuste --- : Acción Agregar nuevo campo Agregar nueva regla Agregar regla Addify Título de sección de campos adicionales Tipo de ajuste Dirección de correo electrónico del administrador Mensaje de correo electrónico del administrador
 Asunto del correo electrónico del administrador Texto del correo electrónico del administrador Correo electrónico del administrador / gerente de la tienda Todos los campos
 Todas las reglas de cotización
 Todas las cotizaciones Todos los campos de registro
 Todas las reglas de precios basadas en roles
 Todas las reglas Todas las cotizaciones enviadas Todos los correos electrónicos de administrador relacionados con nuestro módulo se enviarán a esta dirección de correo electrónico.  Todos los correos electrónicos de administrador relacionados con nuestro módulo se enviarán a esta dirección de correo electrónico. Si este correo electrónico está vacío, se usa la dirección de correo electrónico predeterminada del administrador.  Permitir que los motores de búsqueda indexen Permita que los motores de búsqueda rastreen e indexen productos ocultos, categorías y otras páginas. Si bien utiliza la opción global cuando oculta los productos de los usuarios invitados, también permanecerán ocultos para los motores de búsqueda, es decir, Google no podrá clasificar esas páginas en los resultados de búsqueda. Marque esta casilla si desea que Google rastree y clasifique las páginas ocultas. Tipos de archivo  Tipos permitidos Tipos de carga de archivos permitidos. por ejemplo (png, jpg, txt). Agregue comas separadas, no use punto (.). Aplicar en todos los productos Aprobar nuevas configuraciones de mensajes de usuario Aprobar nueva configuración de usuario Aprobar mensaje de correo electrónico con información fiscal
 Aprobar información fiscal Asunto del correo electrónico
 Asunto de correo electrónico aprobado Texto de correo electrónico aprobado Solicitud de aprobación automática de exención de impuestos
 B2B Registro B2B
 Configuración B2B
 B2B para WooCommerce
 Configuración de Captcha Marque si desea hacer que el campo adicional del campo 1 sea obligatorio. Marque si desea hacer que el campo adicional del campo 2 sea obligatorio. Marque si desea hacer que el campo adicional del campo 3 sea obligatorio. Marque si desea hacer que el campo Empresa sea obligatorio.

 Marque si desea hacer que el campo Correo electrónico sea obligatorio.
 Marque si desea hacer que el campo Carga de archivo / imagen sea obligatorio.
 Marque si desea hacer que el campo Mensaje sea obligatorio.
 Marque si desea hacer que el campo Nombre sea obligatorio.
 Marque si desea hacer que el campo Teléfono sea obligatorio. Check this if you want to apply this rule on all products. Elija clientes
 Elija roles de usuario para otorgarles el estado de exención de impuestos.
 Click para ver Campo de la empresa Etiqueta de botón personalizada Enlace de botón personalizado Mensaje personalizado URL personalizada Cliente
 Correo electrónico del cliente Nombre del cliente Clientes y roles
 Fecha Fecha de publicación Campos predeterminados
 Campos predeterminados para la configuración de registro
 Deshabilitar esta casilla de verificación mostrará una casilla de verificación en la página de pago para notificarles que la exención de impuestos está disponible
 Rechazar mensaje de correo electrónico con información fiscal
 Rechazar información fiscal Asunto del correo electrónico
 Asunto de correo electrónico rechazado Texto de correo electrónico rechazado Muestre la etiqueta anterior en el botón personalizado, por ejemplo, "Solicitar una cotización" Mostrar el texto anterior cuando el precio está oculto, por ejemplo, "El precio está oculto" Editar campo de registro Editar regla Notificación de correo electrónico
 Configuración de correo electrónico y notificaciones
 Campo de correo electrónico Texto de respuesta por correo electrónico Ajustes del correo electrónico
 Asunto del email
 Habilite el campo adicional 1 en el formulario Solicitar un presupuesto.

 Habilite el campo adicional 2 en el formulario Solicitar un presupuesto.
 Habilite el campo adicional 3 en el formulario Solicitar un presupuesto.
 Habilitar Ajax agregar a la cotización (página del producto)
 Habilitar Ajax agregar a la cotización (página de la tienda) Habilitar Aprobar nuevo usuario Habilitar Aprobar nuevo usuario en la página de pago Habilitar Captcha Habilitar campo de empresa

 Habilite el campo Empresa en el Formulario de solicitud de presupuesto. Habilitar campos predeterminados Habilitar campo de correo electrónico Habilite el campo Correo electrónico en el Formulario de solicitud de presupuesto. Habilitar campo 1
 Habilitar campo 2
 Habilitar campo 3

 Habilitar campo de carga de archivos
 Habilite el campo Carga de archivo / imagen en el Formulario de solicitud de presupuesto.
 Habilitar la visibilidad global
 Habilite el campo Google reCaptcha en el Formulario de solicitud de presupuesto. Habilitar campo de mensaje Habilite el campo Mensaje en el Formulario de solicitud de presupuesto. Habilitar campo de nombre

 Habilite el campo Nombre en el Formulario de solicitud de presupuesto.
 Habilitar campo de teléfono

 Habilite el campo Teléfono en el Formulario de solicitud de presupuesto. Habilitar campo de texto
 Habilitar el campo Textarea
 Habilitar la selección de roles de usuario
 Habilitar la notificación por correo electrónico del administrador
 Habilitar para invitado Habilitar para este rol Habilite o deshabilite ajax add to quote en la página del producto.
 Habilite o deshabilite ajax add to quote en la página de la tienda. Activar o desactivar la visibilidad global.
 Habilite o deshabilite la notificación de nuevo usuario al administrador desde este módulo.
 Habilitar o deshabilitar presupuesto para usuarios invitados. Habilite o deshabilite la notificación de nuevo usuario al administrador desde este módulo.
 Habilitar esta casilla de verificación inhabilitará los impuestos para todos los usuarios aprobados y exentos de impuestos.
 Habilite esta opción si desea incluir una copia de los detalles de la cotización en el correo electrónico enviado al cliente. Los detalles de la cotización se incorporarán junto con el texto del cuerpo del correo electrónico anterior.
 Habilite esto si desea enviar un correo electrónico al cliente después de enviar una cotización.
 Habilitar la notificación por correo electrónico de bienvenida
 Activar / Desactivar Aprobar nuevo usuario en la página de pago. Si lo habilita, se colocará el pedido del cliente con registro y se asignará el estado pendiente al usuario. Una vez que el usuario cierre la sesión del sitio, no podrá volver a iniciar sesión hasta que el administrador apruebe al usuario. Si lo desactiva, el usuario será aprobado automáticamente cuando se registre desde la página de pago.
 Activar / Desactivar Aprobar nuevo usuario. Cuando esta opción está habilitada, todos los nuevos usuarios registrados se establecerán en Pendiente hasta que el administrador lo apruebe. Habilitar / deshabilitar campos predeterminados de WooCommerce.
 Habilitar / deshabilitar la selección de roles de usuario en la página de registro. Si está habilitado, se mostrará un menú desplegable de roles de usuario en la página de registro.
 Habilitar / deshabilitar impuestos para usuarios aprobados y exentos de impuestos.
 Excluir roles de usuario Solicitud de exención
 ¡falló! No se puede procesar su solicitud. Campo 1

 Campo 2 Etiqueta de campo para el cuadro de selección de selección de rol de usuario.
 Configuraciones de campos
Campo de carga de archivo / imagen
 
Etiqueta de campo de carga de archivo
 Campo de carga de archivo / imagen Primer mensaje que se mostrará al usuario cuando finalice el proceso de registro, este mensaje se mostrará solo cuando se requiera la aprobación manual.
 Disminución fija
 Aumento fijo
 Precio fijo
 General
 Configuración general Configuración global
 Configuración de visibilidad global
 Ir a la tienda
 Configuración de Google reCaptcha Invitado Usuarios invitados Esconder
 Ocultar botón Añadir al carrito

 Ocultar precio
 Ocultar texto de precio Si se aplica más de una regla al mismo cliente, se aplicará la regla que se agregue en último lugar. Si esta opción está marcada, se mostrará un mensaje para el usuario invitado sobre la exención de impuestos.
 Si esta opción está marcada, se mostrará un mensaje para los usuarios de función de usuario seleccionados anteriormente sobre la exención de impuestos.
 Si esta opción está marcada, las solicitudes de exención de impuestos se aprobarán automáticamente y los usuarios de las funciones de usuario seleccionadas anteriormente serán elegibles para la exención de impuestos inmediatamente después de enviar la información.
 Notas importantes: En la configuración general, puede habilitar / deshabilitar impuestos para usuarios específicos y elegir qué campo (s) desea mostrar en el formulario de solicitud de exención de impuestos.
 En esta sección, puede especificar los clientes y los roles de usuario que están exentos de impuestos. Estos clientes y roles no están obligados a completar el formulario de impuestos de la página "Mi cuenta".
 Incluir copia de cotización en correo electrónico
 ¡Tipo de archivo invalido! ReCaptcha inválido! Mantenga Agregar al carrito y agregue un nuevo botón personalizado Mantenga el botón Agregar al carrito y agregue un nuevo botón de cotización
 Etiqueta Etiqueta del campo de carga del archivo.
 Etiqueta del campo de texto.
 Etiqueta del campo textarea.
 Etiqueta del campo adicional del campo 1.

 Etiqueta del campo adicional del campo 2.
 Etiqueta del campo adicional del campo 3.
 Etiqueta del campo Empresa.
 Etiqueta del campo Correo electrónico.
 Etiqueta del campo Carga de archivo / imagen.
 Etiqueta del campo Mensaje.
 Etiqueta del campo Nombre.
 Etiqueta del campo Teléfono. Enlace para botón personalizado, por ejemplo, "http://www.example.com" Manage Approve new user settings from here. Administre la configuración de correo electrónico desde aquí.
 Administra la configuración de Google reCaptcha.
 Administre la configuración general del módulo desde aquí.
 Administre la configuración general del módulo de registro desde aquí. Administre la redirección a la página de cotización después de Agregar a cotización y redirija a cualquier página después del envío del formulario de cotización.
 Administre la configuración de roles de usuario desde aquí. Elija si desea mostrar el menú desplegable de roles de usuario en la página de registro o no y elija qué roles de usuario desea mostrar en el menú desplegable de la página de registro.
 Cantidad máxima
 Mensaje de error de cantidad máxima
 Campo de mensaje Mensaje de texto
 Mensaje para los usuarios cuando se crea la cuenta Mensaje para los usuarios cuando la cuenta es rechazada por el administrador.

 Mensaje para los usuarios cuando la cuenta es rechazada
 Mensaje para los usuarios cuando la cuenta está pendiente de aprobación Cantidad mínima
 Mensaje de error de cantidad mínima
 Campo de nombre Nuevo campo de registro Nueva regla
 No No hay productos en la cesta de cotizaciones.
 No se ha hecho ninguna cita todavía.
 No se encontró campo de registro

 No se encontró campo de registro en la papelera No se encontró ninguna regla.
 No se encontró ninguna regla en la papelera.
 Non LoggedIn / Guest Pagos
 Porcentaje de disminución

 Porcentaje de aumento Campo del teléfono Tenga en cuenta que la visibilidad por roles de usuario tiene alta prioridad. Si las configuraciones están activas para cualquier función de usuario, la configuración global no funcionará para esa función específica.
 Tenga en cuenta que la visibilidad por roles de usuario tiene alta prioridad. Si las siguientes configuraciones están activas para cualquier función de usuario, la configuración global no funcionará para esa función específica.
 Precio específico para un cliente Precio específico de un rol Prioridad de precios: Producto
 Producto SKU  Visibilidad de productos
 Proporcione un número entre 0 y 100. Si se aplica más de una regla en el mismo elemento, se aplicará la regla con mayor prioridad. 1 es alto y 100 es bajo.
 Proporcione el valor de alta prioridad 1 a baja prioridad 10. Si se aplica más de una regla en el mismo elemento, se aplicará la regla con alta prioridad. Cantidad

 Citar
 Citar
 Cita #
 Cita #:
 Colocación de la cesta de cotizaciones Estilo de cesta de cotización
 Detalles de cotización

 Información de cotización:
 Regla de cotización para usuarios invitados
 Regla de cotización para usuarios registrados
 Regla de cotización para categorías seleccionadas
 Regla de cotización para productos seleccionados
 Presupuesto para roles de usuario
 Citas Redirigir después del envío de la cotización
 Configuración de redireccionamiento
 Redirigir a la página de cotización
 Redirigir a la página de cotización después de que un producto se haya agregado correctamente a la cotización.
 Redirigir a cualquier página después de que la cotización se envíe correctamente.
 Redirigir a esta URL personalizada cuando el usuario intente acceder al catálogo restringido. por ejemplo, http://www.example.com
 Modo de redireccionamiento
 Campo de registro
 Campos de registro

 Precio regular del producto
 Eliminar
 Eliminar impuestos automáticamente
 Eliminar este elemento Reemplace el botón Agregar al carrito con un botón de cotización
 Reemplace Agregar al carrito con un botón personalizado
 ¿Reemplazar precio original?
 Reemplazar precio original? Solicitar presupuesto Campos del formulario de solicitud de cotización
 Solicitar una cotización Reglas
 ¡Solicitar presupuesto! Solicitud de presupuesto
 Solicitud de reglas de cotización Solicitud de reglas de cotización Volver a la tienda Precios basados en roles

 Reglas de precios basadas en roles Precios basados en roles (por clientes)



 Precios basados en roles (por roles de usuario) Detalles de la regla

 Prioridad de regla Configuraciones de regla Tipo de regla SKU Buscar campo de registro Regla de búsqueda
 Llave secreta
 Seleccionar categorías Seleccione el menú donde desea mostrar la canasta de presupuestos pequeños. Si no hay un menú, entonces debe crear un menú en los menús de WordPress; de lo contrario, no se mostrará la canasta de mini cotizaciones.
 Seleccione el método de pago para los roles de usuario
 Seleccionar métodos de pago:
 Seleccionar productos Seleccionar métodos de envío para roles de usuario
 Seleccionar métodos de envío:
 Seleccionar roles de usuario Seleccione categorías en qué productos desea aplicar. Seleccione si desea mostrar productos u ocultar productos. Seleccione los productos en los que desea aplicar. Seleccione el diseño de Quote Basket
 Seleccione los roles de usuario para los que desea mostrar el formulario de exención de impuestos en la página "Mi cuenta".
 Seleccione los roles de usuario que desea excluir de la aprobación manual. Estos roles de usuarios serán aprobados automáticamente. Seleccione qué roles de usuario desea mostrar en el menú desplegable de la página de registro. Nota: El rol de administrador no está disponible para mostrar en el menú desplegable. Enviar correo electrónico al cliente Configuraciones
 Envío
 mostrar
 Mostrar mensaje de exención de impuestos en la página de pago
 Mostrar mensaje de exención de impuestos
 Mostrar mensaje de exención de impuestos para usuarios invitados.
 Mostrar ocultar Clave del sitio Lo sentimos, su nonce no se verificó.

 Orden de clasificación
 Orden de clasificación del campo adicional del campo 1.
 Orden de clasificación del campo adicional del campo 2.
 Orden de clasificación del campo adicional del campo 3.
 Orden de clasificación del campo Empresa.
 Orden de clasificación del campo Correo electrónico.
 Orden de clasificación del campo Carga de archivo / imagen.
 Orden de clasificación del campo Mensaje.
 Orden de clasificación del campo Nombre.
 Orden de clasificación del campo Teléfono.
 Enviar
 Mensaje de éxito Exento de impuestos
 Etiqueta de campo de texto
 Etiqueta de campo Textarea
 Esta dirección de correo electrónico se utilizará para recibir notificaciones de correo electrónico de nuevos usuarios para el administrador, si está vacía, se utilizará la dirección de correo electrónico predeterminada del administrador de WordPress. Este asunto del correo electrónico se utiliza cuando se envía una nueva notificación de usuario al administrador. Este texto de correo electrónico se usará cuando se envíe una nueva notificación de usuario al administrador. Si Aprobar nuevo usuario está activo, puede escribir un texto sobre la aprobación del nuevo usuario. Este campo de carga de archivos se mostrará en el formulario de impuestos en la página de mi cuenta de usuario. Este campo se puede utilizar para recoger el certificado fiscal, etc.
 Esta es la clave secreta de Google reCaptcha, puede obtenerla de google. Sin esta clave, Google reCaptcha no funcionará. Esta es la clave del sitio de Google reCaptcha, puede obtenerla de google. Sin esta clave, Google reCaptcha no funcionará. Este es el mensaje de correo electrónico aprobado, este mensaje se usa cuando el administrador aprueba la cuenta. Este es el asunto del correo electrónico aprobado, este asunto es cuando se usa cuando el administrador aprueba la cuenta. Este es el mensaje de correo electrónico rechazado, este mensaje se utiliza cuando el administrador rechaza la cuenta. Este es el asunto del correo electrónico rechazado, este asunto se usa cuando el administrador rechaza la cuenta. Este es el cuerpo del correo electrónico; cuando un nuevo cliente se registre, este correo electrónico se enviará automáticamente y los campos personalizados se incluirán en ese correo electrónico. Este texto del cuerpo se incluirá junto con los datos de los campos predeterminados. Este es el asunto del correo electrónico; Este asunto se utiliza cuando el correo electrónico se envía al usuario en la creación de la cuenta para incluir datos de campos y un mensaje de aprobación manual. Este es el título de la sección donde se muestran campos adicionales en el formulario de registro frontal. Este mensaje aparecerá en la página de envío de presupuesto, cuando el usuario envíe el presupuesto. Este mensaje se mostrará a los usuarios invitados.
 Este mensaje se mostrará cuando el usuario intente acceder al catálogo restringido.
 Este mensaje se mostrará cuando el usuario agregue o actualice la información fiscal en mi cuenta.
 Este mensaje se utilizará cuando un usuario agregue o actualice la información fiscal de mi cuenta.
 Este mensaje se utilizará cuando el administrador apruebe la información fiscal enviada.
 Este mensaje se utilizará cuando el administrador desapruebe la información fiscal enviada.
 Este asunto se utilizará cuando un usuario agregue o actualice la información fiscal de mi cuenta.
 Este asunto se utilizará cuando el administrador apruebe la información fiscal enviada.
 Este asunto se utilizará cuando el administrador desapruebe la información fiscal enviada.
 Este tema se utilizará cuando se envíe un correo electrónico al usuario cuando se envíe la cotización. Este campo de texto se mostrará en el formulario de impuestos en la página de mi cuenta de usuario. Este campo se puede utilizar para recopilar el nombre, la identificación fiscal, etc.
 Este texto se usará cuando se envíe un correo electrónico al usuario cuando se envíe la cotización. Este campo de área de texto se mostrará en el formulario de impuestos en la página de mi cuenta de usuario. Este campo se puede utilizar para recopilar información adicional, etc.
 Esto se mostrará cuando el usuario intente iniciar sesión después del registro y su cuenta aún esté pendiente de aprobación del administrador.
 Esto será visible para los roles de usuario que el cliente ha seleccionado anteriormente.
 Esto le ayudará a agregar campos en el formulario de cotización.
 Esto lo ayudará a ocultar el precio y agregarlo al carrito y mostrar el botón de solicitud de cotización para los roles de usuario seleccionados, incluidos los usuarios invitados.
 Esto lo ayudará a mostrar u ocultar productos para todos los clientes, incluidos los invitados. Esto solo funcionará para precio fijo, disminución fija y disminución de porcentaje. URL de la página para redireccionar después del envío de la cotización
 URL de la página para redirigir después de que la cotización se envíe correctamente.
 Mensaje de error del carro de actualización Rol del usuario Etiqueta de campo de rol de usuario Configuración de rol de usuario Valor
 Ver
 Ver cotización Ver campo de registro Ver regla Ver este artículo Visibilidad por roles de usuario Texto del cuerpo del correo electrónico de bienvenida / pendiente

 Bienvenido / Asunto pendiente de correo electrónico Complemento WooCommerce B2b. (POR FAVOR REALICE UNA COPIA DE SEGURIDAD ANTES DE ACTUALIZAR EL PLUGIN).
 X si
 Recibió una nueva solicitud de cotización. Su cotización está actualmente vacía. Af-visibilidad del producto af ha sido agregado a su cotización. http://www.addifypro.com http://www.addifypro.com ¿es requerido? Se requiere reCaptcha! 