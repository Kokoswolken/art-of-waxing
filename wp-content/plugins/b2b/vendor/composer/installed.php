<?php return array(
	'root' => array(
		'pretty_version' => '1.0.0+no-version-set',
		'version' => '1.0.0.0',
		'type' => 'library',
		'install_path' => __DIR__ . '/../../',
		'aliases' => array(),
		'reference' => null,
		'name' => '__root__',
		'dev' => true,
	),
	'versions' => array(
		'__root__' => array(
			'pretty_version' => '1.0.0+no-version-set',
			'version' => '1.0.0.0',
			'type' => 'library',
			'install_path' => __DIR__ . '/../../',
			'aliases' => array(),
			'reference' => null,
			'dev_requirement' => false,
		),
		'ph-7/eu-vat-validator' => array(
			'pretty_version' => '3.0',
			'version' => '3.0.0.0',
			'type' => 'library',
			'install_path' => __DIR__ . '/../ph-7/eu-vat-validator',
			'aliases' => array(),
			'reference' => '67c060ee07a753e97eed34b54246d658d1a65332',
			'dev_requirement' => false,
		),
	),
);
