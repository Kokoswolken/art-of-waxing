<?php
//Tab1
add_settings_section(  
	'page_1_section',         // ID used to identify this section and with which to register options  
	'',   // Title to be displayed on the administration page  
	'afrfq_page_1_section_callback', // Callback used to render the description of the section  
	'addify-rfq-1'                           // Page on which to add this section of options  
);

add_settings_field(   
	'quote_menu',                      // ID used to identify the field throughout the theme  
	esc_html__('Quote Basket Placement', 'addify_b2b'),    // The label to the left of the option interface element  
	'quote_menu_callback',   // The name of the function responsible for rendering the option interface  
	'addify-rfq-1',                          // The page on which this option will be displayed  
	'page_1_section',         // The name of the section to which this field belongs  
	array(                              // The array of arguments to pass to the callback. In this case, just a description.  
		esc_html__('Select Menu where you want to show Mini Quote Basket. If there is no menu then you have to create menu in WordPress menus otherwise mini quote basket will not show.', 'addify_b2b'),
	)  
);  
register_setting(  
	'rfq_setting-group-1',  
	'quote_menu'  
);

add_settings_field(   
	'afrfq_basket_option',                      // ID used to identify the field throughout the theme  
	esc_html__('Quote Basket Style', 'addify_b2b'),    // The label to the left of the option interface element  
	'afrfq_basket_option_callback',   // The name of the function responsible for rendering the option interface  
	'addify-rfq-1',                          // The page on which this option will be displayed  
	'page_1_section',         // The name of the section to which this field belongs  
	array(                              // The array of arguments to pass to the callback. In this case, just a description.  
		esc_html__('Select the design of Quote Basket', 'addify_b2b'),
	)  
);  
register_setting(  
	'rfq_setting-group-1',  
	'afrfq_basket_option'  
);

add_settings_field(   
	'allow_guest',                      // ID used to identify the field throughout the theme  
	esc_html__('Enable for Guest', 'addify_b2b'),    // The label to the left of the option interface element  
	'allow_guest_callback',   // The name of the function responsible for rendering the option interface  
	'addify-rfq-1',                          // The page on which this option will be displayed  
	'page_1_section',         // The name of the section to which this field belongs  
	array(                              // The array of arguments to pass to the callback. In this case, just a description.  
		esc_html__('Enable or Disable quote for guest users.', 'addify_b2b'),
	)  
);  
register_setting(  
	'rfq_setting-group-1',  
	'allow_guest'  
);

add_settings_field(   
	'enable_ajax_shop',                      // ID used to identify the field throughout the theme  
	esc_html__('Enable Ajax add to Quote (Shop Page)', 'addify_b2b'),    // The label to the left of the option interface element  
	'enable_ajax_shop_callback',   // The name of the function responsible for rendering the option interface  
	'addify-rfq-1',                          // The page on which this option will be displayed  
	'page_1_section',         // The name of the section to which this field belongs  
	array(                              // The array of arguments to pass to the callback. In this case, just a description.  
		esc_html__('Enable or Disable Ajax add to quote on shop page.', 'addify_b2b'),
	)  
);  
register_setting(  
	'rfq_setting-group-1',  
	'enable_ajax_shop'  
);

add_settings_field(   
	'enable_ajax_product',                      // ID used to identify the field throughout the theme  
	esc_html__('Enable Ajax add to Quote (Product Page)', 'addify_b2b'),    // The label to the left of the option interface element  
	'enable_ajax_product_callback',   // The name of the function responsible for rendering the option interface  
	'addify-rfq-1',                          // The page on which this option will be displayed  
	'page_1_section',         // The name of the section to which this field belongs  
	array(                              // The array of arguments to pass to the callback. In this case, just a description.  
		esc_html__('Enable or Disable Ajax add to quote on product page.', 'addify_b2b'),
	)  
);  
register_setting(  
	'rfq_setting-group-1',  
	'enable_ajax_product'  
);

add_settings_field(   
	'afrfq_success_message',                      // ID used to identify the field throughout the theme  
	esc_html__('Success Message', 'addify_b2b'),    // The label to the left of the option interface element  
	'afrfq_success_message_callback',   // The name of the function responsible for rendering the option interface  
	'addify-rfq-1',                          // The page on which this option will be displayed  
	'page_1_section',         // The name of the section to which this field belongs  
	array(                              // The array of arguments to pass to the callback. In this case, just a description.  
		esc_html__('This message will appear on quote submission page, when user submit quote.', 'addify_b2b'),
	)  
);  
register_setting(  
	'rfq_setting-group-1',  
	'afrfq_success_message'  
);

add_settings_field(   
	'afrfq_email_subject',                      // ID used to identify the field throughout the theme  
	esc_html__('Email Subject', 'addify_b2b'),    // The label to the left of the option interface element  
	'afrfq_email_subject_callback',   // The name of the function responsible for rendering the option interface  
	'addify-rfq-1',                          // The page on which this option will be displayed  
	'page_1_section',         // The name of the section to which this field belongs  
	array(                              // The array of arguments to pass to the callback. In this case, just a description.  
		esc_html__('This subject will be used when email is sent to user when quote is submitted.', 'addify_b2b'),
	)  
);  
register_setting(  
	'rfq_setting-group-1',  
	'afrfq_email_subject'  
);

add_settings_field(   
	'afrfq_email_text',                      // ID used to identify the field throughout the theme  
	esc_html__('Email Response Text', 'addify_b2b'),    // The label to the left of the option interface element  
	'afrfq_email_text_callback',   // The name of the function responsible for rendering the option interface  
	'addify-rfq-1',                          // The page on which this option will be displayed  
	'page_1_section',         // The name of the section to which this field belongs  
	array(                              // The array of arguments to pass to the callback. In this case, just a description.  
		esc_html__('This text will be used when email is sent to user when quote is submitted.', 'addify_b2b'),
	)  
);  
register_setting(  
	'rfq_setting-group-1',  
	'afrfq_email_text'  
);

add_settings_field(   
	'enable_user_mail_option',                      // ID used to identify the field throughout the theme  
	esc_html__('Send Email to Customer', 'addify_b2b'),    // The label to the left of the option interface element  
	'enable_user_mail_option_callback',   // The name of the function responsible for rendering the option interface  
	'addify-rfq-1',                          // The page on which this option will be displayed  
	'page_1_section',         // The name of the section to which this field belongs  
	array(                              // The array of arguments to pass to the callback. In this case, just a description.  
		esc_html__('Enable this if you want to send email to customer after submitting a Quote.', 'addify_b2b'),
	)  
);  

register_setting(  
	'rfq_setting-group-1',  
	'enable_user_mail_option'  
);

add_settings_field(   
	'enable_user_mail',                      // ID used to identify the field throughout the theme  
	esc_html__('Include Copy of Quote in Email', 'addify_b2b'),    // The label to the left of the option interface element  
	'enable_user_mail_callback',   // The name of the function responsible for rendering the option interface  
	'addify-rfq-1',                          // The page on which this option will be displayed  
	'page_1_section',         // The name of the section to which this field belongs  
	array(                              // The array of arguments to pass to the callback. In this case, just a description.  
		esc_html__('Enable this if you want to include a copy of quote details in the email sent to customer. The quote details will be embedded along the with the above email body text.', 'addify_b2b'),
	)  
);  
register_setting(  
	'rfq_setting-group-1',  
	'enable_user_mail'  
);

add_settings_field(   
	'afrfq_admin_email',                      // ID used to identify the field throughout the theme  
	esc_html__('Admin/Shop Manager Email', 'addify_b2b'),    // The label to the left of the option interface element  
	'afrfq_admin_email_callback',   // The name of the function responsible for rendering the option interface  
	'addify-rfq-1',                          // The page on which this option will be displayed  
	'page_1_section',         // The name of the section to which this field belongs  
	array(                              // The array of arguments to pass to the callback. In this case, just a description.  
		esc_html__('All admin emails that are related to our module will be sent to this email address. If this email is empty then default admin email address is used.', 'addify_b2b'),
	)  
);  
register_setting(  
	'rfq_setting-group-1',  
	'afrfq_admin_email'  
);




//Tab 2
add_settings_section(  
	'page_1_section',         // ID used to identify this section and with which to register options  
	'',   // Title to be displayed on the administration page  
	'afrfq_page_2_section_callback', // Callback used to render the description of the section  
	'addify-rfq-2'                           // Page on which to add this section of options  
);

add_settings_field(   
	'afrfq_fields',                      // ID used to identify the field throughout the theme  
	esc_html__('Request a Quote Form Fields', 'addify_b2b'),    // The label to the left of the option interface element  
	'afrfq_fields_callback',   // The name of the function responsible for rendering the option interface  
	'addify-rfq-2',                          // The page on which this option will be displayed  
	'page_1_section',         // The name of the section to which this field belongs  
	''                           // The array of arguments to pass to the callback. In this case, just a description.    
);  
register_setting(  
	'rfq_setting-group-2',  
	'afrfq_fields'  
);


//Tab 10
add_settings_section(  
	'page_1_section',         // ID used to identify this section and with which to register options  
	'',   // Title to be displayed on the administration page  
	'afrfq_page_10_section_callback', // Callback used to render the description of the section  
	'addify-rfq-10'                           // Page on which to add this section of options  
);

add_settings_field(   
	'afrfq_redirect_to_quote',                      // ID used to identify the field throughout the theme  
	esc_html__('Redirect to Quote Page', 'addify_b2b'),    // The label to the left of the option interface element  
	'afrfq_redirect_to_quote_callback',   // The name of the function responsible for rendering the option interface  
	'addify-rfq-10',                          // The page on which this option will be displayed  
	'page_1_section',         // The name of the section to which this field belongs  
	array(                              // The array of arguments to pass to the callback. In this case, just a description.  
		esc_html__('Redirect to Quote page after a product added to Quote successfully.', 'addify_b2b'),
	)                           // The array of arguments to pass to the callback. In this case, just a description.    
);  
register_setting(  
	'rfq_setting-group-10',  
	'afrfq_redirect_to_quote'  
);

add_settings_field(   
	'afrfq_redirect_after_submission',                      // ID used to identify the field throughout the theme  
	esc_html__('Redirect After Quote Submission', 'addify_b2b'),    // The label to the left of the option interface element  
	'afrfq_redirect_after_submission_callback',   // The name of the function responsible for rendering the option interface  
	'addify-rfq-10',                          // The page on which this option will be displayed  
	'page_1_section',         // The name of the section to which this field belongs  
	array(                              // The array of arguments to pass to the callback. In this case, just a description.  
		esc_html__('Redirect to any page after Quote is submitted successfully.', 'addify_b2b'),
	)                           // The array of arguments to pass to the callback. In this case, just a description.    
);  
register_setting(  
	'rfq_setting-group-10',  
	'afrfq_redirect_after_submission'  
);

add_settings_field(   
	'afrfq_redirect_url',                      // ID used to identify the field throughout the theme  
	esc_html__('URL of Page to Redirect after Quote Submission', 'addify_b2b'),    // The label to the left of the option interface element  
	'afrfq_redirect_url_callback',   // The name of the function responsible for rendering the option interface  
	'addify-rfq-10',                          // The page on which this option will be displayed  
	'page_1_section',         // The name of the section to which this field belongs  
	array(                              // The array of arguments to pass to the callback. In this case, just a description.  
		esc_html__('URL of page to redirect after Quote is submitted successfully.', 'addify_b2b'),
	)                           // The array of arguments to pass to the callback. In this case, just a description.    
);  
register_setting(  
	'rfq_setting-group-10',  
	'afrfq_redirect_url'  
);


//Tab 3
add_settings_section(  
	'page_1_section',         // ID used to identify this section and with which to register options  
	'',   // Title to be displayed on the administration page  
	'afrfq_page_3_section_callback', // Callback used to render the description of the section  
	'addify-rfq-3'                           // Page on which to add this section of options  
);

add_settings_field(   
	'afrfq_enable_captcha',                      // ID used to identify the field throughout the theme  
	esc_html__('Enable Captcha', 'addify_b2b'),    // The label to the left of the option interface element  
	'afrfq_enable_captcha_callback',   // The name of the function responsible for rendering the option interface  
	'addify-rfq-3',                          // The page on which this option will be displayed  
	'page_1_section',         // The name of the section to which this field belongs  
	array(                              // The array of arguments to pass to the callback. In this case, just a description.  
		esc_html__('Enable Google reCaptcha field on the Request a Quote Form.', 'addify_b2b'),
	)                           // The array of arguments to pass to the callback. In this case, just a description.    
);  
register_setting(  
	'rfq_setting-group-3',  
	'afrfq_enable_captcha'  
);

add_settings_field(   
	'afrfq_site_key',                      // ID used to identify the field throughout the theme  
	esc_html__('Site Key', 'addify_b2b'),    // The label to the left of the option interface element  
	'afrfq_site_key_callback',   // The name of the function responsible for rendering the option interface  
	'addify-rfq-3',                          // The page on which this option will be displayed  
	'page_1_section',         // The name of the section to which this field belongs  
	array(                              // The array of arguments to pass to the callback. In this case, just a description.  
		esc_html__('This is Google reCaptcha site key, you can get this from Google. Without this key Google reCaptcha will not work.', 'addify_b2b'),
	)                           // The array of arguments to pass to the callback. In this case, just a description.    
);  
register_setting(  
	'rfq_setting-group-3',  
	'afrfq_site_key'  
);

add_settings_field(   
	'afrfq_secret_key',                      // ID used to identify the field throughout the theme  
	esc_html__('Secret Key', 'addify_b2b'),    // The label to the left of the option interface element  
	'afrfq_secret_key_callback',   // The name of the function responsible for rendering the option interface  
	'addify-rfq-3',                          // The page on which this option will be displayed  
	'page_1_section',         // The name of the section to which this field belongs  
	array(                              // The array of arguments to pass to the callback. In this case, just a description.  
		esc_html__('This is Google reCaptcha secret key, you can get this from Google. Without this key Google reCaptcha will not work.', 'addify_b2b'),
	)                           // The array of arguments to pass to the callback. In this case, just a description.    
);  
register_setting(  
	'rfq_setting-group-3',  
	'afrfq_secret_key'  
);





//Tab 1
function afrfq_page_1_section_callback() {
	?>
	<div class="afb2b_setting_div">
		<p><?php echo esc_html__('This will help you to hide price and add to cart and show request a quote button for selected user roles including the guests users.', 'addify_b2b'); ?></p>
	</div>

	<?php 
} // function afreg_page_1_section_callback

function quote_menu_callback( $args) {
	?>
	
	<?php 
	$menus   = get_terms('nav_menu');
	$options = get_option('quote_menu');
	if (!is_array($options) ) {
		$options = array();
	}
	?>
	<select name="quote_menu[]" class="quote_select2" multiple="multiple">
		<option value=""><?php echo esc_html__('---Choose Menu---', 'addify_b2b'); ?></option>
	<?php
	foreach ($menus as $menu) {
		?>
			<option value="<?php echo intval($menu->term_id); ?>" 
									  <?php 
										if (in_array(intval($menu->term_id), $options) ) {
											echo 'selected' ; 
										} 
										?>
			><?php echo esc_attr($menu->name); ?></option>
	<?php } ?>
	</select>
	<p class="description quote_menu"> <?php echo esc_attr($args[0]); ?> </p>
	<?php      
} // end quote_menu_callback 


function afrfq_basket_option_callback( $args ) {
	?>
	
	<input  value="dropdown" class="" id="" type="radio" name="afrfq_basket_option" <?php echo checked(get_option('afrfq_basket_option'), 'dropdown'); ?> >
	<?php 
	echo esc_html__('Quote Basket with Dropdown', 'addify_b2b'
	); 
	?>
	<br>
	<input value="icon" class="" id="" type="radio" name="afrfq_basket_option" <?php echo  checked(get_option('afrfq_basket_option'), 'icon'); ?> >
	<?php 
	echo esc_html__('Icon and Number of items', 'addify_b2b'
	); 
	?>
	<p class="description quote_menu"> <?php echo esc_attr($args[0]); ?> </p>
	<?php    
}

function allow_guest_callback( $args) {
	?>
	<input type="checkbox" id="allow_guest" class="setting_fields" name="allow_guest" value="yes" <?php echo checked('yes', esc_attr(get_option('allow_guest'))); ?> >
	<p class="description allow_guest"> <?php echo esc_attr($args[0]); ?> </p>
	<?php      
} // end allow_guest_callback 


function enable_ajax_shop_callback( $args) {
	?>
	<input type="checkbox" id="enable_ajax_shop" class="setting_fields" name="enable_ajax_shop" value="yes" <?php echo checked('yes', esc_attr(get_option('enable_ajax_shop'))); ?> >
	<p class="description enable_ajax_shop"> <?php echo esc_attr($args[0]); ?> </p>
	<?php      
} // end enable_ajax_shop_callback 

function enable_ajax_product_callback( $args) {
	?>
	<input type="checkbox" id="enable_ajax_product" class="setting_fields" name="enable_ajax_product" value="yes" <?php echo checked('yes', esc_attr(get_option('enable_ajax_product'))); ?> >
	<p class="description enable_ajax_product"> <?php echo esc_attr($args[0]); ?> </p>
	<?php      
} // end enable_ajax_product_callback 

function afrfq_success_message_callback( $args) {
	?>
	<input type="text" id="afrfq_success_message" class="setting_fields" name="afrfq_success_message" value="<?php echo esc_attr(get_option('afrfq_success_message')); ?>">
	<p class="description afrfq_success_message"> <?php echo esc_attr($args[0]); ?> </p>
	<?php      
} // end afrfq_success_message_callback 

function afrfq_email_subject_callback( $args) {
	?>
	<input type="text" id="afrfq_email_subject" class="setting_fields" name="afrfq_email_subject" value="<?php echo esc_attr(get_option('afrfq_email_subject')); ?>">
	<p class="description afrfq_email_subject"> <?php echo esc_attr($args[0]); ?> </p>
	<?php      
} // end afrfq_email_subject_callback 

function afrfq_email_text_callback( $args) {
	?>
	
	<?php

	$content   = get_option('afrfq_email_text');
	$editor_id = 'afrfq_email_text';
	$settings  = array(
	'tinymce' => true,
	'textarea_rows' => 10,
	'quicktags' => array('buttons' => 'em,strong,link',),
	'quicktags' => true,
	'tinymce' => true,
	);

	wp_editor($content, $editor_id, $settings);

	?>
	<p class="description afrfq_email_text"> <?php echo esc_attr($args[0]); ?> </p>
	<?php      
} // end afrfq_email_text_callback



function enable_user_mail_option_callback( $args) {
	?>
	<input type="checkbox" name="enable_user_mail_option" id="enable_user_mail_option" value="yes" <?php echo checked('yes', esc_attr(get_option('enable_user_mail_option'))); ?> >
	<p class="description afrfq_admin_email"> <?php echo esc_attr($args[0]); ?> </p>
	<?php      
} 

function enable_user_mail_callback( $args) {
	?>
	<input type="checkbox" name="enable_user_mail" id="enable_user_mail" value="yes" <?php echo checked('yes', esc_attr(get_option('enable_user_mail'))); ?> >
	<p class="description afrfq_admin_email"> <?php echo esc_attr($args[0]); ?> </p>
	<?php      
} 

function afrfq_admin_email_callback( $args) {
	?>
	<input type="text" id="afrfq_admin_email" class="setting_fields" name="afrfq_admin_email" value="<?php echo esc_attr(get_option('afrfq_admin_email')); ?>">
	<p class="description afrfq_admin_email"> <?php echo esc_attr($args[0]); ?> </p>
	<?php      
} // end afrfq_admin_email_callback 




//Tab 2
function afrfq_page_2_section_callback() {
	?>
	<div class="afb2b_setting_div">
		<p><?php echo esc_html__('This will help you to add fields on the quote form.', 'addify_b2b'); ?></p>
	</div>

	<?php 
} // function afreg_page_1_section_callback

function searchForId( $id, $array) {

	if (!empty($array)) {
		foreach ($array as $key => $val) {
			if ($val['field_key'] === $id) {
				return $key;
			}
		}
	}
	return null;
}



function afrfq_fields_callback() {
	?>
	
	<div class="afpvu_accordian">
		<div id="accordion">
			<!-- Name Field-->
	<?php
				$name_field_array = get_option('afrfq_fields');
				
	if (!empty($name_field_array[0]['enable_field'])) {
		$name_is_enabled = $name_field_array[0]['enable_field'];
	} else {
		$name_is_enabled = '';
	}                
				
	if (!empty($name_field_array[0]['field_required'])) {
		$name_is_required = $name_field_array[0]['field_required'];
	} else {
		$name_is_required = '';
	}
				
	if (!empty($name_field_array[0]['field_sort_order'])) {
		$name_sort_order = $name_field_array[0]['field_sort_order'];
	} else {
		$name_sort_order = '';
	}
				
	if (!empty($name_field_array[0]['field_label'])) {
		$name_label = $name_field_array[0]['field_label'];
	} else {
		$name_label = '';
	}
				
				
				
	?>
			<h3><?php echo esc_html__('Name Field', 'addify_b2b'); ?></h3>
			<div>
				<p>
					<table class="addify-table-optoin">
						<tbody>
							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Enable Name Field', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input type="checkbox" name="afrfq_fields[0][enable_field]" id="afrfq_enable_name_field" value="yes" <?php echo checked('yes', esc_attr($name_is_enabled)); ?> />
									<p><?php echo esc_html__('Enable Name field on the Request a Quote Form.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('is Required?', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input type="checkbox" name="afrfq_fields[0][field_required]" id="afrfq_name_field_required" value="yes" <?php echo  checked('yes', esc_attr($name_is_required)); ?> />
									<p><?php echo esc_html__('Check if you want to make Name field required.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Sort Order', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input class="afrfq_input_class" type="number" name="afrfq_fields[0][field_sort_order]" id="aftax_name_field_sortorder" min="0" value="<?php echo esc_attr($name_sort_order); ?>" />
									<p><?php echo esc_html__('Sort Order of the Name field.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Label', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input class="afrfq_input_class" type="text" name="afrfq_fields[0][field_label]" id="afrfq_name_field_label"  value="<?php echo esc_attr($name_label); ?>" />
									<p><?php echo esc_html__('Label of the Name field.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<input type="hidden" name="afrfq_fields[0][field_key]" value="afrfq_name_field">
						</tbody>
					</table>
				</p>
			</div>

			<!-- Email Field -->

	<?php

				$email_field_array = get_option('afrfq_fields');
				
	if (!empty($email_field_array[1]['enable_field'])) {
		$email_is_enabled = $email_field_array[1]['enable_field'];
	} else {
		$email_is_enabled = '';
	}
				
	if (!empty($email_field_array[1]['field_required'])) {
		$email_is_required = $email_field_array[1]['field_required'];
	} else {
		$email_is_required = '';
	}
				
	if (!empty($email_field_array[1]['field_sort_order'])) {
		$email_sort_order = $email_field_array[1]['field_sort_order'];
	} else {
		$email_sort_order = '';
	}
				
	if (!empty($email_field_array[1]['field_label'])) {
		$email_label = $email_field_array[1]['field_label'];
	} else {
		$email_label = '';
	}
				
				
				
	?>

			<h3><?php echo esc_html__('Email Field', 'addify_b2b'); ?></h3>
			<div>
				<p>
					<table class="addify-table-optoin">
						<tbody>
							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Enable Email Field', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input type="checkbox" name="afrfq_fields[1][enable_field]" id="afrfq_enable_email_field" value="yes" <?php echo  checked('yes', esc_attr($email_is_enabled)); ?> />
									<p><?php echo esc_html__('Enable Email field on the Request a Quote Form.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('is Required?', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input type="checkbox" name="afrfq_fields[1][field_required]" id="afrfq_email_field_required" value="yes" <?php echo  checked('yes', esc_attr($email_is_required)); ?> />
									<p><?php echo esc_html__('Check if you want to make Email field required.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Sort Order', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input class="afrfq_input_class" type="number" name="afrfq_fields[1][field_sort_order]" id="afrfq_email_field_sortorder" min="0" value="<?php echo esc_attr($email_sort_order); ?>" />
									<p><?php echo esc_html__('Sort Order of the Email field.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Label', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input class="afrfq_input_class" type="text" name="afrfq_fields[1][field_label]" id="afrfq_email_field_label"  value="<?php echo esc_attr($email_label); ?>" />
									<p><?php echo esc_html__('Label of the Email field.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<input type="hidden" name="afrfq_fields[1][field_key]" value="afrfq_email_field">
						</tbody>
					</table>
				</p>
			</div>

			<!-- Company Field -->

	<?php

				$company_field_array = get_option('afrfq_fields');
				
	if (!empty($company_field_array[2]['enable_field'])) {
		$company_is_enabled = $company_field_array[2]['enable_field'];
	} else {
		$company_is_enabled = '';
	}
				
	if (!empty($company_field_array[2]['field_required'])) {
		$company_is_required = $company_field_array[2]['field_required'];
	} else {
		$company_is_required = '';
	}
				
	if (!empty($company_field_array[2]['field_sort_order'])) {
		$company_sort_order = $company_field_array[2]['field_sort_order'];
	} else {
		$company_sort_order = '';
	}
				
	if (!empty($company_field_array[2]['field_label'])) {
		$company_label = $company_field_array[2]['field_label'];
	} else {
		$company_label = '';
	}
				
				
				
				
	?>
			<h3><?php echo esc_html__('Company Field', 'addify_b2b'); ?></h3>
			<div>
				<p>
					<table class="addify-table-optoin">
						<tbody>
							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Enable Company Field', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input type="checkbox" name="afrfq_fields[2][enable_field]" id="afrfq_enable_company_field" value="yes" <?php echo checked('yes', esc_attr($company_is_enabled)); ?> />
									<p><?php echo esc_html__('Enable Company field on the Request a Quote Form.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('is Required?', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input type="checkbox" name="afrfq_fields[2][field_required]" id="afrfq_company_field_required" value="yes" <?php echo  checked('yes', esc_attr($company_is_required)); ?> />
									<p><?php echo esc_html__('Check if you want to make Company field required.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Sort Order', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input class="afrfq_input_class" type="number" name="afrfq_fields[2][field_sort_order]" id="afrfq_company_field_sortorder" min="0" value="<?php echo esc_attr($company_sort_order); ?>" />
									<p><?php echo esc_html__('Sort Order of the Company field.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Label', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input class="afrfq_input_class" type="text" name="afrfq_fields[2][field_label]" id="afrfq_company_field_label"  value="<?php echo esc_attr($company_label); ?>" />
									<p><?php echo esc_html__('Label of the Company field.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<input type="hidden" name="afrfq_fields[2][field_key]" value="afrfq_company_field">
						</tbody>
					</table>
				</p>
			</div>

			<!-- Phone Field -->

	<?php

				$phone_field_array = get_option('afrfq_fields');
				
	if (!empty($phone_field_array[3]['enable_field'])) {
		$phone_is_enabled = $phone_field_array[3]['enable_field'];
	} else {
		$phone_is_enabled = '';
	}
				
	if (!empty($phone_field_array[3]['field_required'])) {
		$phone_is_required = $phone_field_array[3]['field_required'];
	} else {
		$phone_is_required = '';
	}
				
	if (!empty($phone_field_array[3]['field_sort_order'])) {
		$phone_sort_order = $phone_field_array[3]['field_sort_order'];
	} else {
		$phone_sort_order = '';
	}
				
	if (!empty($phone_field_array[3]['field_label'])) {
		$phone_label = $phone_field_array[3]['field_label'];
	} else {
		$phone_label = '';
	}
				
				
				
				
	?>

			<h3><?php echo esc_html__('Phone Field', 'addify_b2b'); ?></h3>
			<div>
				<p>
					<table class="addify-table-optoin">
						<tbody>
							
							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Enable Phone Field', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input type="checkbox" name="afrfq_fields[3][enable_field]" id="afrfq_enable_phone_field" value="yes" <?php echo checked('yes', esc_attr($phone_is_enabled)); ?> />
									<p><?php echo esc_html__('Enable Phone field on the Request a Quote Form.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('is Required?', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input type="checkbox" name="afrfq_fields[3][field_required]" id="afrfq_phone_field_required" value="yes" <?php echo  checked('yes', esc_attr($phone_is_required)); ?> />
									<p><?php echo esc_html__('Check if you want to make Phone field required.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Sort Order', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input class="afrfq_input_class" type="number" name="afrfq_fields[3][field_sort_order]" id="afrfq_phone_field_sortorder" min="0" value="<?php echo esc_attr($phone_sort_order); ?>" />
									<p><?php echo esc_html__('Sort Order of the Phone field.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Label', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input class="afrfq_input_class" type="text" name="afrfq_fields[3][field_label]" id="afrfq_phone_field_label"  value="<?php echo esc_attr($phone_label); ?>" />
									<p><?php echo esc_html__('Label of the Phone field.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<input type="hidden" name="afrfq_fields[3][field_key]" value="afrfq_phone_field">

						</tbody>
					</table>
				</p>
			</div>

			<!-- File/Image Upload field -->

	<?php

				$file_field_array = get_option('afrfq_fields');
				
	if (!empty($file_field_array[4]['enable_field'])) {
		$file_is_enabled = $file_field_array[4]['enable_field'];
	} else {
		$file_is_enabled = '';
	}
				
	if (!empty($file_field_array[4]['field_required'])) {
		$file_is_required = $file_field_array[4]['field_required'];
	} else {
		$file_is_required = '';
	}
				
	if (!empty($file_field_array[4]['field_sort_order'])) {
		$file_sort_order = $file_field_array[4]['field_sort_order'];    
	} else {
		$file_sort_order = '';
	}
				
	if (!empty($file_field_array[4]['field_label'])) {
		$file_label = $file_field_array[4]['field_label'];
	} else {
		$file_label = '';    
	}
				
	if (!empty($file_field_array[4]['file_allowed_types'])) {
		$file_allowed_types = $file_field_array[4]['file_allowed_types'];
	} else {
		$file_allowed_types = '';
	}
				
				
				
				
	?>

			<h3><?php echo esc_html__('File/Image Upload Field', 'addify_b2b'); ?></h3>
			<div>
				<p>
					<table class="addify-table-optoin">
						<tbody>
							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Enable File Upload Field', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input type="checkbox" name="afrfq_fields[4][enable_field]" id="afrfq_enable_phone_field" value="yes" <?php echo checked('yes', esc_attr($file_is_enabled)); ?> />
									<p><?php echo esc_html__('Enable File/Image Upload field on the Request a Quote Form.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('is Required?', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input type="checkbox" name="afrfq_fields[4][field_required]" id="afrfq_phone_field_required" value="yes" <?php echo  checked('yes', esc_attr($file_is_required)); ?> />
									<p><?php echo esc_html__('Check if you want to make File/Image Upload field required.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Sort Order', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input class="afrfq_input_class" type="number" name="afrfq_fields[4][field_sort_order]" id="afrfq_phone_field_sortorder" min="0" value="<?php echo esc_attr($file_sort_order); ?>" />
									<p><?php echo esc_html__('Sort Order of the File/Image Upload field.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Label', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input class="afrfq_input_class" type="text" name="afrfq_fields[4][field_label]" id="afrfq_phone_field_label"  value="<?php echo esc_attr($file_label); ?>" />
									<p><?php echo esc_html__('Label of the File/Image Upload field.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Allowed Types', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input class="afrfq_input_class" type="text" name="afrfq_fields[4][file_allowed_types]" id="afrfq_file_allowed_types"  value="<?php echo esc_attr($file_allowed_types); ?>" />
									<p><?php echo esc_html__('Allowed file upload types. e.g (png,jpg,txt). Add comma separated, please do not use dot(.).', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<input type="hidden" name="afrfq_fields[4][field_key]" value="afrfq_file_field">
						</tbody>
					</table>
				</p>
			</div>

			<!-- Message Field -->

	<?php

				$message_field_array = get_option('afrfq_fields');
				
	if (!empty($message_field_array[5]['enable_field'])) {
		$message_is_enabled = $message_field_array[5]['enable_field'];
	} else {
		$message_is_enabled = '';
	}
				
	if (!empty($message_field_array[5]['field_required'])) {
		$message_is_required = $message_field_array[5]['field_required'];
	} else {
		$message_is_required = '';
	}
				
	if (!empty($message_field_array[5]['field_sort_order'])) {
		$message_sort_order = $message_field_array[5]['field_sort_order'];
	} else {
		$message_sort_order = '';
	}
				
	if (!empty($message_field_array[5]['field_label'])) {
		$message_label = $message_field_array[5]['field_label'];
	} else {
		$message_label = '';
	}
				
				
				
				
	?>

			<h3><?php echo esc_html__('Message Field', 'addify_b2b'); ?></h3>
			<div>
				<p>
					<table class="addify-table-optoin">
						<tbody>
							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Enable Message Field', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input type="checkbox" name="afrfq_fields[5][enable_field]" id="afrfq_enable_message_field" value="yes" <?php echo checked('yes', esc_attr($message_is_enabled)); ?> />
									<p><?php echo esc_html__('Enable Message field on the Request a Quote Form.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('is Required?', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input type="checkbox" name="afrfq_fields[5][field_required]" id="afrfq_message_field_required" value="yes" <?php echo  checked('yes', esc_attr($message_is_required)); ?> />
									<p><?php echo esc_html__('Check if you want to make Message field required.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Sort Order', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input class="afrfq_input_class" type="number" name="afrfq_fields[5][field_sort_order]" id="afrfq_message_field_sortorder" min="0" value="<?php echo esc_attr($message_sort_order); ?>" />
									<p><?php echo esc_html__('Sort Order of the Message field.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Label', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input class="afrfq_input_class" type="text" name="afrfq_fields[5][field_label]" id="afrfq_message_field_label"  value="<?php echo esc_attr($message_label); ?>" />
									<p><?php echo esc_html__('Label of the Message field.', 'addify_b2b'); ?></p>
								</td>
							</tr>


							<input type="hidden" name="afrfq_fields[5][field_key]" value="afrfq_message_field">
						</tbody>
					</table>
				</p>
			</div>

			<!-- Additional Form Field 1 -->

	<?php

				$field1_field_array = get_option('afrfq_fields');

	if (!empty($field1_field_array[6]['enable_field'])) {
		$field1_is_enabled = $field1_field_array[6]['enable_field'];
	} else {
		$field1_is_enabled = '';
	}
				
	if (!empty($field1_field_array[6]['field_required'])) {
		$field1_is_required = $field1_field_array[6]['field_required'];
	} else {
		$field1_is_required = '';
	}
				
	if (!empty($field1_field_array[6]['field_sort_order'])) {
		$field1_sort_order = $field1_field_array[6]['field_sort_order'];
	} else {
		$field1_sort_order = '';
	}
				
	if (!empty($field1_field_array[6]['field_label'])) {
		$field1_label = $field1_field_array[6]['field_label'];
	} else {
		$field1_label = '';
	}
				
				
				
				
	?>

			<h3><?php echo esc_html__('Field 1', 'addify_b2b'); ?></h3>

			<div>
				<p>
					<table class="addify-table-optoin">
						<tbody>
							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Enable Field 1', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input type="checkbox" name="afrfq_fields[6][enable_field]" id="afrfq_enable_field1_field" value="yes" <?php echo checked('yes', esc_attr($field1_is_enabled)); ?> />
									<p><?php echo esc_html__('Enable Additional Field 1 on the Request a Quote Form.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('is Required?', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input type="checkbox" name="afrfq_fields[6][field_required]" id="afrfq_field1_field_required" value="yes" <?php echo  checked('yes', esc_attr($field1_is_required)); ?> />
									<p><?php echo esc_html__('Check if you want to make Additional Field 1 field required.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Sort Order', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input class="afrfq_input_class" type="number" name="afrfq_fields[6][field_sort_order]" id="afrfq_field1_field_sortorder" min="0" value="<?php echo esc_attr($field1_sort_order); ?>" />
									<p><?php echo esc_html__('Sort Order of the Additional Field 1 field.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Label', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input class="afrfq_input_class" type="text" name="afrfq_fields[6][field_label]" id="afrfq_field1_field_label"  value="<?php echo esc_attr($field1_label); ?>" />
									<p><?php echo esc_html__('Label of the Additional Field 1 field.', 'addify_b2b'); ?></p>
								</td>
							</tr>


							<input type="hidden" name="afrfq_fields[6][field_key]" value="afrfq_field1_field">
						</tbody>
					</table>
				</p>
			</div>

			<!-- Additional Field 2 -->

	<?php

				$field2_field_array = get_option('afrfq_fields');
				
	if (!empty($field2_field_array[7]['enable_field'])) {
		$field2_is_enabled = $field2_field_array[7]['enable_field'];
	} else {
		$field2_is_enabled = '';
	}
				
	if (!empty($field2_field_array[7]['field_required'])) {
		$field2_is_required = $field2_field_array[7]['field_required'];
	} else {
		$field2_is_required = '';
	}
				
	if (!empty($field2_field_array[7]['field_sort_order'])) {
		$field2_sort_order = $field2_field_array[7]['field_sort_order'];
	} else {
		$field2_sort_order = '';
	}
				
	if (!empty($field2_field_array[7]['field_label'])) {
		$field2_label = $field2_field_array[7]['field_label'];
	} else {
		$field2_label = '';
	}
				
				
				
				
	?>

			<h3><?php echo esc_html__('Field 2', 'addify_b2b'); ?></h3>

			<div>
				<p>
					<table class="addify-table-optoin">
						<tbody>
							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Enable Field 2', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input type="checkbox" name="afrfq_fields[7][enable_field]" id="afrfq_enable_field2_field" value="yes" <?php echo checked('yes', esc_attr($field2_is_enabled)); ?> />
									<p><?php echo esc_html__('Enable Additional Field 2 on the Request a Quote Form.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('is Required?', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input type="checkbox" name="afrfq_fields[7][field_required]" id="afrfq_field2_field_required" value="yes" <?php echo  checked('yes', esc_attr($field2_is_required)); ?> />
									<p><?php echo esc_html__('Check if you want to make Additional Field 2 field required.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Sort Order', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input class="afrfq_input_class" type="number" name="afrfq_fields[7][field_sort_order]" id="afrfq_field2_field_sortorder" min="0" value="<?php echo esc_attr($field2_sort_order); ?>" />
									<p><?php echo esc_html__('Sort Order of the Additional Field 2 field.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Label', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input class="afrfq_input_class" type="text" name="afrfq_fields[7][field_label]" id="afrfq_field2_field_label"  value="<?php echo esc_attr($field2_label); ?>" />
									<p><?php echo esc_html__('Label of the Additional Field 2 field.', 'addify_b2b'); ?></p>
								</td>
							</tr>


							<input type="hidden" name="afrfq_fields[7][field_key]" value="afrfq_field2_field">
						</tbody>
					</table>
				</p>
			</div>

			<!-- Additional Field 3 -->

	<?php

				$field3_field_array = get_option('afrfq_fields');
				
	if (!empty($field3_field_array[8]['enable_field'])) {
		$field3_is_enabled = $field3_field_array[8]['enable_field'];
	} else {
		$field3_is_enabled = '';
	}
				
	if (!empty($field3_field_array[8]['field_required'])) {
		$field3_is_required = $field3_field_array[8]['field_required'];
	} else {
		$field3_is_required = '';
	}
				
	if (!empty($field3_field_array[8]['field_sort_order'])) {
		$field3_sort_order = $field3_field_array[8]['field_sort_order'];
	} else {
		$field3_sort_order = '';
	}
				
	if (!empty($field3_field_array[8]['field_label'])) {
		$field3_label = $field3_field_array[8]['field_label'];
	} else {
		$field3_label = '';
	}
				
				
				
				
	?>

			<h3><?php echo esc_html__('Field 2', 'addify_b2b'); ?></h3>

			<div>
				<p>
					<table class="addify-table-optoin">
						<tbody>
							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Enable Field 3', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input type="checkbox" name="afrfq_fields[8][enable_field]" id="afrfq_enable_field3_field" value="yes" <?php echo checked('yes', esc_attr($field3_is_enabled)); ?> />
									<p><?php echo esc_html__('Enable Additional Field 3 on the Request a Quote Form.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('is Required?', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input type="checkbox" name="afrfq_fields[8][field_required]" id="afrfq_field3_field_required" value="yes" <?php echo  checked('yes', esc_attr($field3_is_required)); ?> />
									<p><?php echo esc_html__('Check if you want to make Additional Field 3 field required.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Sort Order', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input class="afrfq_input_class" type="number" name="afrfq_fields[8][field_sort_order]" id="afrfq_field3_field_sortorder" min="0" value="<?php echo esc_attr($field3_sort_order); ?>" />
									<p><?php echo esc_html__('Sort Order of the Additional Field 3 field.', 'addify_b2b'); ?></p>
								</td>
							</tr>

							<tr class="addify-option-field">
								<th>
									<div class="option-head">
										<b><?php echo esc_html__('Label', 'addify_b2b'); ?></b>
									</div>
								</th>
								<td>
									<input class="afrfq_input_class" type="text" name="afrfq_fields[8][field_label]" id="afrfq_field3_field_label"  value="<?php echo esc_attr($field3_label); ?>" />
									<p><?php echo esc_html__('Label of the Additional Field 3 field.', 'addify_b2b'); ?></p>
								</td>
							</tr>


							<input type="hidden" name="afrfq_fields[8][field_key]" value="afrfq_field3_field">
						</tbody>
					</table>
				</p>
			</div>


		</div>
	</div>

	<?php 
} // function afreg_page_1_section_callback


//Tab 10
function afrfq_page_10_section_callback() {
	?>
	<div class="afb2b_setting_div">
		<p><?php echo esc_html__('Manage the redirect to quote page after Add to Quote and redirect to any page after quote form submission .', 'addify_b2b'); ?></p>
	</div>

	<?php 
} // function afreg_page_3_section_callback

function afrfq_redirect_to_quote_callback( $args) {
	?>
	<input type="checkbox" id="afrfq_redirect_to_quote" class="setting_fields" name="afrfq_redirect_to_quote" value="quote" <?php echo checked('quote', esc_attr(get_option('afrfq_redirect_to_quote'))); ?> >
	<p class="description afrfq_redirect_to_quote"> <?php echo esc_attr($args[0]); ?> </p>
	<?php      
} // end  

function afrfq_redirect_after_submission_callback( $args) {
	?>
	<input type="checkbox" id="afrfq_redirect_after_submission" class="setting_fields" name="afrfq_redirect_after_submission" value="yes" <?php echo checked('yes', esc_attr(get_option('afrfq_redirect_after_submission'))); ?> >
	<p class="description afrfq_redirect_after_submission"> <?php echo esc_attr($args[0]); ?> </p>
	<?php      
}

function afrfq_redirect_url_callback( $args) {
	?>
	<input type="text" id="afrfq_redirect_url" class="setting_fields" name="afrfq_redirect_url" value="<?php echo esc_url(get_option('afrfq_redirect_url')); ?>" >
	<p class="description afrfq_redirect_url"> <?php echo esc_attr($args[0]); ?> </p>
	<?php      
}





//Tab 3
function afrfq_page_3_section_callback() {
	?>
	<div class="afb2b_setting_div">
		<p><?php echo esc_html__('Manage Google reCaptcha settings.', 'addify_b2b'); ?></p>
	</div>

	<?php 
} // function afreg_page_3_section_callback

function afrfq_enable_captcha_callback( $args) {
	?>
	<input type="checkbox" id="afrfq_enable_captcha" class="setting_fields" name="afrfq_enable_captcha" value="yes" <?php echo checked('yes', esc_attr(get_option('afrfq_enable_captcha'))); ?> >
	<p class="description afrfq_enable_captcha"> <?php echo esc_attr($args[0]); ?> </p>
	<?php      
} // end afrfq_enable_captcha_callback 

function afrfq_site_key_callback( $args) {
	?>
	<input type="text" id="afrfq_site_key" class="setting_fields" name="afrfq_site_key" value="<?php echo esc_attr(get_option('afrfq_site_key')); ?>">
	<p class="description afrfq_site_key"> <?php echo esc_attr($args[0]); ?> </p>
	<?php      
} // end afrfq_site_key_callback 

function afrfq_secret_key_callback( $args) {
	?>
	<input type="text" id="afrfq_secret_key" class="setting_fields" name="afrfq_secret_key" value="<?php echo esc_attr(get_option('afrfq_secret_key')); ?>">
	<p class="description afrfq_secret_key"> <?php echo esc_attr($args[0]); ?> </p>
	<?php      
} // end afrfq_site_key_callback 
