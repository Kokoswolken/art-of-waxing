<?php

add_settings_section(
	'page_1_section',         // ID used to identify this section and with which to register options
	'',   // Title to be displayed on the administration page
	'aftax_page_1_section_callback', // Callback used to render the description of the section
	'addify-aftax-1'                           // Page on which to add this section of options
);

add_settings_field(
	'aftax_enable_auto_tax_exempttion',                      // ID used to identify the field throughout the theme
	esc_html__( 'Remove Tax Automatically', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_enable_auto_tax_exempttion_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-1',                          // The page on which this option will be displayed
	'page_1_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'Enable/Disable tax for tax-exempted and approved users.', 'addify_b2b' ) . '<br>' . esc_html__( 'Enable this checkbox will disable tax for all tax-exempted and approved users.', 'addify_b2b' ) . '<br>' . esc_html__( 'Disable this checkbox will show a checkbox in the checkout page to notify them that tax exemption is available', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-1',
	'aftax_enable_auto_tax_exempttion'
);

add_settings_field(
	'aftax_enable_text_field',                      // ID used to identify the field throughout the theme
	esc_html__( 'Enable Text Field', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_enable_text_field_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-1',                          // The page on which this option will be displayed
	'page_1_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'This text field will be shown in tax form in user my account page. This field can be used to collect name, tax id etc.', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-1',
	'aftax_enable_text_field'
);

add_settings_field(
	'aftax_text_field_label',                      // ID used to identify the field throughout the theme
	esc_html__( 'Text Field Label', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_text_field_label_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-1',                          // The page on which this option will be displayed
	'page_1_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'Label of text field.', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-1',
	'aftax_text_field_label'
);

add_settings_field(
	'aftax_enable_textarea_field',                      // ID used to identify the field throughout the theme
	esc_html__( 'Enable Textarea Field', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_enable_textarea_field_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-1',                          // The page on which this option will be displayed
	'page_1_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'This textarea field will be shown in tax form in user my account page. This field can be used to collect additional info etc.', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-1',
	'aftax_enable_textarea_field'
);

add_settings_field(
	'aftax_textarea_field_label',                      // ID used to identify the field throughout the theme
	esc_html__( 'Textarea Field Label', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_textarea_field_label_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-1',                          // The page on which this option will be displayed
	'page_1_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'Label of textarea field.', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-1',
	'aftax_textarea_field_label'
);

add_settings_field(
	'aftax_enable_fileupload_field',                      // ID used to identify the field throughout the theme
	esc_html__( 'Enable File Upload Field', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_enable_fileupload_field_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-1',                          // The page on which this option will be displayed
	'page_1_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'This file upload field will be shown in tax form in user my account page. This field can be used to collect tax certificate etc.', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-1',
	'aftax_enable_fileupload_field'
);

add_settings_field(
	'aftax_fileupload_field_label',                      // ID used to identify the field throughout the theme
	esc_html__( 'File Upload Field Label', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_fileupload_field_label_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-1',                          // The page on which this option will be displayed
	'page_1_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'Label of file upload field.', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-1',
	'aftax_fileupload_field_label'
);

add_settings_field(
	'aftax_allowed_file_types',                      // ID used to identify the field throughout the theme
	esc_html__( 'Allowed File Types', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_allowed_file_types_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-1',                          // The page on which this option will be displayed
	'page_1_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'Specify allowed file types. Add comma(,) separated values like doc,pdf , etc. to allow multiple file types.', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-1',
	'aftax_allowed_file_types'
);



function aftax_page_1_section_callback( $args ) {
	?>
	<div class="afb2b_setting_div">
		   <p><?php echo esc_html__( 'In general settings you can enable/disable tax for specific users and choose which field(s) you want to show on the tax exemption request form.', 'addify_b2b' ); ?></p>
	</div>
	
	<?php
}

function aftax_enable_auto_tax_exempttion_callback( $args ) {
	?>
	<input type="checkbox" name="aftax_enable_auto_tax_exempttion" id="aftax_enable_auto_tax_exempttion" value="yes" <?php echo checked( 'yes', esc_attr( get_option( 'aftax_enable_auto_tax_exempttion' ) ) ); ?> />
	<p class="description afreg_additional_fields_section_title"> <?php echo wp_kses_post( $args[0] ); ?> </p>
	<?php
}

function aftax_enable_text_field_callback( $args ) {
	$values = is_array( get_option( 'aftax_enable_text_field' ) ) ? get_option( 'aftax_enable_text_field' ) : array();
	?>
	<input type="checkbox" name="aftax_enable_text_field[]" id="aftax_enable_text_field" value="enable" 
	<?php
	if ( in_array( 'enable', $values ) ) {
		echo 'checked';
	}
	?>
																										 /> <?php echo esc_html__( 'Enable', 'addify_b2b' ); ?>
	<input type="checkbox" name="aftax_enable_text_field[]" id="aftax_enable_text_field" value="required" 
	<?php
	if ( in_array( 'required', $values ) ) {
		echo 'checked';
	}
	?>
																										   /> <?php echo esc_html__( 'Required', 'addify_b2b' ); ?>
	<p class="description afreg_additional_fields_section_title"> <?php echo esc_attr( $args[0] ); ?> </p>
	<?php
}

function aftax_text_field_label_callback( $args ) {
	?>
	<input type="text" class="afrfq_input_class" name="aftax_text_field_label" id="aftax_text_field_label" value="<?php echo esc_attr( get_option( 'aftax_text_field_label' ) ); ?>" />
	<p class="description afreg_additional_fields_section_title"> <?php echo esc_attr( $args[0] ); ?> </p>

	<?php
}

function aftax_enable_textarea_field_callback( $args ) {
	$values = is_array( get_option( 'aftax_enable_textarea_field' ) ) ? get_option( 'aftax_enable_textarea_field' ) : array();
	?>
	<input type="checkbox" name="aftax_enable_textarea_field[]" id="aftax_enable_textarea_field" value="enable" 
	<?php
	if ( in_array( 'enable', $values ) ) {
		echo 'checked';
	}
	?>
																												 /> <?php echo esc_html__( 'Enable', 'addify_b2b' ); ?>
	<input type="checkbox" name="aftax_enable_textarea_field[]" id="aftax_enable_textarea_field" value="required" 
	<?php
	if ( in_array( 'required', $values ) ) {
		echo 'checked';
	}
	?>
																												   /> <?php echo esc_html__( 'Required', 'addify_b2b' ); ?>
	<p class="description afreg_additional_fields_section_title"> <?php echo esc_attr( $args[0] ); ?> </p>
	<?php
}

function aftax_textarea_field_label_callback( $args ) {
	?>
	<input type="text" class="afrfq_input_class" name="aftax_textarea_field_label" id="aftax_textarea_field_label" value="<?php echo esc_attr( get_option( 'aftax_textarea_field_label' ) ); ?>" />
	<p class="description afreg_additional_fields_section_title"> <?php echo esc_attr( $args[0] ); ?> </p>
	
	<?php
}

function aftax_enable_fileupload_field_callback( $args ) {

	$values = is_array( get_option( 'aftax_enable_fileupload_field' ) ) ? get_option( 'aftax_enable_fileupload_field' ) : array();
	?>
	<input type="checkbox" name="aftax_enable_fileupload_field[]" id="aftax_enable_fileupload_field" value="enable" 
	<?php
	if ( in_array( 'enable', $values ) ) {
		echo 'checked';
	}
	?>
																													 /> <?php echo esc_html__( 'Enable', 'addify_b2b' ); ?>
	<input type="checkbox" name="aftax_enable_fileupload_field[]" id="aftax_enable_fileupload_field" value="required" 
	<?php
	if ( in_array( 'required', $values ) ) {
		echo 'checked';
	}
	?>
																													   /> <?php echo esc_html__( 'Required', 'addify_b2b' ); ?>
	<p class="description afreg_additional_fields_section_title"> <?php echo esc_attr( $args[0] ); ?> </p>
	
	<?php
}

function aftax_fileupload_field_label_callback( $args ) {
	?>
	<input type="text" class="afrfq_input_class" name="aftax_fileupload_field_label" id="aftax_fileupload_field_label" value="<?php echo esc_attr( get_option( 'aftax_fileupload_field_label' ) ); ?>" />
	<p class="description afreg_additional_fields_section_title"> <?php echo esc_attr( $args[0] ); ?> </p>
	
	<?php

}

function aftax_allowed_file_types_callback( $args ) {

	?>
	<input type="text" class="afrfq_input_class" name="aftax_allowed_file_types" id="aftax_allowed_file_types" value="<?php echo esc_attr( get_option( 'aftax_allowed_file_types' ) ); ?>" />
	<p class="description afreg_additional_fields_section_title"> <?php echo esc_attr( $args[0] ); ?> </p>
	
	<?php
}
