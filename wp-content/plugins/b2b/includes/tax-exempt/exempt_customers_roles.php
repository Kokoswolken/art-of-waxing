<?php

add_settings_section(
	'page_2_section',         // ID used to identify this section and with which to register options
	'',   // Title to be displayed on the administration page
	'aftax_page_2_section_callback', // Callback used to render the description of the section
	'addify-aftax-2'                           // Page on which to add this section of options
);

add_settings_field(
	'aftax_exempted_customers',                      // ID used to identify the field throughout the theme
	esc_html__( 'Choose Customers', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_exempted_customers_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-2',                          // The page on which this option will be displayed
	'page_2_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'Enable/Disable tax for tax-exempted and approved users.', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-2',
	'aftax_exempted_customers'
);

add_settings_field(
	'aftax_exempted_user_roles',                      // ID used to identify the field throughout the theme
	esc_html__( 'Select User Roles', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_exempted_user_roles_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-2',                          // The page on which this option will be displayed
	'page_2_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'Choose user roles to grant them tax exemption status.', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-2',
	'aftax_exempted_user_roles'
);



function aftax_page_2_section_callback( $args ) {
	?>
	<div class="afb2b_setting_div">
		   <p><?php echo esc_html__( 'In this section, you can specify the customers and user roles who are exempted from tax. These customers and roles are not required to fill the tax form from "My Account" page.', 'addify_b2b' ); ?></p>
	</div>
	 
	<?php
}

function aftax_exempted_customers_callback( $args ) {
	?>
	<select name="aftax_exempted_customers[]" id="aftax_exempted_customers" multiple="multiple">
	<?php
	$afcustomers = get_option( 'aftax_exempted_customers' );

	if ( ! empty( $afcustomers ) && is_array( $afcustomers ) ) {
		foreach ( $afcustomers as $usr ) {
			$author_obj = get_user_by( 'id', $usr );
			?>

		<option value="<?php echo intval( $usr ); ?>" selected="selected"><?php echo esc_attr( $author_obj->display_name ); ?>(<?php echo esc_attr( $author_obj->user_email ); ?>)</option>

			<?php
		}
	}
	?>

	</select>
	<p class="description afreg_additional_fields_section_title"> <?php echo esc_attr( $args[0] ); ?> </p>
	<?php
}

function aftax_exempted_user_roles_callback( $args ) {
	?>
	<div class="all_cats">
		<ul>

	<?php
	$aftax_exempted_user_roles = ( get_option( 'aftax_exempted_user_roles' ) );
	global $wp_roles;
	$roles = $wp_roles->get_names();
	foreach ( $roles as $key => $value ) {
		?>

				<li class="par_cat">
					<input type="checkbox" class="parent" name="aftax_exempted_user_roles[]" id="aftax_exempted_user_roles" value="<?php echo esc_attr( $key ); ?>" 
		<?php
		if ( ! empty( $aftax_exempted_user_roles ) && in_array( $key, (array) $aftax_exempted_user_roles ) ) {
			echo 'checked';
		}
		?>
					/>
		<?php echo esc_attr( $value ); ?>
				</li>

	<?php } ?>
				<li class="par_cat">
					<input type="checkbox" class="parent" name="aftax_exempted_user_roles[]" id="aftax_exempted_user_roles" value="guest" 
		<?php
		if ( ! empty( $aftax_exempted_user_roles ) && in_array( 'guest', (array) $aftax_exempted_user_roles ) ) {
			echo 'checked';
		}
		?>
					/>
		<?php echo esc_attr( 'Guest' ); ?>
				</li>

		</ul>
	</div>
	 
	<p class="description afreg_additional_fields_section_title"> <?php echo esc_attr( $args[0] ); ?> </p>
	<?php
}
