<?php

add_settings_section(
	'page_4_section',         // ID used to identify this section and with which to register options
	'',   // Title to be displayed on the administration page
	'aftax_page_4_section_callback', // Callback used to render the description of the section
	'addify-aftax-4'                           // Page on which to add this section of options
);

add_settings_field(
	'aftax_admin_email',                      // ID used to identify the field throughout the theme
	esc_html__( 'Admin/Shop Manager Email', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_admin_email_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-4',                          // The page on which this option will be displayed
	'page_4_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'All admin emails that are related to our module will be sent to this email address.', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-4',
	'aftax_admin_email'
);

add_settings_field(
	'aftax_add_tax_info_message',                      // ID used to identify the field throughout the theme
	esc_html__( 'Add/Update Tax Info Message', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_add_tax_info_message_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-4',                          // The page on which this option will be displayed
	'page_4_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'This message will be shown when user add or update tax info in my account.', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-4',
	'aftax_add_tax_info_message'
);


add_settings_field(
	'aftax_admin_email_subject',                      // ID used to identify the field throughout the theme
	esc_html__( 'Admin Email Subject', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_admin_email_subject_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-4',                          // The page on which this option will be displayed
	'page_4_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'This subject will be used when a user add or update tax info from my account.', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-4',
	'aftax_admin_email_subject'
);

add_settings_field(
	'aftax_admin_email_message',                      // ID used to identify the field throughout the theme
	esc_html__( 'Admin Email Message', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_admin_email_message_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-4',                          // The page on which this option will be displayed
	'page_4_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'This message will be used when a user add or update tax info from my account.', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-4',
	'aftax_admin_email_message'
);

add_settings_field(
	'aftax_approve_tax_info_email_subject',                      // ID used to identify the field throughout the theme
	esc_html__( 'Approve Tax Info Email Subject', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_approve_tax_info_email_subject_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-4',                          // The page on which this option will be displayed
	'page_4_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'This subject will be used when admin approves submitted tax info.', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-4',
	'aftax_approve_tax_info_email_subject'
);

add_settings_field(
	'aftax_approve_tax_info_email_message',                      // ID used to identify the field throughout the theme
	esc_html__( 'Approve Tax Info Email Message', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_approve_tax_info_email_message_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-4',                          // The page on which this option will be displayed
	'page_4_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'This message will be used when admin approves submitted tax info.', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-4',
	'aftax_approve_tax_info_email_message'
);

add_settings_field(
	'aftax_disapprove_tax_info_email_subject',                      // ID used to identify the field throughout the theme
	esc_html__( 'Disapprove Tax Info Email Subject', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_disapprove_tax_info_email_subject_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-4',                          // The page on which this option will be displayed
	'page_4_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'This subject will be used when admin disapprove submitted tax info.', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-4',
	'aftax_disapprove_tax_info_email_subject'
);

add_settings_field(
	'aftax_disapprove_tax_info_email_message',                      // ID used to identify the field throughout the theme
	esc_html__( 'Disapprove Tax Info Email Message', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_disapprove_tax_info_email_message_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-4',                          // The page on which this option will be displayed
	'page_4_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'This message will be used when admin disapprove submitted tax info.', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-4',
	'aftax_disapprove_tax_info_email_message'
);

function aftax_page_4_section_callback( $args ) {
	?>
	<div class="afb2b_setting_div">
		   <p><?php echo esc_html__( 'Email & Notification Settings', 'addify_b2b' ); ?></p>
	</div>
	 
	<?php
}

function aftax_admin_email_callback( $args ) {
	?>
	<input type="email" class="afrfq_input_class" name="aftax_admin_email" id="aftax_admin_email" value="<?php echo esc_attr( get_option( 'aftax_admin_email' ) ); ?>" />
	<p class="description afreg_additional_fields_section_title"> <?php echo esc_attr( $args[0] ); ?> </p>
	<?php
}


function aftax_add_tax_info_message_callback( $args ) {
	?>
	<textarea class="afrfq_input_class" name="aftax_add_tax_info_message" id="aftax_add_tax_info_message" rows="7"><?php echo esc_attr( get_option( 'aftax_add_tax_info_message' ) ); ?></textarea>
	<p class="description afreg_additional_fields_section_title"> <?php echo esc_attr( $args[0] ); ?> </p>
	<?php
}

function aftax_admin_email_subject_callback( $args ) {
	?>
	<input type="text" class="afrfq_input_class" name="aftax_admin_email_subject" id="aftax_admin_email_subject" value="<?php echo esc_attr( get_option( 'aftax_admin_email_subject' ) ); ?>" />
	<p class="description afreg_additional_fields_section_title"> <?php echo esc_attr( $args[0] ); ?> </p>
	<?php
}


function aftax_admin_email_message_callback( $args ) {
	?>
	<?php

	$content   = get_option( 'aftax_admin_email_message' );
	$editor_id = 'aftax_admin_email_message';
	$settings  = array( 'textarea_name' => 'aftax_admin_email_message' );

	wp_editor( $content, $editor_id, $settings );

	?>
	<p class="description afreg_additional_fields_section_title"> <?php echo esc_attr( $args[0] ); ?> </p>
	<?php
}

function aftax_approve_tax_info_email_subject_callback( $args ) {
	?>
	<input type="text" class="afrfq_input_class" name="aftax_approve_tax_info_email_subject" id="aftax_approve_tax_info_email_subject" value="<?php echo esc_attr( get_option( 'aftax_approve_tax_info_email_subject' ) ); ?>" />
	<p class="description afreg_additional_fields_section_title"> <?php echo esc_attr( $args[0] ); ?> </p>
	<?php
}


function aftax_approve_tax_info_email_message_callback( $args ) {
	?>
	<?php

	$content   = get_option( 'aftax_approve_tax_info_email_message' );
	$editor_id = 'aftax_approve_tax_info_email_message';
	$settings  = array( 'textarea_name' => 'aftax_approve_tax_info_email_message' );

	wp_editor( $content, $editor_id, $settings );

	?>
	<p class="description afreg_additional_fields_section_title"> <?php echo esc_attr( $args[0] ); ?> </p>
	<?php
}

function aftax_disapprove_tax_info_email_subject_callback( $args ) {
	?>
	<input type="text" class="afrfq_input_class" name="aftax_disapprove_tax_info_email_subject" id="aftax_disapprove_tax_info_email_subject" value="<?php echo esc_attr( get_option( 'aftax_disapprove_tax_info_email_subject' ) ); ?>" />
	<p class="description afreg_additional_fields_section_title"> <?php echo esc_attr( $args[0] ); ?> </p>
	<?php
}


function aftax_disapprove_tax_info_email_message_callback( $args ) {
	?>
	<?php

	$content   = get_option( 'aftax_disapprove_tax_info_email_message' );
	$editor_id = 'aftax_disapprove_tax_info_email_message';
	$settings  = array( 'textarea_name' => 'aftax_disapprove_tax_info_email_message' );

	wp_editor( $content, $editor_id, $settings );

	?>
	<p class="description afreg_additional_fields_section_title"> <?php echo esc_attr( $args[0] ); ?> </p>
	<?php
}
