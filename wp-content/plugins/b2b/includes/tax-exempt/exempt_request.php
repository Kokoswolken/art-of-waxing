<?php

add_settings_section(
	'page_3_section',         // ID used to identify this section and with which to register options
	'',   // Title to be displayed on the administration page
	'aftax_page_3_section_callback', // Callback used to render the description of the section
	'addify-aftax-3'                           // Page on which to add this section of options
);

add_settings_field(
	'aftax_requested_roles',                      // ID used to identify the field throughout the theme
	esc_html__( 'Select User Roles', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_requested_roles_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-3',                          // The page on which this option will be displayed
	'page_3_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
	)
);
register_setting(
	'aftax_setting-group-3',
	'aftax_requested_roles'
);

add_settings_field(
	'aftax_enable_auto_tax_exempt',                      // ID used to identify the field throughout the theme
	esc_html__( 'Auto Approve Tax Exempt Request', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_enable_auto_tax_exempt_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-3',                          // The page on which this option will be displayed
	'page_3_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'If this option is checked then tax exempt requests will be auto-approved and users of above selected user roles will be eligible for tax exempt right after submitting the info.', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-3',
	'aftax_enable_auto_tax_exempt'
);


add_settings_field(
	'aftax_enable_tax_exm_msg',                      // ID used to identify the field throughout the theme
	esc_html__( 'Show Tax Exemption Message on Checkout Page', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_enable_tax_exm_msg_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-3',                          // The page on which this option will be displayed
	'page_3_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'If this option is checked then a message will be displayed for the above selected user role users about tax exemption.', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-3',
	'aftax_enable_tax_exm_msg'
);

add_settings_field(
	'aftax_role_message_text',                      // ID used to identify the field throughout the theme
	esc_html__( 'Message Text', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_role_message_text_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-3',                          // The page on which this option will be displayed
	'page_3_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'This will be visible for the user roles customer has selected above.', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-3',
	'aftax_role_message_text'
);

function aftax_page_3_section_callback( $args ) {
	?>
	<div class="afb2b_setting_div">
		<p><?php echo esc_html__( 'Select user roles for whom you want to display tax exemption form in "My Account" page.', 'addify_b2b' ); ?></p>
	</div>
	 
	<?php
}

function aftax_requested_roles_callback( $args ) {
	?>
	<div class="all_cats">
		<ul>

	<?php
	$afuserroles = get_option( 'aftax_requested_roles' );
	global $wp_roles;
	$roles = $wp_roles->get_names();
	foreach ( $roles as $key => $value ) {
		?>

				<li class="par_cat">
					<input type="checkbox" class="parent" name="aftax_requested_roles[]" id="aftax_requested_roles" value="<?php echo esc_attr( $key ); ?>" 
		<?php
		if ( is_array( $afuserroles ) && in_array( $key, $afuserroles ) ) {
			echo 'checked';
		}
		?>
					/>
		<?php echo esc_attr( $value ); ?>
				</li>

	<?php } ?>

		</ul>
	</div>
	
	<?php
}

function aftax_enable_auto_tax_exempt_callback( $args ) {
	?>
	<input type="checkbox" name="aftax_enable_auto_tax_exempt" id="aftax_enable_auto_tax_exempt" value="yes" <?php echo checked( 'yes', esc_attr( get_option( 'aftax_enable_auto_tax_exempt' ) ) ); ?> />
	<p class="description afreg_additional_fields_section_title"> <?php echo esc_attr( $args[0] ); ?> </p>
	<?php
}

function aftax_enable_tax_exm_msg_callback( $args ) {
	?>
	<input type="checkbox" name="aftax_enable_tax_exm_msg" id="aftax_enable_tax_exm_msg" value="yes" <?php echo checked( 'yes', esc_attr( get_option( 'aftax_enable_tax_exm_msg' ) ) ); ?> />
	<p class="description afreg_additional_fields_section_title"> <?php echo esc_attr( $args[0] ); ?> </p>
	<?php
}

function aftax_role_message_text_callback( $args ) {
	?>
	<?php

	$content   = get_option( 'aftax_role_message_text' );
	$editor_id = 'aftax_role_message_text';
	$settings  = array( 'textarea_name' => 'aftax_role_message_text' );

	wp_editor( $content, $editor_id, $settings );

	?>
	<p class="description afreg_additional_fields_section_title"> <?php echo esc_attr( $args[0] ); ?> </p>
	<?php
}
