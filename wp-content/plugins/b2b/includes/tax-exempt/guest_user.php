<?php

add_settings_section(
	'page_5_section',         // ID used to identify this section and with which to register options
	'',   // Title to be displayed on the administration page
	'aftax_page_5_section_callback', // Callback used to render the description of the section
	'addify-aftax-5'                           // Page on which to add this section of options
);

add_settings_field(
	'aftax_enable_guest_message',                      // ID used to identify the field throughout the theme
	esc_html__( 'Show tax exemption message', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_enable_guest_message_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-5',                          // The page on which this option will be displayed
	'page_5_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'If this option is checked then a message will be displayed for guest user about tax exemption.', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-5',
	'aftax_enable_guest_message'
);

add_settings_field(
	'aftax_guest_message_text',                      // ID used to identify the field throughout the theme
	esc_html__( 'Message Text', 'addify_b2b' ),    // The label to the left of the option interface element
	'aftax_guest_message_text_callback',   // The name of the function responsible for rendering the option interface
	'addify-aftax-5',                          // The page on which this option will be displayed
	'page_5_section',         // The name of the section to which this field belongs
	array(                              // The array of arguments to pass to the callback. In this case, just a description.
		esc_html__( 'This message will be displayed for guest users.', 'addify_b2b' ),
	)
);
register_setting(
	'aftax_setting-group-5',
	'aftax_guest_message_text'
);

function aftax_page_5_section_callback( $args ) {
	?>
	<div class="afb2b_setting_div">
		   <p><?php echo esc_html__( 'Show tax exemption message for guest users.', 'addify_b2b' ); ?></p>
	</div>
	 
	<?php
}

function aftax_enable_guest_message_callback( $args ) {
	?>
	<input type="checkbox" name="aftax_enable_guest_message" id="aftax_enable_guest_message" value="yes" <?php echo checked( 'yes', esc_attr( get_option( 'aftax_enable_guest_message' ) ) ); ?> />
	<p class="description afreg_additional_fields_section_title"> <?php echo esc_attr( $args[0] ); ?> </p>
	<?php
}
function aftax_guest_message_text_callback( $args ) {
	?>
	<?php

	$content   = get_option( 'aftax_guest_message_text' );
	$editor_id = 'aftax_guest_message_text';
	$settings  = array( 'textarea_name' => 'aftax_guest_message_text' );

	wp_editor( $content, $editor_id, $settings );

	?>
	<p class="description afreg_additional_fields_section_title"> <?php echo esc_attr( $args[0] ); ?> </p>
	<?php
}
