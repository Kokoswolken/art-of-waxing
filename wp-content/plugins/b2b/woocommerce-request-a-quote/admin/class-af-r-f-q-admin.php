<?php
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! class_exists( 'AF_R_F_Q_Admin' ) ) {

	class AF_R_F_Q_Admin extends Addify_Request_For_Quote {

		public $errors;

		public function __construct() {

			// Enqueue Scripts.
			add_action( 'admin_enqueue_scripts', array( $this, 'afrfq_admin_scripts' ) );

			// Custom meta boxes.
			add_action( 'add_meta_boxes', array( $this, 'afrfq_add_metaboxes' ) );
			add_action( 'admin_init', array( $this, 'afrfq_register_metaboxes' ), 10 );

			// Add menus.
			add_action( 'admin_menu', array( $this, 'afrfq_custom_menu_admin' ) );

			// Save custom post types.
			add_action( 'save_post_addify_rfq', array( $this, 'afrfq_meta_box_save' ) );
			add_action( 'save_post_addify_quote', array( $this, 'afrfq_update_quote_details' ) );
			add_action( 'save_post_addify_rfq_fields', array( $this, 'afrfq_update_fields_meta' ) );

			// Manage table of Quotes.
			add_filter( 'manage_addify_quote_posts_columns', array( $this, 'addify_quote_columns_head' ) );
			add_action( 'manage_addify_quote_posts_custom_column', array( $this, 'addify_quote_columns_content' ), 10, 2 );

			// Manage table of quote fields.
			add_filter( 'manage_addify_rfq_fields_posts_columns', array( $this, 'addify_quote_fields_columns_head' ) );
			add_action( 'manage_addify_rfq_fields_posts_custom_column', array( $this, 'addify_quote_fields_columns_content' ), 10, 2 );

			// Manage table of quote rules.
			add_filter( 'manage_addify_rfq_posts_columns', array( $this, 'addify_quote_rules_columns_head' ) );
			add_action( 'manage_addify_rfq_posts_custom_column', array( $this, 'addify_quote_rules_columns_content' ), 10, 2 );

			// Module Settings.
			add_action( 'admin_init', array( $this, 'af_r_f_q_setting_files' ), 10 );

			// Add variation level settings for out of stock.
			add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'af_r_f_q_variable_fields' ), 10, 3 );
			add_action( 'woocommerce_save_product_variation', array( $this, 'af_r_f_q_save_custom_field_variations' ), 10, 2 );

			// Add Custom fields in page attributes for fields.
			add_action( 'page_attributes_misc_attributes', array( $this, 'addify_afrfq_add_fields' ) );
		}

		public function af_r_f_q_setting_files() {
			include_once AFRFQ_PLUGIN_DIR . '/admin/settings/tabs/general.php';
			include_once AFRFQ_PLUGIN_DIR . '/admin/settings/tabs/custom-message.php';
			include_once AFRFQ_PLUGIN_DIR . '/admin/settings/tabs/captcha-settings.php';
			include_once AFRFQ_PLUGIN_DIR . '/admin/settings/tabs/emails-setting.php';
			include_once AFRFQ_PLUGIN_DIR . '/admin/settings/tabs/editors-settings.php';
			include_once AFRFQ_PLUGIN_DIR . '/admin/settings/tabs/quote-attributes.php';
			include_once AFRFQ_PLUGIN_DIR . '/admin/settings/tabs/button.php';
		}

		public function af_r_f_q_variable_fields( $loop, $variation_data, $variation ) {

			if ( 'yes' == get_post_meta( $variation->ID, 'disable_rfq', true ) ) {
				$value = 'disabled';
			} elseif ( get_post_meta( $variation->ID, 'disable_rfq', true ) ) {
				$value = get_post_meta( $variation->ID, 'disable_rfq', true );
			} else {
				$value = 'show';
			}

			wp_nonce_field('afrfq_nonce_action' . $variation->ID, 'afrfq_nonce_field' . $variation->ID );

			woocommerce_wp_radio(
				array(
					'id'          => "disable_rfq$loop",
					'name'        => 'disable_rfq[' . $variation->ID . ']',
					'value'       => $value,
					'label'       => __( 'Show/Hide/Disable Quote', 'addify_b2b' ),
					'options'     => array( 
						'show'          => __( 'Show Quote button if rule applicable', 'addify_b2b' ),
						'disabled'      => __( 'Disable Quote Button', 'addify_b2b' ),
						'disabled_swap' => __( 'Disable Quote and show Add to Cart Button', 'addify_b2b' ),
						'hide'          => __( 'Hide Quote Button', 'addify_b2b' ),
						'hide_swap'     => __( 'Hide Quote and show Add to Cart Button', 'addify_b2b' ),
					),
					'desc_tip'    => true,
					'description' => __( 'show/Disable/hide/replace request a quote for above variation.', 'addify_b2b' ),
				)
			);
		}

		public function af_r_f_q_save_custom_field_variations( $variation_id, $loop ) {

			if ( !is_ajax() ) {
				if ( !isset( $_POST['afrfq_field_nonce' . $variation_id ] ) || ! wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['afrfq_field_nonce' . $variation_id ] ) ), 'afrfq_nonce_action' . $variation_id ) ) {
					die( esc_html__('RFQ Security violated', 'addify_b2b') );
				}
			}

			if ( isset( $_POST['disable_rfq'][ $variation_id ] ) ) {
				update_post_meta( $variation_id, 'disable_rfq', sanitize_text_field( wp_unslash( $_POST['disable_rfq'][ $variation_id ] ) ) );
			} else {
				update_post_meta( $variation_id, 'disable_rfq', 'show' );
			}
		}

		public function addify_afrfq_add_fields() {
			global $post;

			if ( 'addify_rfq_fields' !== $post->post_type ) {
				return;
			}

			$post_id = $post->ID;

			$afrfq_field_enable   = get_post_meta( $post_id, 'afrfq_field_enable', true );
			$afrfq_field_required = get_post_meta( $post_id, 'afrfq_field_required', true );

			?>
			<p class="post-attributes-label-wrapper afrfq-label-wrapper">
				<label class="post-attributes-label" for="quote_status">
					<?php esc_html_e( 'Enable/Disable', 'addify_b2b' ); ?>
				</label>
			</p>
				<select name="afrfq_field_enable" id="afrfq_field_enable" >
					<option value="enable" <?php echo selected( 'enable', $afrfq_field_enable ); ?> > <?php esc_html_e( 'Enable', 'addify_b2b' ); ?></option>
					<option value="disable" <?php echo selected( 'disable', $afrfq_field_enable ); ?> > <?php esc_html_e( 'Disable', 'addify_b2b' ); ?></option>
				</select>
			<p class="post-attributes-label-wrapper afrfq-label-wrapper">
				<label class="post-attributes-label" for="quote_status">
					<?php esc_html_e( 'Required', 'addify_b2b' ); ?>
				</label>
			</p>
			<input type="checkbox" value="yes" <?php echo checked( 'yes', $afrfq_field_required ); ?> name="afrfq_field_required">
			<?php
		}

		public function afrfq_update_fields_meta( $post_id ) {

			//For custom post type:
			$exclude_statuses = array(
				'auto-draft',
				'trash'
			);

			$action = isset( $_GET['action'] ) ? sanitize_text_field( wp_unslash( $_GET['action'] ) ) : '';

			if ( in_array( get_post_status($post_id), $exclude_statuses ) || is_ajax() || 'untrash' === $action ) {
				return;
			}

			// if our nonce isn't there, or we can't verify it, return
			if ( empty( $_POST['afrfq_field_nonce'] ) || ! wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['afrfq_field_nonce'] ) ), 'afrfq_field_nonce' ) ) {
				die( esc_html__('Site Security Violated', 'addify_b2b') );
			}

			try {

				$form_data = sanitize_meta( '', wp_unslash( $_POST ), '' );

				$validation = true;

				$quote_fields_obj = new AF_R_F_Q_Quote_Fields();

				if ( isset( $form_data['afrfq_field_name'] ) && ! $quote_fields_obj->afrfq_validate_field_name( $form_data['afrfq_field_name'], $post_id ) ) {
					throw new Exception( __( 'Field name should be unique for each field.', 'addify_b2b' ), 1 );
				}

				if ( ! empty( $form_data ) ) {
					include_once AFRFQ_PLUGIN_DIR . 'admin/meta-boxes/fields/save-fields.php';
				}
			} catch ( Exception $e ) {

				echo esc_html( 'Error: ' . $e->getMessage() );
			}

		}

		public function afrfq_update_quote_details( $post_id ) {

			//For custom post type:
			$exclude_statuses = array(
				'auto-draft',
				'trash'
			);

			$action = isset( $_GET['action'] ) ? sanitize_text_field( wp_unslash( $_GET['action'] ) ) : '';

			if ( in_array( get_post_status($post_id), $exclude_statuses ) || is_ajax() || 'untrash' === $action ) {
				return;
			}

			// if our nonce isn't there, or we can't verify it, return
			if ( empty( $_POST['afrfq_field_nonce'] ) || ! wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['afrfq_field_nonce'] ) ), 'afrfq_field_nonce' ) ) {
				die( esc_html__('Site Security Violated', 'addify_b2b') );
			}

			$form_data = sanitize_meta( '', wp_unslash( $_POST ), '' );

			if ( !empty( $form_data ) ) {
				include_once AFRFQ_PLUGIN_DIR . 'admin/meta-boxes/quotes/update-quote.php';
			}

			if ( isset( $form_data['addify_convert_to_order'] ) ) {
				include_once AFRFQ_PLUGIN_DIR . 'admin/meta-boxes/quotes/convert-quote-to-order.php';
			}
		}


		public function afrfq_admin_scripts() {

			$screen = get_current_screen();

			if ( ! in_array( $screen->post_type, array( 'addify_rfq', 'addify_quote', 'addify_rfq_fields' ), true )  && 'toplevel_page_addify-b2b' !== $screen->id ) {
				return;
			}

			wp_enqueue_style( 'afrfq-adminc', AFRFQ_URL . '/assets/css/afrfq_admin.css', false, '1.0' );
			wp_enqueue_style( 'select2', AFRFQ_URL . '/assets/css/select2.css', false, '1.0' );

			wp_enqueue_script( 'select2', AFRFQ_URL . '/assets/js/select2.js', array( 'jquery' ), '1.0', true );
			wp_enqueue_script( 'afrfq-adminj', AFRFQ_URL . '/assets/js/afrfq_admin.js', array( 'jquery' ), '1.0', true );

			wp_enqueue_script('jquery-ui-accordion');
			$afrfq_data = array(
				'admin_url' => admin_url( 'admin-ajax.php' ),
				'nonce'     => wp_create_nonce( 'afquote-ajax-nonce' ),

			);
			wp_localize_script( 'afrfq-adminj', 'afrfq_php_vars', $afrfq_data );

			wp_enqueue_script( 'jquery-ui-tabs' );
			wp_enqueue_style( 'thickbox' );
			wp_enqueue_script( 'thickbox' );
			wp_enqueue_script( 'media-upload' );
			wp_enqueue_media();
		}

		public function afrfq_register_metaboxes() {

			add_meta_box(
				'afrfq-rule-settings',
				esc_html__( 'Rule Settings', 'addify_b2b' ),
				array( $this, 'afrfq_rule_setting_callback' ),
				'addify_rfq',
				'normal',
				'high'
			);

		}

		public function afrfq_add_metaboxes() {

			add_meta_box(
				'afrfq-user-info',
				esc_html__( 'Customer Information', 'addify_b2b' ),
				array( $this, 'afrfq_customer_info_callback' ),
				'addify_quote',
				'normal',
				'high'
			);

			add_meta_box(
				'afrfq-quote-info',
				esc_html__( 'Quote Details', 'addify_b2b' ),
				array( $this, 'afrfq_quote_info_callback' ),
				'addify_quote',
				'normal',
				'high'
			);

			add_meta_box(
				'afrfq-quote-status',
				esc_html__( 'Quote Attributes', 'addify_b2b' ),
				array( $this, 'afrfq_quote_status_callback' ),
				'addify_quote',
				'side',
				'high'
			);

			// Add meta boxes for fields.
			add_meta_box(
				'afrfq-field-attributes',
				esc_html__( 'Field Attributes and Values', 'addify_b2b' ),
				array( $this, 'afrfq_field_attribute_callback' ),
				'addify_rfq_fields',
				'normal',
				'high'
			);
		}

		public function afrfq_field_attribute_callback() {
			wp_nonce_field( 'afrfq_field_nonce', 'afrfq_field_nonce' );
			include_once AFRFQ_PLUGIN_DIR . 'admin/meta-boxes/fields/field-attribute.php';
		}

		public function afrfq_customer_info_callback() {

			$quote_fields_obj = new AF_R_F_Q_Quote_Fields();
			$quote_fields     = (array) $quote_fields_obj->quote_fields;

			include_once AFRFQ_PLUGIN_DIR . 'admin/meta-boxes/quotes/customer-info.php';
		}

		public function afrfq_quote_status_callback() {
			include_once AFRFQ_PLUGIN_DIR . 'admin/meta-boxes/quotes/quote-status.php';
		}

		public function afrfq_quote_info_callback() {

			global $post;

			if ( ! empty( get_post_meta( $post->ID, 'quote_proid', true ) ) ) {
				include_once AFRFQ_PLUGIN_DIR . 'admin/templates/addify-afrfq-edit-form.php';
			} else {
				include_once AFRFQ_PLUGIN_DIR . 'admin/meta-boxes/quotes/quote-details.php';
			}
		}

		public function afrfq_rule_setting_callback() {
			global $post;
			wp_nonce_field( 'afrfq_field_nonce', 'afrfq_field_nonce' );
			$afrfq_rule_type          = get_post_meta( intval( $post->ID ), 'afrfq_rule_type', true );
			$afrfq_rule_priority      = get_post_meta( intval( $post->ID ), 'afrfq_rule_priority', true );
			$afrfq_hide_products      = unserialize( get_post_meta( intval( $post->ID ), 'afrfq_hide_products', true ) );
			$afrfq_hide_categories    = unserialize( get_post_meta( intval( $post->ID ), 'afrfq_hide_categories', true ) );
			$afrfq_hide_user_role     = unserialize( get_post_meta( intval( $post->ID ), 'afrfq_hide_user_role', true ) );
			$afrfq_is_hide_price      = get_post_meta( intval( $post->ID ), 'afrfq_is_hide_price', true );
			$afrfq_hide_price_text    = get_post_meta( intval( $post->ID ), 'afrfq_hide_price_text', true );
			$afrfq_is_hide_addtocart  = get_post_meta( intval( $post->ID ), 'afrfq_is_hide_addtocart', true );
			$afrfq_custom_button_text = get_post_meta( intval( $post->ID ), 'afrfq_custom_button_text', true );
			$afrfq_form               = get_post_meta( intval( $post->ID ), 'afrfq_form', true );
			$afrfq_contact7_form      = get_post_meta( intval( $post->ID ), 'afrfq_contact7_form', true );
			$afrfq_custom_button_link = get_post_meta( intval( $post->ID ), 'afrfq_custom_button_link', true );

			include_once AFRFQ_PLUGIN_DIR . 'admin/meta-boxes/rules/new-quote-rule.php';
		}

		public function afrfq_meta_box_save( $post_id ) {

			//For custom post type:
			$exclude_statuses = array(
				'auto-draft',
				'trash'
			);

			$action = isset( $_GET['action'] ) ? sanitize_text_field( wp_unslash( $_GET['action'] ) ) : '';

			if ( in_array( get_post_status($post_id), $exclude_statuses ) || is_ajax() || 'untrash' === $action ) {
				return;
			}

			include_once AFRFQ_PLUGIN_DIR . 'admin/meta-boxes/rules/save-rule-settings.php';
		}

		public function afrfq_custom_menu_admin() {

			if ( defined('AFB2B_PLUGIN_DIR')) {
				return;
			}
			
			add_menu_page(
				esc_html__( 'Request a Quote', 'addify_b2b' ),
				esc_html__( 'Request a Quote', 'addify_b2b' ),
				'manage_options',
				'edit.php?post_type=addify_quote',
				'',
				AFRFQ_URL . 'assets/images/grey.png',
				'30'
			);

			add_submenu_page(
				'edit.php?post_type=addify_quote',
				esc_html__( 'All Quote', 'addify_b2b' ),
				esc_html__( 'All Quotes', 'addify_b2b' ),
				'manage_options',
				'edit.php?post_type=addify_quote',
				''
			);

			add_submenu_page(
				'edit.php?post_type=addify_quote',
				esc_html__( 'All Quote Rules', 'addify_b2b' ),
				esc_html__( 'Quote Rules', 'addify_b2b' ),
				'manage_options',
				'edit.php?post_type=addify_rfq',
				''
			);

			add_submenu_page(
				'edit.php?post_type=addify_quote',
				esc_html__( 'ALL Quote Fields', 'addify_b2b' ),
				esc_html__( 'Quote Fields', 'addify_b2b' ),
				'manage_options',
				'edit.php?post_type=addify_rfq_fields',
				''
			);

			add_submenu_page(
				'edit.php?post_type=addify_quote',
				esc_html__( 'Settings', 'addify_b2b' ),
				esc_html__( 'Settings', 'addify_b2b' ),
				'manage_options',
				'af-rfq-settings',
				array( $this, 'af_r_f_q_settings' )
			);
		}

		public function af_r_f_q_settings() {
			include_once AFRFQ_PLUGIN_DIR . 'admin/settings/settings.php';
		}

		public function afrfq_author_admin_notice() {
			?>
			<div class="updated notice notice-success is-dismissible">
				<p><?php echo esc_html__( 'Settings saved successfully.', 'addify_b2b' ); ?></p>
			</div>
			<?php
		}

		public function addify_quote_rules_columns_head( $columns ) {

			unset( $columns['date'] );
			$columns['af_users']    = __( 'User Roles', 'addify_b2b' );
			$columns['af_price']    = __( 'Hide Price', 'addify_b2b' );
			$columns['af_btn']      = __( 'Button Text', 'addify_b2b' );
			$columns['af_priority'] = __( 'Rule Priority', 'addify_b2b' );
			$columns['date']        = __( 'Date', 'addify_b2b' );
			return $columns;
		}

		public function addify_quote_rules_columns_content( $column_name, $post_id ) {
			switch ( $column_name ) {
				case 'af_users':
					echo esc_attr( implode( ', ', (array) unserialize( (string) get_post_meta( $post_id, 'afrfq_hide_user_role', true ) ) ) );
					break;
				case 'af_btn':
					echo esc_attr( get_post_meta( $post_id, 'afrfq_custom_button_text', true ) );
					break;
				case 'af_priority':
					global $post;
					echo esc_attr( $post->menu_order );
					break;
				case 'af_price':
					echo esc_attr( 'yes' === get_post_meta( $post_id, 'afrfq_is_hide_price', true ) ? 'Yes' : 'No' );
					break;
			}
		}

		public function addify_quote_fields_columns_head( $columns ) {
			unset( $columns['date'] );
			$columns['af_label']   = __( 'Label', 'addify_b2b' );
			$columns['af_type']    = __( 'Type', 'addify_b2b' );
			$columns['af_name']    = __( 'Meta Key/ Field Name', 'addify_b2b' );
			$columns['af_default'] = __( 'Default Value', 'addify_b2b' );
			$columns['af_order']   = __( 'Display Order', 'addify_b2b' );
			$columns['af_satus']   = __( 'Status', 'addify_b2b' );
			$columns['af_requi']   = __( 'Required', 'addify_b2b' );
			$columns['date']       = __( 'Date', 'addify_b2b' );
			return $columns;
		}

		public function addify_quote_fields_sortable_columns( $columns ) {

			$columns['af_order'] = __( 'Display Order', 'addify_b2b' );
			return $columns;
		}

		public function addify_quote_fields_columns_content( $column_name, $post_id ) {
			switch ( $column_name ) {
				case 'af_label':
					echo esc_attr( ucwords( get_post_meta( $post_id, 'afrfq_field_label', true ) ) );
					break;

				case 'af_type':
					echo esc_attr( ucwords( get_post_meta( $post_id, 'afrfq_field_type', true ) ) );
					break;
				case 'af_name':
					echo esc_attr( get_post_meta( $post_id, 'afrfq_field_name', true ) );
					break;
				case 'af_default':
					echo esc_attr( ucwords( str_replace( '_', ' ', get_post_meta( $post_id, 'afrfq_field_value', true ) ) ) );
					break;
				case 'af_order':
					global $post;
					echo esc_attr( $post->menu_order );
					break;
				case 'af_satus':
					echo esc_attr( ucwords( get_post_meta( $post_id, 'afrfq_field_enable', true ) ) );
					break;
				case 'af_requi':
					echo esc_attr( 'yes' === get_post_meta( $post_id, 'afrfq_field_required', true ) ? 'Yes' : 'No' );
					break;
			}
		}

		public function addify_quote_columns_head( $columns ) {

			$new_columns           = array();
			$new_columns['cb']     = '<input type="checkbox" />';
			$new_columns['title']  = esc_html__( 'Quote #', 'addify_b2b' );
			$new_columns['name']   = esc_html__( 'Customer Name', 'addify_b2b' );
			$new_columns['email']  = esc_html__( 'Customer Email', 'addify_b2b' );
			$new_columns['status'] = esc_html__( 'Quote Status', 'addify_b2b' );
			$new_columns['date']   = esc_html__( 'Date', 'addify_b2b' );
			return $new_columns;

		}

		public function addify_quote_columns_content( $column_name, $post_ID ) {

			$af_fields_obj = new AF_R_F_Q_Quote_Fields();

			switch ( $column_name ) {
				case 'name':
					echo esc_attr( $af_fields_obj->afrfq_get_user_name( $post_ID ) );
					break;

				case 'email':
					echo esc_attr( $af_fields_obj->afrfq_get_user_email( $post_ID ) );
					break;
				case 'status':
					echo esc_attr( ucwords( str_replace( 'af_', '', get_post_meta( $post_ID, 'quote_status', true ) ) ) );
					break;
			}

		}
	}

	new AF_R_F_Q_Admin();
}
