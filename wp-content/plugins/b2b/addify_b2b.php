<?php
/*
 * Plugin Name:       B2B for WooCommerce
 * Plugin URI:        https://woocommerce.com/products/b2b-for-woocommerce/
 * Description:       WooCommerce B2B plugin offers merchants a complete wholesale solution to optimize their website for both B2B & B2C customers. (PLEASE TAKE BACKUP BEFORE UPDATING THE PLUGIN).
 * Version:           2.1.2
 * Author:            Addify
 * Developed By:      Addify
 * Author URI:        http://www.addifypro.com
 * Support:           http://www.addifypro.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 * Text Domain:       addify_b2b
 *
 * Woo: 5357367:5c361a5af01db5982ef558962a2ee426
 *
 * WC requires at least: 3.0.9
 * WC tested up to: 7.*.*
 */
// Exit if accessed directly
if (! defined('ABSPATH') ) {
	exit;
}

if ( !is_multisite() ) {
	if (!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins'))) ) {

		function afb2b_admin_notice() {

			$afpvu_allowed_tags = array(
				'a' => array(
					'class' => array(),
					'href' => array(),
					'rel' => array(),
					'title' => array(),
				),
				'b' => array(),
				'div' => array(
					'class' => array(),
					'title' => array(),
					'style' => array(),
				),
				'p' => array(
					'class' => array(),
				),
				'strong' => array(),
			);

			// Deactivate the plugin
			deactivate_plugins(__FILE__);

			$afpvu_woo_check = '<div id="message" class="error">
				<p><strong>' . __('WooCommerce B2B plugin is inactive', 'addify_b2b' ) . '</strong>' . __('The', 'addify_b2b') . '<a href="http://wordpress.org/extend/plugins/woocommerce/">' . __('WooCommerce plugin', 'addify_b2b' ) . '</a> ' . __('must be active for this plugin to work. Please install and activate WooCommerce', 'addify_b2b' ) . '»</p></div>';
			echo wp_kses(__($afpvu_woo_check, 'addify_b2b'), $afpvu_allowed_tags);
		}

		add_action('admin_notices', 'afb2b_admin_notice');
	}
}

if (!class_exists('Addify_B2B_Plugin') ) {

	class Addify_B2B_Plugin {
	

		public function __construct() {

			$this->afb2b_global_constents_vars();
			$this->set_media_path_url();
			
			register_activation_hook(__FILE__, array($this, 'afb2b_add_prod_visbility_page'));

			add_action('wp_loaded', array( $this, 'afb2b_init' ));

			add_action('init', array($this, 'afb2b_custom_post_types' ));

			add_action('wp_ajax_get_states', array($this, 'get_states'));
			add_action('wp_ajax_nopriv_get_states', array($this, 'get_states')); 

			add_action('wp_ajax_aftaxsearchUsers', array($this, 'aftaxsearchUsers'));

			add_filter( 'woocommerce_email_classes', array( $this, 'afreg_emails' ), 90, 1 );

			if ( extension_loaded('soap') ) {
				require_once plugin_dir_path(__FILE__) . 'vendor/autoload.php';
			} else {
				add_action('admin_notices', array( $this, 'add_admin_notice_for_soap') );
			}

			include_once AFB2B_PLUGIN_DIR . 'woocommerce-request-a-quote/class-addify-request-for-quote.php';
			include_once AFB2B_PLUGIN_DIR . 'products-visibility-by-user-roles/addify_product_visibility.php';
			include_once AFB2B_PLUGIN_DIR . 'addify-order-restrictions/addify-order-restrictions.php';

			if (is_admin() ) {
				include_once AFB2B_PLUGIN_DIR . 'class_afb2b_admin.php';
				include_once AFB2B_PLUGIN_DIR . 'additional_classes/class_afb2b_registration_admin.php';
				include_once AFB2B_PLUGIN_DIR . 'additional_classes/class_afb2b_role_based_pricing_admin.php';
				include_once AFB2B_PLUGIN_DIR . 'additional_classes/class-afb2b-shipping-front.php';
				include_once AFB2B_PLUGIN_DIR . 'additional_classes/class_aftax_admin.php';
			} else {
	
				include_once AFB2B_PLUGIN_DIR . 'additional_classes/class_afb2b_registration_front.php';
				include_once AFB2B_PLUGIN_DIR . 'additional_classes/class_af_tax_display.php';
				include_once AFB2B_PLUGIN_DIR . 'additional_classes/class_afb2b_role_based_pricing_front.php';
				include_once AFB2B_PLUGIN_DIR . 'additional_classes/class-afb2b-shipping-front.php';
				include_once AFB2B_PLUGIN_DIR . 'additional_classes/class-afb2b-payments-front.php';
				include_once AFB2B_PLUGIN_DIR . 'additional_classes/class-aftax-front.php';
			}
		}
		
		public function add_admin_notice_for_soap() {
			?>
			<div id="message" class="error">
				<p>
					<a href="https://woocommerce.com/products/b2b-for-woocommerce/">
						<?php esc_html_e('B2B for WooComerce:', 'addify_b2b'); ?>
					</a>
					<?php esc_html_e(' Kindly activate soap application from your server to validate VIES VAT number validation.', 'addify_b2b'); ?>
				</p>
			</div>
			<?php
		}

		private function set_media_path_url() {

			$upload_dir = wp_upload_dir();

			$upload_path = $upload_dir['basedir'] . '/addify-tax-exempt/';

			if ( !is_dir( $upload_path ) ) {
				mkdir( $upload_path );
			}

			$upload_url = $upload_dir['baseurl'] . '/addify-tax-exempt/';

			if ( ! defined( 'AFTAX_MEDIA_URL' ) ) {
				define( 'AFTAX_MEDIA_URL', $upload_url);
			}

			if ( ! defined( 'AFTAX_MEDIA_PATH' ) ) {
				define( 'AFTAX_MEDIA_PATH', $upload_path);
			}
		}

		public function get_states() {

			if (isset($_POST['nonce']) && '' != $_POST['nonce']) {

				$nonce = sanitize_text_field($_POST['nonce']);
			} else {
				$nonce = 0;
			}

			if (! wp_verify_nonce($nonce, 'afreg-ajax-nonce') ) {

				die( esc_html__('Security Violated', 'addify_b2b') );
			}

			if (!empty($_POST['country'])) {

				$country = sanitize_text_field($_POST['country']);
			}

			if (!empty($_POST['width'])) {

				$width = sanitize_text_field($_POST['width']);
			}

			if (!empty($_POST['name'])) {

				$name = sanitize_text_field($_POST['name']);
			}

			if (!empty($_POST['label'])) {

				$label = sanitize_text_field($_POST['label']);
			}

			if (!empty($_POST['message'])) {

				$message = sanitize_text_field($_POST['message']);
			}

			if (!empty($_POST['required'])) {

				$required = sanitize_text_field($_POST['required']);
			}

			if (!empty($_POST['af_state'])) {

				$af_state = sanitize_text_field($_POST['af_state']);
			}
			

			global $woocommerce;
			$countries_obj = new WC_Countries();
			$states        = $countries_obj->get_states($country);
			
			if (!empty($states) && !empty($country)) {
				?>

			<p id="dropdown_state" class="form-row <?php echo esc_attr($width); ?>">
				<label for="<?php echo esc_attr($name); ?>"><?php echo esc_html__( $label, 'addify_b2b' ); ?> 
					<?php 
					if (1 == $required) {
						?>
						 <span class="required">*</span> <?php } ?>
				</label>

				<select class="js-example-basic-single" name="billing_state">
					<option value=""><?php echo esc_html__('Select a county / state...', 'addify_b2b'); ?></option>
					
					<?php foreach ($states as $key => $value) { ?>
						<option value="<?php echo esc_attr($key); ?>" <?php echo selected($af_state, $key); ?>><?php echo esc_attr($value); ?></option>
					<?php } ?>
				</select>

				<?php if (isset($message) && ''!=$message) { ?>
					<span style="width:100%;float: left"><?php echo esc_html__($message, 'addify_b2b'); ?></span>
				<?php } ?>
			</p>

			<?php } elseif (is_array($states) && !empty($country)) { ?>
				
				<p id="dropdown_state" class="form-row <?php echo esc_attr($width); ?>">
					<input type="hidden" name="billing_state" value="<?php echo esc_attr($country); ?>" />
				</p>

			

			<?php } else { ?>
				<label for="<?php echo esc_attr($name); ?>"><?php echo esc_html__( $label, 'addify_b2b' ); ?> 
					<?php 
					if (1 == $required) {
						?>
						 <span class="required">*</span> <?php } ?>
				</label>
				<p id="dropdown_state" class="form-row <?php echo esc_attr($width); ?>">
					<input type="text" name="billing_state" value="<?php echo esc_attr($af_state); ?>" />
				</p>

			<?php } ?>

			<script type="text/javascript">
				jQuery(document).ready(function() {
					jQuery('.js-example-basic-single').select2();
				});

				  
				

			</script>


			<?php 
			die();
		}


		public function aftaxsearchUsers() {

			if (isset($_POST['nonce']) && '' != $_POST['nonce']) {

				$nonce = sanitize_text_field($_POST['nonce']);
			} else {
				$nonce = 0;
			}

			if (isset($_POST['q']) && '' != $_POST['q']) {

				if (! wp_verify_nonce($nonce, 'aftax-ajax-nonce') ) {

					die('Failed ajax security check!');
				}
				

				$pro = sanitize_text_field($_POST['q']);

			} else {

				$pro = '';

			}


			$data_array  = array();
			$users       = new WP_User_Query(
				array(
				'search'         => '*' . esc_attr($pro) . '*',
				'search_columns' => array(
				'user_login',
				'user_nicename',
				'user_email',
				'user_url',
				),
				) 
			);
			$users_found = $users->get_results();

			if (!empty($users_found)) {

				foreach ($users_found as $proo) {

					$title        = $proo->display_name . '(' . $proo->user_email . ')';
					$data_array[] = array( $proo->ID, $title ); // array( User ID, User name and email )
				}
			}
			
			echo json_encode($data_array);

			die();

		}

		public function afb2b_global_constents_vars() {

			if (!defined('AFB2B_URL') ) {
				define('AFB2B_URL', plugin_dir_url(__FILE__));
			}

			if (!defined('AFB2B_BASENAME') ) {
				define('AFB2B_BASENAME', plugin_basename(__FILE__));
			}

			if (! defined('AFB2B_PLUGIN_DIR') ) {
				define('AFB2B_PLUGIN_DIR', plugin_dir_path(__FILE__));
			}
		}

		public function afb2b_init() {

			if (function_exists('load_plugin_textdomain') ) {
				load_plugin_textdomain('addify_b2b', false, dirname(plugin_basename(__FILE__)) . '/languages/');
			}
		}

		public function afb2b_add_prod_visbility_page() {

			$upload_url = wp_upload_dir();


			if (!is_dir($upload_url['basedir'] . '/addify_registration_uploads')) {
				mkdir($upload_url['basedir'] . '/addify_registration_uploads', 0777, true);
			}

			//Product Visibility error page
			if (null == get_page_by_path('af-product-visibility')) {

				$new_page = array(
				'post_status' => 'publish',
				'post_type' => 'page',
				'post_author' => 1,
				'post_name' => esc_html__('af-product-visibility', 'addify_b2b'),
				'post_title' => esc_html__('Products Visibility', 'addify_b2b'),
				'post_content' => '[addify-product-visibility-page]',
				'post_parent' => 0,
				'comment_status' => 'closed'
				);

				$page_id = wp_insert_post($new_page);

				update_option('addify_pvu_page_id', $page_id);
			} else {
				$page_id = get_page_by_path('af-product-visibility');
				update_option('addify_pvu_page_id', $page_id->ID);
			}

			//Request a quote page.

			if (null == get_page_by_path('request-a-quote')) {

				$new_page = array(
				'post_status' => 'publish',
				'post_type' => 'page',
				'post_author' => 1,
				'post_name' => esc_html__('request-a-quote', 'addify_b2b'
				),
				'post_title' => esc_html__('Request a Quote', 'addify_b2b'
				),
				'post_content' => '[addify-quote-request-page]',
				'post_parent' => 0,
				'comment_status' => 'closed'
				);

				$page_id = wp_insert_post($new_page);

				update_option('addify_atq_page_id', $page_id);
			} else {
				$page_id = get_page_by_path('request-a-quote');
				update_option('addify_atq_page_id', $page_id);
			}

			if (empty(get_option('afrfq_fields'))) {

				$newval = array();

				$enable_field       = 'yes';
				$field_required     = 'yes';
				$field_label        = 'Name';
				$field_sort_order   = 1;
				$file_allowed_types = '';
				$field_key          = 'afrfq_name_field';

				$enable_field1       = 'yes';
				$field_required1     = 'yes';
				$field_label1        = 'Email';
				$field_sort_order1   = 2;
				$file_allowed_types1 = '';
				$field_key1          = 'afrfq_email_field';

				$enable_field2       = 'yes';
				$field_required2     = 'yes';
				$field_label2        = 'Message';
				$field_sort_order2   = 3;
				$file_allowed_types2 = '';
				$field_key2          = 'afrfq_message_field';

				$newval['field_0'] = array('enable_field' => $enable_field, 'field_required' => $field_required, 'field_label' => $field_label, 'field_sort_order' => $field_sort_order, 'field_key' => $field_key, 'file_allowed_types' => $file_allowed_types);

				$newval['field_1'] = array('enable_field' => $enable_field1, 'field_required' => $field_required1, 'field_label' => $field_label1, 'field_sort_order' => $field_sort_order1, 'field_key' => $field_key1, 'file_allowed_types' => $file_allowed_types1);

				$newval['field_2'] = array('enable_field' => $enable_field2, 'field_required' => $field_required2, 'field_label' => $field_label2, 'field_sort_order' => $field_sort_order2, 'field_key' => $field_key2, 'file_allowed_types' => $file_allowed_types2);

				update_option('afrfq_fields', serialize(sanitize_meta('afrfq_fields', $newval, '')));
			}

			update_option('afrfq_success_message', 'Your Quote Submitted Successfully.');
			update_option('afrfq_pro_success_message', 'Product Added to Quote Successfully.');
			update_option('afrfq_view_button_message', 'View Quote');
			update_option('afrfq_basket_option', 'dropdown');

			$this->afreg_insert_default_fields();

			$this->afreg_insert_emails_default_text();
			

			
		}

		public function afreg_insert_default_fields() {

			//First Name
			$first_name_posts = get_page_by_path( 'first_name', OBJECT, 'def_reg_fields' );
			if ('' == $first_name_posts) {
				$first_name_post = array(
					'post_title'   => 'First Name',
					'post_name'    => 'first_name',
					'post_type'    => 'def_reg_fields',
					'post_status'  => 'unpublish',
					'menu_order'   => 1        
				);
				$first_name_id   = wp_insert_post($first_name_post);
				update_post_meta($first_name_id, 'placeholder', 'Enter your first name');
				update_post_meta($first_name_id, 'is_required', 1);
				update_post_meta($first_name_id, 'width', 'half');
				update_post_meta($first_name_id, 'type', 'text');
				update_post_meta($first_name_id, 'message', '');
			}

			//Last Name
			$last_name_posts = get_page_by_path( 'last_name', OBJECT, 'def_reg_fields' );
			if ('' == $last_name_posts) {
				$last_name_post = array(
					'post_title'   => 'Last Name',
					'post_name'    => 'last_name',
					'post_type'    => 'def_reg_fields',
					'post_status'  => 'unpublish',
					'menu_order'   => 2        
				);
				$last_name_id   = wp_insert_post($last_name_post);
				update_post_meta($last_name_id, 'placeholder', 'Enter your last name');
				update_post_meta($last_name_id, 'is_required', 1);
				update_post_meta($last_name_id, 'width', 'half');
				update_post_meta($last_name_id, 'type', 'text');
				update_post_meta($last_name_id, 'message', '');
			}

			//Company
			$company_posts = get_page_by_path( 'billing_company', OBJECT, 'def_reg_fields' );
			if ('' == $company_posts) {
				$company_post = array(
					'post_title'   => 'Company',
					'post_name'    => 'billing_company',
					'post_type'    => 'def_reg_fields',
					'post_status'  => 'unpublish',
					'menu_order'   => 3       
				);
				$company_id   = wp_insert_post($company_post);
				update_post_meta($company_id, 'placeholder', 'Enter your company');
				update_post_meta($company_id, 'is_required', 0);
				update_post_meta($company_id, 'width', 'full');
				update_post_meta($company_id, 'type', 'text');
				update_post_meta($company_id, 'message', '');
			}


			//Country
			$country_posts = get_page_by_path( 'billing_country', OBJECT, 'def_reg_fields' );
			if ('' == $country_posts) {
				$country_post = array(
					'post_title'   => 'Country',
					'post_name'    => 'billing_country',
					'post_type'    => 'def_reg_fields',
					'post_status'  => 'unpublish',
					'menu_order'   => 4       
				);
				$country_id   = wp_insert_post($country_post);
				update_post_meta($country_id, 'placeholder', 'Select your country');
				update_post_meta($country_id, 'is_required', 1);
				update_post_meta($country_id, 'width', 'full');
				update_post_meta($country_id, 'type', 'select');
				update_post_meta($country_id, 'message', '');
			}

			//Address Line 1
			$address_1_posts = get_page_by_path( 'billing_address_1', OBJECT, 'def_reg_fields' );
			if ('' == $address_1_posts) {
				$address_1_post = array(
					'post_title'   => 'Street Address',
					'post_name'    => 'billing_address_1',
					'post_type'    => 'def_reg_fields',
					'post_status'  => 'unpublish',
					'menu_order'   => 5       
				);
				$address_1_id   = wp_insert_post($address_1_post);
				update_post_meta($address_1_id, 'placeholder', 'House number and street name');
				update_post_meta($address_1_id, 'is_required', 1);
				update_post_meta($address_1_id, 'width', 'full');
				update_post_meta($address_1_id, 'type', 'text');
				update_post_meta($address_1_id, 'message', '');
			}


			//Address Line 2
			$address_2_posts = get_page_by_path( 'billing_address_2', OBJECT, 'def_reg_fields' );
			if ('' == $address_2_posts) {
				$address_2_post = array(
					'post_title'   => 'Address 2',
					'post_name'    => 'billing_address_2',
					'post_type'    => 'def_reg_fields',
					'post_status'  => 'unpublish',
					'menu_order'   => 6       
				);
				$address_2_id   = wp_insert_post($address_2_post);
				update_post_meta($address_2_id, 'placeholder', 'Apartment, suite, unit etc. (optional)');
				update_post_meta($address_2_id, 'is_required', 0);
				update_post_meta($address_2_id, 'width', 'full');
				update_post_meta($address_2_id, 'type', 'text');
				update_post_meta($address_2_id, 'message', '');
			}

			//State
			$state_posts = get_page_by_path( 'billing_state', OBJECT, 'def_reg_fields' );
			if ('' == $state_posts) {
				$state_post = array(
					'post_title'   => 'State / County',
					'post_name'    => 'billing_state',
					'post_type'    => 'def_reg_fields',
					'post_status'  => 'unpublish',
					'menu_order'   => 7       
				);
				$state_id   = wp_insert_post($state_post);
				update_post_meta($state_id, 'placeholder', 'Select your state / county');
				update_post_meta($state_id, 'is_required', 1);
				update_post_meta($state_id, 'width', 'full');
				update_post_meta($state_id, 'type', 'select');
				update_post_meta($state_id, 'message', '');
			}


			//City
			$city_posts = get_page_by_path( 'billing_city', OBJECT, 'def_reg_fields' );
			if ('' == $city_posts) {
				$city_post = array(
					'post_title'   => 'Town / City',
					'post_name'    => 'billing_city',
					'post_type'    => 'def_reg_fields',
					'post_status'  => 'unpublish',
					'menu_order'   => 8       
				);
				$city_id   = wp_insert_post($city_post);
				update_post_meta($city_id, 'placeholder', 'Enter your city');
				update_post_meta($city_id, 'is_required', 1);
				update_post_meta($city_id, 'width', 'half');
				update_post_meta($city_id, 'type', 'text');
				update_post_meta($city_id, 'message', '');
			}


			//Post Code
			$postcode_posts = get_page_by_path( 'billing_postcode', OBJECT, 'def_reg_fields' );
			if ('' == $postcode_posts) {
				$postcode_post = array(
					'post_title'   => 'Postcode / Zip',
					'post_name'    => 'billing_postcode',
					'post_type'    => 'def_reg_fields',
					'post_status'  => 'unpublish',
					'menu_order'   => 9      
				);
				$postcode_id   = wp_insert_post($postcode_post);
				update_post_meta($postcode_id, 'placeholder', 'Enter your postcode / zip');
				update_post_meta($postcode_id, 'is_required', 1);
				update_post_meta($postcode_id, 'width', 'half');
				update_post_meta($postcode_id, 'type', 'text');
				update_post_meta($postcode_id, 'message', '');
			}

			//Phone
			$phone_posts = get_page_by_path( 'billing_phone', OBJECT, 'def_reg_fields' );
			if ('' == $phone_posts) {
				$phone_post = array(
					'post_title'   => 'Phone',
					'post_name'    => 'billing_phone',
					'post_type'    => 'def_reg_fields',
					'post_status'  => 'unpublish',
					'menu_order'   => 10      
				);
				$phone_id   = wp_insert_post($phone_post);
				update_post_meta($phone_id, 'placeholder', 'Enter your phone');
				update_post_meta($phone_id, 'is_required', 1);
				update_post_meta($phone_id, 'width', 'full');
				update_post_meta($phone_id, 'type', 'tel');
				update_post_meta($phone_id, 'message', '');
			}


		}


		public function afb2b_custom_post_types() {

			$labels = array(
			'name'                => esc_html__('Registration Fields', 'addify_b2b'),
			'singular_name'       => esc_html__('Registration Field', 'addify_b2b'),
			'add_new'             => esc_html__('Add New Field', 'addify_b2b'),
			'add_new_item'        => esc_html__('Add New Field', 'addify_b2b'),
			'edit_item'           => esc_html__('Edit Registration Field', 'addify_b2b'),
			'new_item'            => esc_html__('New Registration Field', 'addify_b2b'),
			'view_item'           => esc_html__('View Registration Field', 'addify_b2b'),
			'search_items'        => esc_html__('Search Registration Field', 'addify_b2b'),
			'exclude_from_search' => true,
			'not_found'           => esc_html__('No registration field found', 'addify_b2b'),
			'not_found_in_trash'  => esc_html__('No registration field found in trash', 'addify_b2b'),
			'parent_item_colon'   => '',
			'all_items'           => esc_html__('All Fields', 'addify_b2b'),
			'menu_name'           => esc_html__('Registration Fields', 'addify_b2b'),
			);
		
			$args = array(
			'labels' => $labels,
			'menu_icon' => 'dashicons-id',
			'public' => false,
			'publicly_queryable' => false,
			'show_ui' => true,
			'show_in_menu' => false,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'has_archive' => true,
			'hierarchical' => false,
			'menu_position' => 30,
			'rewrite' => array('slug' => 'addify_b2b', 'with_front'=>false ),
			'supports' => array('title')
			);
		
			register_post_type('afreg_fields', $args);



			$labels1 = array(
			'name'                => esc_html__('Request for Quote Rules', 'addify_b2b'),
			'singular_name'       => esc_html__('Request for Quote Rule', 'addify_b2b'),
			'add_new'             => esc_html__('Add New Rule', 'addify_b2b'),
			'add_new_item'        => esc_html__('Add New Rule', 'addify_b2b'),
			'edit_item'           => esc_html__('Edit Rule', 'addify_b2b'),
			'new_item'            => esc_html__('New Rule', 'addify_b2b'),
			'view_item'           => esc_html__('View Rule', 'addify_b2b'),
			'search_items'        => esc_html__('Search Rule', 'addify_b2b'),
			'exclude_from_search' => true,
			'not_found'           => esc_html__('No rule found', 'addify_b2b'),
			'not_found_in_trash'  => esc_html__('No rule found in trash', 'addify_b2b'),
			'parent_item_colon'   => '',
			'all_items'           => esc_html__('All Rules', 'addify_b2b'),
			'menu_name'           => esc_html__('Request for Quote', 'addify_b2b'),
			);

			$args1 = array(
			'labels' => $labels1,
			'menu_icon'  => '',
			'public' => false,
			'publicly_queryable' => false,
			'show_ui' => true,
			'show_in_menu' => false,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'has_archive' => true,
			'hierarchical' => false,
			'menu_position' => 30,
			'rewrite' => array('slug' => 'addify_b2b'
			, 'with_front'=>false ),
			'supports' => array('title')
			);

			register_post_type('addify_b2b'
			, $args1);

			register_post_type(
				'addify_quote',
				array(
				'public' => true,
				'publicly_queryable' => false,
				'show_in_menu' => false,
				'menu_position' => 30,
				'menu_icon' => 'dashicons-cart',
				'labels' => array(
						'name'  => esc_html__('All Quotes', 'addify_b2b'),
						'add_new'             => '',
						'add_new_item'        => '',
				),
				'supports' => array(''),

				)
			);

			//Role Based Pricing Custom Post Type

			$labels2 = array(
			'name'                => __('Role Based Pricing Rules', 'addify_b2b'),
			'singular_name'       => __('Role Based Pricing Rules', 'addify_b2b'),
			'add_new'             => __('Add New Rule', 'addify_b2b'),
			'add_new_item'        => __('Add Rule', 'addify_b2b'),
			'edit_item'           => __('Edit Rule', 'addify_b2b'),
			'new_item'            => __('New Rule', 'addify_b2b'),
			'view_item'           => __('View Rule', 'addify_b2b'),
			'search_items'        => __('Search Rule', 'addify_b2b'),
			'exclude_from_search' => true,
			'not_found'           => __('No rule found', 'addify_b2b'),
			'not_found_in_trash'  => __('No rule found in trash', 'addify_b2b'),
			'parent_item_colon'   => '',
			'all_items'           => __('All Rules', 'addify_b2b'),
			'menu_name'           => __('Role Based Pricing', 'addify_b2b'),
			);
	
			$args2 = array(
			'labels' => $labels2,
			'menu_icon'  => '',
			'public' => false,
			'publicly_queryable' => false,
			'show_ui' => true,
			'show_in_menu' => false,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'page', 
			'has_archive' => true,
			'hierarchical' => false,
			'menu_position' => 30,
			'rewrite' => array('slug' => 'csp-rule', 'with_front'=>false ),
			'supports' => array( 'title')
			);
	
			register_post_type('csp_rules', $args2);

		}

		public function afrfq_add_to_quote_callback_function() {

			global $wp_session;

			

			if (isset($_POST['nonce']) && '' != $_POST['nonce']) {

				$nonce = sanitize_text_field($_POST['nonce']);
			} else {
				$nonce = 0;
			}

			if (! wp_verify_nonce($nonce, 'afquote-ajax-nonce') ) {

				die('Failed ajax security check!');
			}

			if (!empty($_REQUEST['product_id'])) {

				$product_id = intval($_REQUEST['product_id']);
			} else {
				$product_id = intval(0);
			}
			
			if (isset($_REQUEST['variation_id'])) {
				$product_id = intval($_REQUEST['variation_id']);
			}

			if (!empty($_REQUEST['quantity'])) {
				$product_quantity = intval($_REQUEST['quantity']);
			} else {
				$product_quantity = intval(0);
			}

			if (!empty($_REQUEST['woo_addons'])) {

				$woo_addons = sanitize_meta('', $_REQUEST['woo_addons'], '');

			} else {

				$woo_addons = '';
			}

			if (!empty($_REQUEST['woo_addons1'])) {

				$woo_addons1 = sanitize_meta('', $_REQUEST['woo_addons1'], '');

			} else {

				$woo_addons1 = '';
			}
			
			$key = -1;

			$quotes = array();
			if (!empty(WC()->session->get('quotes'))) {

				$quotes = WC()->session->get('quotes');
				$keys   = array_keys(array_combine(array_keys($quotes), array_column($quotes, 'pid')), $product_id);
				if (!empty($keys)) {
					$key = $keys[0];
				}
			}

			if ((int) $key >= 0 ) {

				$quotes[$key]['quantity'] = (int) $quotes[$key]['quantity'] + (int) $product_quantity;
			} else {

				$quote = array('pid' => $product_id, 'quantity' => $product_quantity, 'woo_addons' => $woo_addons, 'woo_addons1' => $woo_addons1);
				array_push($quotes, $quote);
			}

			WC()->session->set('quotes', $quotes);

			if ('yes' == get_option('enable_ajax_shop')) {
				include_once AFB2B_PLUGIN_DIR . 'includes/ajax_add_to_quote.php';
			} else {

				echo 'success';
			}

			die();

		}

		public function afrfq_add_to_quote_single_vari_callback_function() {

			global $wp_session;

			if (isset($_POST['nonce']) && '' != $_POST['nonce']) {

				$nonce = sanitize_text_field($_POST['nonce']);
			} else {
				$nonce = 0;
			}

			if (! wp_verify_nonce($nonce, 'afquote-ajax-nonce') ) {

				die('Failed ajax security check!');
			}

			if (!empty($_REQUEST['vari_id'])) {
				$vari_id = intval($_REQUEST['vari_id']);
			} else {
				$vari_id = 0;
			}
			
			if (!empty($_REQUEST['product_id'])) {
				$product_id = intval($_REQUEST['product_id']);
			} else {
				$product_id = 0;
			}

			if (!empty($_REQUEST['variation'])) {
				$variation = sanitize_meta('', $_REQUEST['variation'], '');
			} else {
				$variation = array();
			}
			
			if (!empty($_REQUEST['quantity'])) {
				$product_quantity = intval($_REQUEST['quantity']);
			} else {
				$product_quantity = intval(0);
			}

			if (!empty($_REQUEST['woo_addons'])) {

				$woo_addons = sanitize_meta('', $_REQUEST['woo_addons'], '');

			} else {

				$woo_addons = '';
			}

			if (!empty($_REQUEST['woo_addons1'])) {

				$woo_addons1 = sanitize_meta('', $_REQUEST['woo_addons1'], '');

			} else {

				$woo_addons1 = '';
			}
			
			$key = -1;

			$quotes = array();
			if (!empty(WC()->session->get('quotes'))) {

				$quotes = WC()->session->get('quotes');

				$keys = array_keys(array_combine(array_keys($quotes), array_column($quotes, 'vari_id')), $vari_id);


				if (!empty($keys)) {
					$key = $keys[0];
				}
			}

			if ((int) $key >= 0) {

				$quotes[$key]['quantity'] = (int) $quotes[$key]['quantity'] + (int) $product_quantity;
			} else {

				$quote = array('pid' => $product_id,'vari_id' => $vari_id,  'quantity' => $product_quantity,'variation' => $variation , 'woo_addons' => $woo_addons, 'woo_addons1' => $woo_addons1);
				array_push($quotes, $quote);
			}

			

			WC()->session->set('quotes', $quotes);
			
			if ('yes' == get_option('enable_ajax_product')) {
				include_once AFB2B_PLUGIN_DIR . 'includes/ajax_add_to_quote.php';
			} else {

				echo 'success';
			}

			die();

		}

		public function afrfq_add_to_quote_single_callback_function() {

			global $wp_session;
			

			if (isset($_POST['nonce']) && '' != $_POST['nonce']) {

				$nonce = sanitize_text_field($_POST['nonce']);
			} else {
				$nonce = 0;
			}

			if (! wp_verify_nonce($nonce, 'afquote-ajax-nonce') ) {

				die('Failed ajax security check!');
			}

			if (!empty($_REQUEST['product_id'])) {
				$product_id = intval($_REQUEST['product_id']);
			} else {
				$product_id = 0;
			}
			
			if (!empty($_REQUEST['quantity'])) {
				$product_quantity = intval($_REQUEST['quantity']);
			} else {
				$product_quantity = 0;
			}

			if (!empty($_REQUEST['woo_addons'])) {

				$woo_addons = sanitize_meta('', $_REQUEST['woo_addons'], '');

			} else {

				$woo_addons = '';
			}

			if (!empty($_REQUEST['woo_addons1'])) {

				$woo_addons1 = sanitize_meta('', $_REQUEST['woo_addons1'], '');

			} else {

				$woo_addons1 = '';
			}
			
			$key = -1;

			$quotes = array();
			if (!empty(WC()->session->get('quotes'))) {

				$quotes = WC()->session->get('quotes');
				$keys   = array_keys(array_combine(array_keys($quotes), array_column($quotes, 'pid')), $product_id);
				if (!empty($keys)) {
					$key = $keys[0];
				}
			}

			if ((int) $key >= 0) {

				$quotes[$key]['quantity'] = (int) $quotes[$key]['quantity'] + (int) $product_quantity;
			} else {

				$quote = array('pid' => $product_id, 'quantity' => $product_quantity, 'woo_addons' => $woo_addons, 'woo_addons1' => $woo_addons1);
				array_push($quotes, $quote);
			}

			WC()->session->set('quotes', $quotes);
			
			if ('yes' == get_option('enable_ajax_product')) {
				include_once AFB2B_PLUGIN_DIR . 'includes/ajax_add_to_quote.php';
			} else {

				echo 'success';
			}

			die();

		}

		public function afrfq_remove_quote_item_callback_function() {

			global $wp_session;

			

			if (isset($_POST['nonce']) && '' != $_POST['nonce']) {

				$nonce = sanitize_text_field($_POST['nonce']);
			} else {
				$nonce = 0;
			}

			if (! wp_verify_nonce($nonce, 'afquote-ajax-nonce') ) {

				die('Failed ajax security check!');
			}

			if (!empty($_REQUEST['quote_key'])) {
				$quote_key = intval($_REQUEST['quote_key']);
			} else {
				$quote_key = 0;
			}
			
			//print_r($_SESSION);
			$quotes = WC()->session->get('quotes');
			//print_r($quotes);
			unset($quotes[$quote_key]);
			sort($quotes);
			WC()->session->set('quotes', $quotes);
			$quoteItemCount = 0;
			foreach ($quotes as $qouteItem) {

				$quoteItemCount += $qouteItem['quantity'];
			}
			include_once AFB2B_PLUGIN_DIR . 'includes/ajax_add_to_quote.php';            
			die();
		}


		public function afreg_emails( $emails) {

			require_once AFB2B_PLUGIN_DIR . 'classes/afreg-admin-email-class.php';
			$emails['afreg_admin_email_new_user'] = new  Addify_Registration_Fields_Admin_Email();
			$emails['afreg_admin_email_new_user']->init_form_fields();

			require_once AFB2B_PLUGIN_DIR . 'classes/afreg-admin-email-class-update-account.php';
			$emails['afreg_admin_email_update_user'] = new  Addify_Registration_Fields_Admin_Email_Update_Account();
			$emails['afreg_admin_email_update_user']->init_form_fields();


			require_once AFB2B_PLUGIN_DIR . 'classes/afreg-user-email-class.php';
			$emails['afreg_user_email_new_user'] = new  Addify_Registration_Fields_User_Email();


			require_once AFB2B_PLUGIN_DIR . 'classes/afreg-pending-user-email-class.php';
			$emails['afreg_pending_user_email_user'] = new  Addify_Registration_Fields_Pending_User_Email();


			require_once AFB2B_PLUGIN_DIR . 'classes/afreg-approved-user-email-class.php';
			$emails['afreg_approved_user_email_user'] = new  Addify_Registration_Fields_Approved_User_Email();


			require_once AFB2B_PLUGIN_DIR . 'classes/afreg-disapproved-user-email-class.php';
			$emails['afreg_disapproved_user_email_user'] = new  Addify_Registration_Fields_Disapproved_User_Email();

			return $emails;
		}


		public function afreg_insert_emails_default_text() {

			if (empty(get_option('afreg_admin_email_text'))) {

				$afreg_admin_email_text = '<p>Hi,</p>
<p>A new user has registered to your website. Here are the details,</p>
<p>{customer_details}</p>
<p><em>To disable this email, go to WooCommerce &gt; Settings &gt; Emails &gt; Addify Registration New User Email Admin.</em></p>';

				update_option('afreg_admin_email_text', $afreg_admin_email_text);

			}


			if (empty(get_option('afreg_update_user_admin_email_text'))) {

				$afreg_update_user_admin_email_text = '<p>Hi,</p>
<p>A new user has just updated their information from My Account Page.  Here are the details,</p>
<p>{customer_details}</p>
<p><em>To disable this email, go to WooCommerce &gt; Settings &gt; Emails &gt; Addify Registration Update User Email Admin.</em></p>';

				update_option('afreg_update_user_admin_email_text', $afreg_update_user_admin_email_text);

			}


			if (empty(get_option('afreg_user_email_text'))) {

				$afreg_user_email_text = '<p>Hi,</p>
<p>Thank you for creating new account. Here is glimpse of the details you have submitted.</p>
<p>{customer_details} </p>
<p>Thank you,</p>';

				update_option('afreg_user_email_text', $afreg_user_email_text);

			}



			if (empty(get_option('afreg_pending_approval_email_text'))) {

				$afreg_pending_approval_email_text = '<p>Hi,</p>
<p><br />Thank you for showing interest. To keep the community clean we like to review the new applicants. You will be notified VIA email about the approval status. Here is the glimpse of your details,</p>
<p>{customer_details}<br /><br />Thank you,</p>';

				update_option('afreg_pending_approval_email_text', $afreg_pending_approval_email_text);

			}



			if (empty(get_option('afreg_approved_email_text'))) {

				$afreg_approved_email_text = '<p>Hi,Thank you for showing patience. You account has been approved.</p>
<p>Thank you,</p>';

				update_option('afreg_approved_email_text', $afreg_approved_email_text);

			}



			if (empty(get_option('afreg_disapproved_email_text'))) {

				$afreg_disapproved_email_text = '<p>Hi,</p>
<p>We regret to inform you that your account has been disapproved. If you think its a mistake, please reach out to us.</p>
<p>Thank you,</p>';

				update_option('afreg_disapproved_email_text', $afreg_disapproved_email_text);

			}

		}

	}

	new Addify_B2B_Plugin();

}
