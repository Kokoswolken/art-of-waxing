<?php
get_header();
$notfoundTitle = get_field('404Title', 'option');
$notfoundContent = get_field('404Content', 'option');
?>

<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
        <div class="container page-wrapper card-wrapper">
            <div class="page-content text-center">
                <h2><?php echo $notfoundTitle; ?></h2>
                <p><?php echo $notfoundContent; ?></p>

            </div><!-- .page-content -->
        </div><!-- .page-wrapper -->
        <div class="container post-results mb-5">
            <div class="wp-search"><?php get_search_form(); ?> </div>
        </div>
    </div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>

