<?php

defined('ABSPATH') || exit;
?>

        </main><!-- #main -->
    </div><!-- #primary -->
</div><!-- #content -->

<footer id="colophon" class="footer" role="contentinfo">
    <div class="container">
        <div class="row"> <!-- #row -->
            <div class="footer-col col-xs-12 col-sm-4">
                <?php if (get_field('footer-logo', 'option')) : ?>
                    <?php $footer_logo = get_field('footer-logo', 'option'); ?>
                    <a href="<?php echo home_url($path = '/', $scheme = 'https'); ?>">
                        <img src="<?= $footer_logo['url']; ?>" title="<?= $footer_logo['title']; ?>"
                             alt="<?= $footer_logo['alt']; ?>">
                    </a>
                <?php endif; ?>
            </div>
            <div class="footer-col col-xs-12 col-sm-4">
                <div class="footer-address">
                    <?php if (get_field('footer-address', 'option')) : ?>
                        <?php the_field('footer-address', 'option'); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="footer-col col-xs-12 col-sm-4">
                <?php $group = get_field('group-footer', 'option'); ?>

                <div class="footer-social">
                    <p class="bold"><?php echo $group['social-header']; ?></p>

                    <?php if (have_rows('group-footer', 'option')) : ?>
                        <div class="footer-social-wrapper">

                            <?php while (have_rows('group-footer', 'option')) : the_row(); ?>
                                <?php if (have_rows('social-icons', 'option')) : ?>
                                    <span class="social-icon">
                                    <?php
                                    while (have_rows('social-icons', 'option')) : the_row();
                                        $iconSocial = get_sub_field('icon');
                                        $linkSocial = get_sub_field('link');
                                        ?>

                                        <a class="social-icon-link" href="<?php echo $linkSocial['url']; ?>"
                                           target="<?php echo $linkSocial['target']; ?>"
                                           title="<?php echo $linkSocial['title']; ?>">
                                            <?php echo wp_get_attachment_image($iconSocial, 'full');?>
                                        </a>
                                    <?php endwhile; ?>
                                </span>
                                <?php endif; ?>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>

                </div>
                <div class="footer-payment">
                    <p class="bold"><?php echo $group['payment-header']; ?></p>

                    <?php if (have_rows('group-footer', 'option')) : ?>
                        <div class="footer-payment-wrapper">

                            <?php while (have_rows('group-footer', 'option')) : the_row(); ?>
                                <?php if (have_rows('payment-icons', 'option')) : ?>
                                    <span class="payment-icon">
                                    <?php
                                    while (have_rows('payment-icons', 'option')) : the_row();
                                        $iconPayment = get_sub_field('icon');
                                        $linkPayment = get_sub_field('link');
                                        ?>
                                        <a class="payment-icon-link" href="<?php echo $linkPayment['url']; ?>"
                                           target="<?php echo $linkPayment['target']; ?>"
                                           title="<?php echo $linkPayment['title']; ?>">
                                            <?php  echo wp_get_attachment_image($iconPayment, 'full'); ?>
                                        </a>
                                    <?php endwhile; ?>
                                </span>
                                <?php endif; ?>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>

                </div>
            </div>

        </div> <!-- #row -->
        <div class="row">
            <div class="col-xs-12">
                <div class="site-disclaimer">
                    <span class="copyright">
                        <?php $url_privacy = get_field('url-privacy', 'option'); ?>
                        Copyright ©
                           <a href="<?php echo home_url($path = '/', $scheme = 'https'); ?>">
                               <?php echo get_bloginfo('name'); ?>,
                           </a> <?php echo date("Y"); ?> -
                            <a class="link-privacy" href="<?php echo $url_privacy['url']; ?>"
                               target="<?php echo $url_privacy['target']; ?>"
                               title="<?php echo $url_privacy['title']; ?>">
                                <?php echo $url_privacy['title']; ?>
                            </a>
                    </span>
                </div>
            </div>

        </div>
    </div>

</footer><!-- #colophon -->
</div><!-- .site-info -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

