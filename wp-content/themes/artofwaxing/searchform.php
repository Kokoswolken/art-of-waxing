<form role="search" method="get" class="search-form" action="<?php echo home_url('/'); ?>">
    <label>
        <input type="text" class="search-field" placeholder="<?php echo _e('Zoeken …'); ?>"
               value="<?php the_search_query(); ?>" name="s">
    </label>
    <input type="submit" class="btn-primary search-submit" value="Zoeken">
</form>
