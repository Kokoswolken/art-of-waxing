import Swiper, { Navigation, Pagination } from 'swiper';

const swiper = new Swiper('.testimonialSwiper', {
    // Optional parameters
    direction: 'vertical',
    loop: true,
     // modules: [Navigation, Pagination],

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

    // And if we need scrollbar
    scrollbar: {
        el: '.swiper-scrollbar',
    },
});
//  new Swiper('.testimonialSwiper', {
//     // Optional parameters
//     // direction: 'vertical',
//
//      modules: [Navigation, Pagination],
//
//      observer: true,
//     observeParents: true,
//     slidesPerView: 3,
//     speed: 400,
//
//     spaceBetween: 32,
// //     loop: true,
//     loop: true,
//
//     // If we need pagination
//     pagination: {
//         el: '.swiper-pagination',
//     },
//
//     // Navigation arrows
//     navigation: {
//         nextEl: '.swiper-button-next',
//         prevEl: '.swiper-button-prev',
//     },
//
//     // And if we need scrollbar
//     scrollbar: {
//         el: '.swiper-scrollbar',
//     },
// });