/**
 * Update cart page elements after add to cart events.
 */
// AddToCartHandler.prototype.updateButton = function( e, fragments, cart_hash, $button ) {
//     $button = typeof $button === 'undefined' ? false : $button;
//
//     if ( $button ) {
//         $button.removeClass( 'loading' );
//
//         if ( fragments ) {
//             $button.addClass( 'added' );
//         }
//
//         // View cart text.
//         if ( fragments && ! wc_add_to_cart_params.is_cart && $button.parent().find( '.added_to_cart' ).length === 0 ) {
//             $button.after( '<a href="' + wc_add_to_cart_params.cart_url + '" class="added_to_cart wc-forward" title="' +
//                 wc_add_to_cart_params.i18n_view_cart + '">' + wc_add_to_cart_params.i18n_view_cart + '</a>' );
//         }
//
//         $( document.body ).trigger( 'wc_cart_button_updated', [ $button ] );
//     }
// };

//
// setTimeout(function(){
//     if ($('#divID').length > 0) {
//         $('#divID').remove();
//     }
// }, 5000)
//
//
// (function() {
//
//     var count = 1,
//         event = function (event) {
//             if (event.animationName == 'nodeInserted') event.target.textContent = 'Element ' + count++ + ' has been parsed!';
//         }
//
//     document.addEventListener('animationstart', event, false);
//     document.addEventListener('MSAnimationStart', event, false);
//     document.addEventListener('webkitAnimationStart', event, false);
//
//     setTimeout(function () {
//         var div = document.createElement('div');
//         div.setAttribute('class', 'some-control');
//         document.getElementById('test').appendChild(div)
//     }, 2000);
//
// });