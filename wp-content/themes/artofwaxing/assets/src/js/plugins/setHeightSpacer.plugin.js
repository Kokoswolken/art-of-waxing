// window.addEventListener('DOMContentLoaded', (event) => {
//
//     let spacer, spacerProp, spacerHeight, mql;
//     spacer = document.getElementsByClassName("wp-block-spacer");
//     spacerHeight = spacer[0].offsetHeight;
//     spacerHeight = spacerHeight/2;
//
//     mql = window.matchMedia('(max-width: 600px)');
//
//     function changeHeightOnMQL(e) {
//         if (e.matches) {
//             spacer[0].style.height = spacerHeight + 'px';
//         }
//     }
//
//     mql.addEventListener('onload', changeHeightOnMQL);
// });

let mql;
mql = window.matchMedia('(max-width: 600px)');

function changeHeightOnMQL(e) {


    if (e.matches) {
        let spacer, spacerHeight;
        spacer = document.getElementsByClassName("wp-block-spacer");
        spacerHeight = spacer[0].offsetHeight;
        spacerHeight = spacerHeight/2;
        spacer[0].style.height = spacerHeight + 'px';
    }
}

// window.onload = changeHeightOnMQL();
mql.addEventListener('load', changeHeightOnMQL);
