/**
 * SASS
 */
// import "./plugins/swiper.plugin"

import '../sass/main.scss';
import '@popperjs/core';

// import { * } from 'bootstrap';

/**
 * Plugins
 */

// import './plugins/moveElement.plugin';
// import './plugins/setHeightSpacer.plugin';

// import Swiper from '../../../node_modules/swiper/swiper-bundle.esm.js';
// import Swiper from '~swiper.esm';

import {
    Alert,
    Button,
    Carousel,
    Collapse,
    Dropdown,
    Modal,
    Offcanvas,
    Popover,
    ScrollSpy,
    Tab,
    Toast,
    Tooltip } from 'bootstrap';
// import Swiper from "swiper";

/**
 * JavaScript
 */

