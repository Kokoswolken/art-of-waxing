<?php

get_header();

/* Start the Loop */
?>
    <div class="page">

        <?php
        if (have_posts()) :
            while (have_posts()) : the_post();
                get_template_part('template-parts/content/content');
            endwhile;
        else :
            get_template_part('template-parts/content/content-none');
        endif;

        ?>
    </div>

<?php
get_footer();