<?php
/**
 * The header.
 *
 * This is the template that displays all of the <head> section and everything up until main.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @since Art of Waxing 1.0
 */

defined('ABSPATH') || exit;
$background = '';
$colorTemplate = get_field('color_template') ?: false;
//if($colorTemplate == true) : $background = '#fff'; endif;

$colorTemplate = ($colorTemplate == true ? $background = 'backgroundColor' : '');

?>
<!doctype html>
<html <?php language_attributes(); ?> >
<head>
    <title><?php bloginfo( 'title' ); ?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
<!-- @todo   Rewrite to swiper.plugin.js files when webpack 5 is adjusted to process swiper files correctly -->
    <link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />
    <script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>
    <script type="module">
        import Swiper from 'https://unpkg.com/swiper@7/swiper-bundle.esm.browser.min.js'

        const swiperTestimonial = new Swiper('.testimonialSwiper', {
            direction: 'horizontal',
            loop: true,
            speed: 400,
            spaceBetween: 16,
            slidesPerView: 1,

            // navigation: {
            //     nextEl: '.swiper-button-next',
            //     prevEl: '.swiper-button-prev',
            // },

            breakpoints: {
                768: {
                    spaceBetween: 20,
                    slidesPerView: 2
                },
                992: {
                    slidesPerView: 3,
                },

                1024: {
                    spaceBetween: 40,
                    slidesPerView: 3
                },

                1200: {
                    slidesPerView: 3,
                    spaceBetween: 64
                    // spaceBetween: 64
                }
            }

            // If we need pagination
            // pagination: {
            //     el: '.swiper-pagination',
            //     clickable: true,
            // },

            // Navigation arrows
            // navigation: {
            //     nextEl: '.swiper-button-next',
            //     prevEl: '.swiper-button-prev',
            // },

            // And if we need scrollbar
            // scrollbar: {
            //     el: '.swiper-scrollbar',
            // },
        });
        const swiperProduct = new Swiper('.productSwiper', {
            direction: 'horizontal',
            loop: true,
            speed: 400,
            spaceBetween: 16,
            slidesPerView: 2,

            // navigation: {
            //     nextEl: '.swiper-button-next',
            //     prevEl: '.swiper-button-prev',
            // },

            breakpoints: {
                768: {
                    spaceBetween: 24,
                    slidesPerView: 3
                },
                992: {
                    slidesPerView: 3,
                },

                1024: {
                    spaceBetween: 48,
                    slidesPerView: 4
                },

                1200: {
                    slidesPerView: 4,
                    spaceBetween: 64
                }
            }

            // If we need pagination
            // pagination: {
            //     el: '.swiper-pagination',
            //     clickable: true,
            // },

            // Navigation arrows
            // navigation: {
            //     nextEl: '.swiper-button-next',
            //     prevEl: '.swiper-button-prev',
            // },

            // And if we need scrollbar
            // scrollbar: {
            //     el: '.swiper-scrollbar',
            // },
        });

        const swiperArchive = new Swiper('.archiveSwiper', {
            direction: 'horizontal',
            loop: false,
            spaceBetween: 50,

            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
        });
    </script>
    <link href="https://fonts.googleapis.com/css2?family=Bilbo+Swash+Caps&family=Montserrat:wght@400;500;600&family=Playfair+Display&display=swap" rel="stylesheet">
    <?php wp_head(); ?>
</head>

<body <?php body_class($background); ?>>
<?php //wp_die($background);?>
<?php wp_body_open(); ?>
<div id="page" class="site">
    <?php get_template_part( 'template-parts/header/nav'); ?>
    <?php get_template_part( 'template-parts/header/hero', 'full' ); ?>

    <div id="content" class="site-content">
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">

