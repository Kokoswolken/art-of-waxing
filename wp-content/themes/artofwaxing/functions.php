<?php
defined('ABSPATH') || exit;

/**
 * Theme Init
 *
 * Sets up theme defaults and registers support for various WordPress features.
 *
 */
require_once( get_template_directory() . '/inc/lib/theme-setup.php' );


/**
 * Custom Post Types
 *
 * Registers Post Types
 *
 */
require_once( get_template_directory() . '/inc/lib/custom-post-types.php' );


/**
 * Reqister WP Block Patterns
 *
 * @since Art of Waxing 1.0
 *
 * @return void
 */

require_once( get_template_directory() . '/inc/lib/register-wp-patterns.php' );

/**
 * Add WooCommerce Support
 *
 * @since Art of Waxing 1.0
 *
 * @return void
 */

require_once( get_template_directory() . '/inc/lib/wc-support.php' );

/**
 * Adjust WooCommerce Hooks
 *
 * @since Art of Waxing 1.0
 *
 * @return void
 */

require_once( get_template_directory() . '/inc/lib/wc-hooks.php' );


/**
 * Reqister ACF Gutenberg Blocks
 *
 * @since Art of Waxing 1.0
 *
 * @return void
 */

require_once( get_template_directory() . '/inc/lib/register-acf.php' );

/**
 * Enqueue scripts and styles.
 *
 * @since Art of Waxing 1.0
 *
 * @return void
 */

require_once( get_template_directory() . '/inc/lib/styles-scripts.php' );



/**
 * WooCommerce Init
 *
 * Sets up theme defaults and registers support for various WordPress features.
 *
 */

//** WooCommerce Init */
//add_action( 'woocommerce_single_product_summary', 'wcAddProductSummary', 5 );
//add_action( 'woocommerce_special_add_to_cart', 'woocommerce_special_add_to_cart', 30 );
//function wcAddProductSummary() {
//    /**
//     * Output the special product add to cart area.
//     */
//    function woocommerce_single_product_summary() {
//        wc_get_template( 'template-parts/block/content-summary.php' ); // if you have arguments, you could use, read the the manual links of this function for reference
//    }
//}
//add_action('woocommerce_single_product_summary_custom', 'woocommerce_output_all_notices', 10);
//add_action('woocommerce_single_product_summary_custom', 'woocommerce_show_product_sale_flash', 10);
//add_action('woocommerce_single_product_summary_custom', 'woocommerce_show_product_images', 10);
//add_action('woocommerce_single_product_summary_custom', 'woocommerce_template_single_title', 10);
//add_action('woocommerce_single_product_summary_custom', 'woocommerce_template_single_rating', 20);
//add_action('woocommerce_single_product_summary_custom', 'woocommerce_template_single_price', 30);
//add_action('woocommerce_single_product_summary_custom', 'woocommerce_template_single_excerpt', 40);
//add_action('woocommerce_single_product_summary_custom', 'woocommerce_template_single_add_to_cart', 50);
//add_action('woocommerce_single_product_summary_custom', 'woocommerce_template_single_meta', 60);
//add_action('woocommerce_single_product_summary_custom', 'woocommerce_template_single_sharing', 70);
//add_action('woocommerce_single_product_summary_custom', 'WC_Structured_Data::generate_product_data()', 70);
//add_action('woocommerce_single_product_summary_custom', 'woocommerce_output_product_data_tabs', 80);
//add_action('woocommerce_single_product_summary_custom', 'woocommerce_upsell_display', 90);
//add_action('woocommerce_single_product_summary_custom', 'woocommerce_output_related_products', 100);
//
//add_action('aow_woocommerce_before_single_product', 'aow_woocommerce_before_single_product', 10);
//add_action('aow_woocommerce_before_single_product_summary', 'aow_woocommerce_before_single_product_summary', 10);
//add_action('aow_woocommerce_single_product_summary', 'aow_woocommerce_single_product_summary', 10);
//add_action('aow_woocommerce_after_single_product_summary', 'aow_woocommerce_after_single_product_summary', 10);
//add_action('aow_woocommerce_after_single_product', 'aow_woocommerce_after_single_product', 10);



function aow_woocommerce_before_single_product() {
//    do_action('woocommerce_before_single_product');
}

function aow_woocommerce_before_single_product_summary() {
//    do_action( 'woocommerce_before_single_product_summary' );
}

function aow_woocommerce_single_product_summary() {
//    do_action('woocommerce_single_product_summary');
}

function aow_woocommerce_after_single_product_summary() {
//    woocommerce_output_product_data_tabs();
//    woocommerce_upsell_display();
//    woocommerce_output_related_products();
}

function aow_woocommerce_after_single_product() {
//    do_action('woocommerce_after_single_product');
}


//    do_action( 'woocommerce_after_single_product' );




remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );


//add_filter('woocommerce_upsell_display', 'addTags', 10);
function addTags() {
//    global $product;
//    echo wc_get_product_tag_list($product->get_id(), '</br> ', '<div class="tag_wrapper"><span class="tagged_as_up_sell">' . _n('', '', count($product->get_tag_ids()), 'woocommerce') . ' ', '</span></div>');
}

//add_action( 'woocommerce_after_single_product_summary', 'bbloomer_woocommerce_output_upsells', 10 );
//
//function bbloomer_woocommerce_output_upsells() {
//    addTags();
//}

remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );
add_action( 'woocommerce_after_cart', 'woocommerce_cross_sell_display' );


//add_filter ( 'woocommerce_product_thumbnails_columns', 'bbloomer_change_gallery_columns' );
//
function bbloomer_change_gallery_columns() {
    return 1;
}
function remove_image_zoom_support() {
//    remove_theme_support( 'wc-product-gallery-zoom' );
}
add_filter('woocommerce_after_cart_item_name', 'woocommerce_after_cart_item_name_test', 10, 2);
function woocommerce_after_cart_item_name_test( $cart_item, $cart_item_key  ){
    $product      = wc_get_product( $cart_item['product_id'] );
    $rating_count = $product->get_rating_count();
    $review_count = $product->get_review_count();
    $average      = $product->get_average_rating();
    if ( $rating_count > 0 ) : ?>
        <div class="woocommerce-product-rating">
            <?php echo wc_get_rating_html( $average, $rating_count ); // WPCS: XSS ok. ?>
            <div class="count"><?php echo esc_html( $review_count ); ?> Reviews</div>
        </div>
    <?php endif;
}