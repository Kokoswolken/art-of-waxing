<?php
get_header();
$notfoundContent = get_field('404Content', 'option');

get_template_part( 'template-parts/content/content', 'results' );

if ( have_posts() ) {  ?>

    <div class="container card-wrapper">
        <?php while ( have_posts() ) {
            the_post();
            get_template_part( 'template-parts/content/content', 'excerpt' );
        }
        wp_reset_query();?>

    </div>

<?php } else { ?>
    <div class="container card-wrapper">
        <?php echo $notfoundContent; ?>
    </div>
<?php
}

get_footer();