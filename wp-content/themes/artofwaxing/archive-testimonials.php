<?php
get_header();

if (have_posts()) : ?>
    <?php $testimonialTitle = get_field('archiveTestimonialContent' , 'option') ?: ''; ?>
    <div class="post-type-archive-testimonials">
        <div class="testimonials container">
            <div class="row testimonials-wrapper">
                <div><?php echo $testimonialTitle; ?></div>
                <?php while (have_posts()) : the_post();
                    get_template_part('template-parts/archive/content', 'review');
                endwhile;
                ?>
            </div>
        </div>
    </div>
<?php else :

    // If no content, include the "No posts found" template.
    get_template_part('template-parts/content/content-none');

endif;

get_footer();

