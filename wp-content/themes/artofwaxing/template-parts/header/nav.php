<?php
$checkoutLink = get_field('checkoutLink', 'option') ?: array('url' => '', 'target' => '',  'title' => '');
$searchLink = get_field('searchLink', 'option') ?: array('url' => '', 'target' => '',  'title' => '');
$accountLink = get_field('accountLink', 'option') ?: array('url' => '', 'target' => '',  'title' => '');
?>

<?php if (has_nav_menu('primary')) : ?>
    <nav id="site-navigation" class="navigation container-fluid" role="navigation"
         aria-label="<?php esc_attr_e('Primary menu', 'aow'); ?>">

        <div class="navigation__logo-wrap">
            <a href="<?php echo home_url($path = '/', $scheme = 'https'); ?>">
                <?php if (has_custom_logo()) : ?>
                    <?php the_custom_logo(); ?>
                <?php endif; ?>
            </a>
        </div>
        <?php
        wp_nav_menu(
            array(
                'theme_location' => 'primary',
                'menu_class' => 'menu-wrapper',
                'container_class' => 'navigation__primary--menu-container',
                'items_wrap' => '<ul id="primary-menu-list" class="%2$s">%3$s</ul>',
                'fallback_cb' => false,
            )
        );
        ?>

        <div class="navigation__icon-wrap">
            <a href="<?php echo $searchLink['url']; ?>" target="<?php echo $searchLink['target']; ?>">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.875 18.75a7.875 7.875 0 100-15.75 7.875 7.875 0 000 15.75zm5.568-2.306L21 21"
                          stroke="#424241" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </a>

            <a href="<?php echo $accountLink['url']; ?>" target="<?php echo $accountLink['target']; ?>">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M12 15a6 6 0 100-12 6 6 0 000 12z" stroke="#424241" stroke-width="1.5"
                          stroke-miterlimit="10"/>
                    <path d="M2.905 20.25a10.504 10.504 0 0118.19 0" stroke="#424241" stroke-width="1.5"
                          stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </a>
            <a href="<?php echo $checkoutLink['url']; ?>" target="<?php echo $checkoutLink['target']; ?>">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M19.579 20.25H4.42a.75.75 0 01-.745-.667l-1.334-12a.75.75 0 01.746-.833h17.824a.75.75 0 01.745.833l-1.333 12a.75.75 0 01-.745.667zM8.25 6.75a3.75 3.75 0 017.5 0"
                          stroke="#424241" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </a>
            <a class="hamburger-menu" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight"
               aria-controls="offcanvasRight">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M3.75 12h16.5M3.75 6h16.5M3.75 18h16.5" stroke="#424241" stroke-width="1.5"
                          stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </a>
        </div>


    </nav>
<?php endif; ?>


<div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
    <div class="offcanvas-header">
            <span class="offcanvas-header-logo">
                <?php if (has_custom_logo()) : ?>
                    <?php the_custom_logo(); ?>
                <?php endif; ?>
            </span>


        <a href="">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M10.875 18.75a7.875 7.875 0 100-15.75 7.875 7.875 0 000 15.75zm5.568-2.306L21 21"
                      stroke="#424241" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
        </a>

        <a href="<?php echo $accountLink['url']; ?>" target="<?php echo $accountLink['url']; ?>">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M12 15a6 6 0 100-12 6 6 0 000 12z" stroke="#424241" stroke-width="1.5"
                      stroke-miterlimit="10"/>
                <path d="M2.905 20.25a10.504 10.504 0 0118.19 0" stroke="#424241" stroke-width="1.5"
                      stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
        </a>
        <a href="<?php echo $checkoutLink['url']; ?>" target="<?php echo $checkoutLink['target']; ?>">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M19.579 20.25H4.42a.75.75 0 01-.745-.667l-1.334-12a.75.75 0 01.746-.833h17.824a.75.75 0 01.745.833l-1.333 12a.75.75 0 01-.745.667zM8.25 6.75a3.75 3.75 0 017.5 0"
                      stroke="#424241" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
        </a>
        <a class="text-reset" data-bs-dismiss="offcanvas" aria-label="Close">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M18.75 5.25l-13.5 13.5m13.5 0L5.25 5.25" stroke="#424241" stroke-width="1.5"
                      stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
        </a>
    </div>
    <div class="offcanvas-body">
        <div class="menu">
            <?php
            wp_nav_menu(
                array(
                    'theme_location' => 'primary',
                    'menu_class' => 'menu-wrapper',
                    'container_class' => 'navigation__primary--menu-container',
                    'items_wrap' => '<ul id="primary-menu-list" class="%2$s">%3$s</ul>',
                    'fallback_cb' => false,
                )
            );
            ?>
        </div>
        <div class="dropdown mt-3">

        </div>
    </div>
</div>