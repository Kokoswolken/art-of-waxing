<?php //Create a 404 page
$notfoundTitle = get_field('404Title', 'option');
$notfoundContent = get_field('404Content', 'option');
?>

  <div class="container page-wrapper card-wrapper">
            <div class="page-content text-center">
                <h2><?php echo $notfoundTitle; ?></h2>
<p><?php echo $notfoundContent; ?></p>

</div><!-- .page-content -->
</div><!-- .page-wrapper -->
<div class="container post-results mb-5">
    <div class="wp-search"><?php get_search_form(); ?> </div>
</div>