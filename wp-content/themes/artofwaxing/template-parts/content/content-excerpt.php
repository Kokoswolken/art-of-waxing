<?php
/**
 * Template part for displaying post archives and search results
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

$image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'small');
$title = get_the_title();
$permalink = get_the_permalink();
?>

<div class="card" id="post-<?php the_ID(); ?>">
    <?php if ($image) : ?>
        <img class="card--image img-fluid" src="<?php echo $image[0]; ?>"/>
    <?php endif; ?>

    <?php if ($title) : ?>
        <h5 class="card-title">
            <span><?php echo $title; ?></span>
        </h5>
    <?php endif; ?>


    <?php if ($permalink) : ?>
        <a class="card-link" href="<?php echo $permalink; ?>"></a>
    <?php endif; ?>
</div>
