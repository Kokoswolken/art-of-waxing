<?php

/**
 * Flexible About us Block
 */

$image = get_field('about_image');
$content = get_field('about_content');
$signee = get_field('about_signee');

// create id attribute for specific styling
$id = 'about-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';
?>
<div id="<?php echo $id; ?>" class="about container <?php echo $align_class; ?>" >
    <div class="row">
        <div class="about-image col-xs-12 col-md-6">
            <img src="<?php echo $image['url']; ?>" title="<?php echo $image['title']; ?>" alt="<?php echo $image['alt']; ?>" class="about-image">
        </div>
        <div class="about-description col-xs-12 col-md-6">
            <?php if ($content) : ?> <div class="about-content"><?php echo $content; ?></div> <?php endif; ?>
            <?php if ($signee) : ?> <span class="about-signee"><?php echo $signee; ?></span> <?php endif; ?>
        </div>

    </div>
</div>