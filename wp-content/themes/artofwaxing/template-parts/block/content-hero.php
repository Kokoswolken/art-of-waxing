<?php

/**
 * Flexible Hero Block
 */

$subheader = get_field('hero_subheader');
$heading = get_field('hero_heading');
$description = get_field('hero_description');
$hero_image = get_field('hero_image');
$shop_link = get_field('hero_shoplink');
$contact_link = get_field('hero_contactlink');


// create id attribute for specific styling
$id = 'hero-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';
?>


<div id="<?php echo $id; ?>" class="hero container <?php echo $align_class; ?>" >
    <div class="row">
        <div class="hero-image col-xs-12 col-md-6">
            <?php if($hero_image) : ?>
                <img src="<?php echo esc_url($hero_image['url']); ?>" alt="<?php echo esc_attr($hero_image['target']); ?>" class="hero-image">
            <?php else : ?>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/images/backgrounds/hero.png" alt="Hero - Art of Waxing" class="hero-image">
            <?php endif; ?>

        </div>
        <div class="hero-content col-xs-12 col-md-6">
            <?php if ($subheader) : ?> <span class="detail large"><?php echo $subheader; ?></span> <?php endif; ?>
            <?php if ($heading) : ?> <h1 class="hero-content-title"><?php echo $heading; ?></h1> <?php endif; ?>
            <?php if ($description) : ?> <div class="hero-content-description"><?php echo $description; ?></div> <?php endif; ?>
            <?php if ($shop_link || $contact_link) : ?>
                <div class="button-wrapper">
                    <?php if ($shop_link) : ?>
                    <a href="<?php echo $shop_link['url']; ?>" target="<?php echo $shop_link['target']; ?>"
                       class="btn-primary"><?php echo $shop_link['title']; ?></a>
                    <?php endif; ?>
                    <?php if ($contact_link) : ?>
                    <a href="<?php echo $contact_link['url']; ?>" target="<?php echo $contact_link['target']; ?>"
                       class="btn-secondary"><?php echo $contact_link['title']; ?></a>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
