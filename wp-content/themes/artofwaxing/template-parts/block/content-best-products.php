<?php

/**
 * Flexible Best Products Block
 */

$heading = get_field('best_products_heading');
$subheader = get_field('best_products_subheader');
$productCard = get_field('best_products_card');

$title_submission = get_field('best_products_submission_title') ?: '';
$subtitle_submission = get_field('best_products_submission_subtitle') ?: '';
$cta_submission = get_field('best_products_submission_cta') ?: '';

$cta_button = get_field('best_products_modal_button') ?: '';
$modal_title = get_field('best_products_modal_title') ?: '';
$modal_content = get_field('best_products_modal_content') ?: '';

// create id attribute for specific styling
$id = 'best-products-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';
?>

<div class="jumbotron jumbotron-fluid">
    <div id="<?php echo $id; ?>" class="best-products container <?php echo $align_class; ?>">

        <?php if ($heading && $subheader) : ?>
            <div class="row products-wrapper">
                <div class="col-12">
                    <h2 class="title"><?php echo $heading; ?></h2>
                    <p class="subtitle"><?php echo $subheader; ?></p>
                </div>
            </div>
        <?php endif; ?>

        <?php if (have_rows('best_products_card')): ?>
            <div class="row card-wrapper">
                <?php while (have_rows('best_products_card')) : the_row(); ?>

                    <div class="col-12 col-md-4 product-card">
                        <?php $img = get_sub_field('card_image'); ?>
                        <?php $title = get_sub_field('card_title'); ?>
                        <?php $subtitle = get_sub_field('card_subtitle'); ?>
                        <?php $link = get_sub_field('card_link'); ?>

                        <?php if ($img && $title && $subtitle) : ?>
                            <img src="<?php echo $img['url']; ?>" title="<?php echo $img['title']; ?>"
                                 alt="<?php echo $img['alt']; ?>">
                            <h2><?php echo $title; ?></h2>
                            <?php echo $subtitle; ?>
                            <?php if($link) : ?>
                                <a class="product-card-link" href="<?php echo $link['url']; ?>"
                                    target="<?php echo $link['target']; ?>"></a>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>

                <?php endwhile; ?>
            </div>
        <?php endif; ?>

        <?php if ($title_submission) : ?>
        <div class="row submission-wrapper">
            <div class="col-12">
                <h3><?php echo $title_submission; ?></h3>
                <div class="submission-content"><?php echo $subtitle_submission; ?></div>
                <?php if($cta_submission) : ?>
                <a class="btn-primary" href="<?php echo $cta_submission['url']; ?>"
                   title="<?php echo $cta_submission['title']; ?>">
                    <?php echo $cta_submission['title']; ?>
                </a>
                <?php endif; ?>
                <?php if($cta_button) : ?>
                <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                        data-bs-target="#subscribeNewsletter">
                    <?php echo $cta_button; ?>
                </button>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <?php if($cta_button) : ?>
    <div class="modal fade" id="subscribeNewsletter" tabindex="-1" aria-labelledby="subscribeNewsletter"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="subscribeNewsletterLabel"><?php echo $modal_title ?></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <?php echo $modal_content; ?>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>
