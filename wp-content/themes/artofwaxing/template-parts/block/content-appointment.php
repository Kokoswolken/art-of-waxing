<?php

/**
 * Flexible Appointment Block
 */

$heading = get_field('appointment_heading');
$description = get_field('appointment_description');
$link = get_field('appointment_link');

// create id attribute for specific styling
$id = 'appointment-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';
?>

<div id="<?php echo $id; ?>" class="appointment container <?php echo $align_class; ?>" >
    <div class="row">
        <div class="appointment-image col-xs-12 col-md-6 col-xl-8">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/images/backgrounds/filler.png" alt="Filler - Art of Waxing" class="appointment-image">
        </div>
        <div class="appointment-content col-xs-12 col-md-6 col-xl-4">
            <?php if ($heading) : ?> <h2 class="appointment-content-title"><?php echo $heading; ?></h2> <?php endif; ?>
            <?php if ($description) : ?> <p class="appointment-content-description"><?php echo $description; ?></p> <?php endif; ?>

            <?php if ($link) : ?>
                <a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"
                   class="btn-primary appointment-link "><?php echo $link['title']; ?></a>
            <?php endif; ?>

        </div>

    </div>
</div>