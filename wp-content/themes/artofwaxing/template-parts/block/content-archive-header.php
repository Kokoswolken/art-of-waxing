<?php
/**
 * Flexible Archive Header
 */

$term = get_queried_object();

$title = get_field('archive_title', $term);
// create id attribute for specific styling
$id = 'archive-header-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';
?>
<div id="<?php echo $id; ?>" class="archive-header <?php echo $align_class; ?>">
    <div class="container archiveSwiperContainer">
        <?php if (have_rows('archive_slider', $term)) : ?>
            <div class="row ">
                <div class="archiveSwiper">
                    <div class="swiper-wrapper">
                        <?php while (have_rows('archive_slider', $term)) : the_row(); ?>
                            <?php $slider_image = get_sub_field('archive_image', $term); ?>
                            <div class="swiper-slide">
                                <img src="<?php echo $slider_image['url']; ?>"/>
                            </div>
                        <?php endwhile; ?>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        <?php endif; ?>


    </div>
    <div class="container">
        <div class="row">
            <?php if ($title) : ?>
                <h2><?php echo $title; ?></h2>
            <?php endif; ?>

            <?php if (have_rows('archive_link', $term)) : ?>
                <div class="button-wrapper">
                    <?php while (have_rows('archive_link', $term)) : the_row(); ?>
                        <?php $link = get_sub_field('archive_category_link'); ?>
                        <a href="<?php echo $link['url']; ?>"
                           target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>