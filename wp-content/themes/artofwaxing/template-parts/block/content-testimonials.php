<?php

/**
 * Flexible Testimonials Block
 */

$averageScore = get_field('averageScore', 'option') ?: '5.0';
$averageStars = get_field('averageStars', 'option') ?: 5;
$textCountReviewsStart = get_field('testimonial_textCountReviewsS') ?: 'Op basis van';
$textCountReviewsEnd = get_field('testimonial_textCountReviewsE') ?: 'reviews';
$link = get_field('testimonials_link');

// create id attribute for specific styling
$id = 'testimonials-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';


$args = array(
    'post_type' => 'testimonials',
    'order' => 'ASC'
);
$query = new WP_Query($args);
$count = $query->found_posts;

$countMessage = $textCountReviewsStart . ' ' . $count . ' ' . $textCountReviewsEnd;
?>

<div id="<?php echo $id; ?>" class="testimonials container <?php echo $align_class; ?>">
    <div class="row testimonialSwiper testimonials-wrapper">
        <div class="swiper-wrapper">
            <!-- Create dynamic function to calculate average score -->
            <div class="swiper-slide testimonial col-sm-12 col-md-6 col-lg-3">
                <p class="score"><?php echo $averageScore; ?></p>
                <div class="testimonial-stars">
                    <?php for ($i = 1; $i <= $averageStars; $i++) : ?>
                        <i></i>
                    <?php endfor; ?>
                </div>
                <p class="small testimonial-count"><?php echo $countMessage; ?></p>
            </div>

            <?php if ($query->have_posts()) : ?>
                <?php while ($query->have_posts()) : $query->the_post(); ?>

                    <?php get_template_part('template-parts/archive/content', 'review'); ?>

                <?php endwhile; ?>

                <?php wp_reset_postdata(); ?>

            <?php endif; ?>
        </div>
    </div>
    <?php if($link) : ?>
    <div class="row readmore">
        <div class="readmore-link col-12">
            <a href="<?php echo $link['url']; ?>"
               target="<?php echo $link['target']; ?>"
               class="btn-secondary"><?php echo $link['title']; ?></a>
        </div>
    </div>
    <?php endif; ?>
</div>