<?php

/**
 * Flexible WooCommerce Webshop Block
 */

$content = get_field('wc_products_content') ?: '';
$link = get_field('wc_products_link') ?: '';

// create id attribute for specific styling
$id = 'webshop-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';
?>

<div class="jumbotron jumbotron-fluid-full">
    <div id="<?php echo $id; ?>" class="latest-products container <?php echo $align_class; ?>">
        <div class="row ">
            <div class="col-sm-12">
                <?php echo $content; ?>
                <ul class="products">
                    <?php
                    $args = array(
                        'post_type' => 'product',
                        'stock' => 1,
                        'posts_per_page' => 4,
                        'orderby' => 'date',
                        'order' => 'DESC');

                    $loop = new WP_Query($args); ?>

                    <div class="productSwiper">
                        <div class="swiper-wrapper">
                            <?php while ($loop->have_posts()) : $loop->the_post();
                                global $product; ?>

                                <li class="product swiper-slide ">

                                    <a id="id-<?php the_id(); ?>" href="<?php the_permalink(); ?>"
                                       title="<?php the_title(); ?>">
                                        <?php if(get_post_meta( $loop->post->ID, '_sale_price', true)) :
                                            echo display_percentage_on_sale_badge('', '', $product);
                                        endif; ?>
                                        <?php if (has_post_thumbnail($loop->post->ID)) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="' . woocommerce_placeholder_img_src() . '" alt="" width="65px" height="115px" />'; ?>
                                        <h2 class="woocommerce-loop-product__title"><?php the_title(); ?></h2>
                                        <?php wc_add_tags_to_archive(); ?>
                                        <p class="price"><?php echo $product->get_price_html(); ?></p>
                                    </a>
                                    <?php woocommerce_template_loop_add_to_cart($loop->post, $product); ?>
                                </li>


                            <?php endwhile; ?>
                        </div>
                    </div>

                    <?php wp_reset_query(); ?>
                </ul>
                <a class="btn-primary latest-products-link" href="<?php echo $link['url']; ?>"
                   target="<?php echo $link['url']; ?>"><?php echo $link['title']; ?></a>
            </div>

        </div>

    </div>
</div>
