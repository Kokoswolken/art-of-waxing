<?php

/**
 * Flexible Treatment Block
 */

$selectedCategory = get_field('selected_category');
$title = get_field('treatment_title') ?: '';
$button = get_field('treatment_button');
$setListView = get_field('treatment_list_view');
$class = ($setListView) ? 'treatments-list' : 'treatments';

// create id attribute for specific styling
$id = 'treatment-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';


$args = array(
    'post_type' => 'treatments',
    'order' => 'ASC',
    'cat' => $selectedCategory,
);

$query = new WP_Query($args);
?>
<div id="<?php echo $id; ?>" class="treatments container <?php echo $align_class; ?>">
    <div class="row">
        <h2 class="<?php echo $class; ?>-title"><?php echo $title; ?></h2>

        <div class="<?php echo $class; ?>-wrapper">
            <?php if ($query->have_posts()) : ?>
                <?php while ($query->have_posts()) : $query->the_post(); ?>

                    <?php get_template_part('template-parts/archive/content', 'treatment'); ?>
                <?php endwhile; ?>

            <?php endif; ?>

            <?php wp_reset_postdata(); ?>

        </div>

        <?php if ($button) : ?>
            <a
                    href="<?php echo $button['url']; ?>"
                    target="<?php echo $button['target']; ?>"
                    class="treatment-button btn-primary">
                <?php echo $button['title']; ?>
            </a>
        <?php endif; ?>
    </div>
</div>
