<?php
$id = get_the_ID();
$price = get_field('price', $id) ?: '';
$setListView = get_field('treatment_list_view');
$class = ($setListView) ? 'treatments-list' : 'treatments';

?>
<div class="<?php echo $class; ?>-blocks">

        <div class="<?php echo $class; ?>-card">
            <?php if (get_the_post_thumbnail() && !$setListView) : the_post_thumbnail('thumbnail'); endif; ?>
            <p class="<?php echo $class; ?>-heading bold"><?php the_title(); ?></p>
            <div class="<?php echo $class; ?>-content"><?php if (!$setListView) : the_content(); endif ?></div>
            <?php if ($price) : ?><p class="<?php echo $class; ?>-price"><?php echo $price; ?></p><?php endif; ?>
        </div>
</div>
