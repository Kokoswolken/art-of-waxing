<?php
$id = get_the_ID();
$permalink = get_the_permalink();
$quote = get_field('quote') ?: get_field('quote', $id) ?: '';
$excerpt = get_field('excerpt') ?: get_field('excerpt', $id) ?: '';
$stars = get_field('stars') ?: get_field('stars', $id) ?: 5;
$classes = (is_post_type_archive()) ? 'col-sm-12 col-md-6 col-lg-3' : 'swiper-slide col-sm-12 col-md-6 col-lg-3';
?>

<div class="testimonial <?php echo $classes; ?>">
    <h3 class="quote"><?php echo $quote; ?></h3>
    <div class="review"><?php echo $excerpt; ?></div>

    <div class="stars">
        <?php for ($i = 1; $i <= $stars; $i++) : ?>
            <i></i>
        <?php endfor; ?>
    </div>
</div>
