<?php

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 ** @since Art of Waxing 1.0
 *
 ** @return void
 */

if ( ! function_exists( 'artofwaxing_theme_setup' ) ) {

    function artofwaxing_theme_setup() {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Twenty Twenty-One, use a find and replace
         * to change 'twentytwentyone' to the name of your theme in all the template files.
         */
        load_theme_textdomain( 'artofwaxing', get_template_directory() . '/languages' );

        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        /*
         * Let WordPress manage the document title.
         * This theme does not use a hard-coded <title> tag in the document head,
         * WordPress will provide it for us.
         */
        add_theme_support( 'title-tag' );

        /**
         * Add post-formats support.
         */
        add_theme_support(
            'post-formats',
            array(
                'link',
                'aside',
                'gallery',
                'image',
                'quote',
                'status',
                'video',
                'audio',
                'chat',
            )
        );

        /*
         * Enable SVG Support for Media Upload
         */

        function cc_mime_types($mimes) {
            $mimes['svg'] = 'image/svg+xml';
            return $mimes;
        }
        add_filter('upload_mimes', 'cc_mime_types');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support( 'post-thumbnails' );

        set_post_thumbnail_size( 1568, 9999 );

        register_nav_menus(
            array(
                'primary' => esc_html__( 'Primary menu', 'artofwaxing' ),
                'footer'  => __( 'Secondary menu', 'artofwaxing' ),
            )
        );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support(
            'html5',
            array(
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
                'style',
                'script',
                'navigation-widgets',
            )
        );

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        $logo_width  = 300;
        $logo_height = 100;

        add_theme_support(
            'custom-logo',
            array(
                'height'               => $logo_height,
                'width'                => $logo_width,
                'flex-width'           => true,
                'flex-height'          => true,
                'unlink-homepage-logo' => true,
            )
        );

        // Add theme support for selective refresh for widgets.
        add_theme_support( 'customize-selective-refresh-widgets' );

        // Add support for Block Styles.
//        add_theme_support( 'wp-block-styles' );

        // Add support for full and wide align images.
        add_theme_support( 'align-wide' );

        // Add support for editor styles.
        add_theme_support( 'editor-styles' );

        // Editor color palette.
        $darkestBrown   = '#776A61';
        $darkerBrown    = '#9F8366';
        $darkBrown      = '#C3A47F';
        $brown          = '#F6F1EB';
        $white          = '#FFFFFF';
        $green          = '#BDC885';
        $coral          = '#FA8E89';
        $darkGray       = '#424241';
        $gray           = '#7C7C7C';
        $lightGray      = '#E8E9ED';

        add_theme_support(
            'editor-color-palette',
            array(
                array(
                    'name'  => esc_html__( 'Darkest Brown', 'artofwaxing' ),
                    'slug'  => 'darkest-brown',
                    'color' => $darkestBrown,
                ),
                array(
                    'name'  => esc_html__( 'Darker Brown', 'artofwaxing' ),
                    'slug'  => 'darker-brown',
                    'color' => $darkerBrown,
                ),
                array(
                    'name'  => esc_html__( 'Dark Brown', 'artofwaxing' ),
                    'slug'  => 'dark-brown',
                    'color' => $darkBrown,
                ),
                array(
                    'name'  => esc_html__( 'Brown', 'artofwaxing' ),
                    'slug'  => 'brown',
                    'color' => $brown,
                ),
                array(
                    'name'  => esc_html__( 'White', 'artofwaxing' ),
                    'slug'  => 'white',
                    'color' => $white,
                ),
                array(
                    'name'  => esc_html__( 'Green', 'artofwaxing' ),
                    'slug'  => 'green',
                    'color' => $green,
                ),
                array(
                    'name'  => esc_html__( 'Coral', 'artofwaxing' ),
                    'slug'  => 'coral',
                    'color' => $coral,
                ),
                array(
                    'name'  => esc_html__( 'Dark Gray', 'artofwaxing' ),
                    'slug'  => 'dark-gray',
                    'color' => $darkGray,
                ),
                array(
                    'name'  => esc_html__( 'Gray', 'artofwaxing' ),
                    'slug'  => 'gray',
                    'color' => $gray,
                ),
                array(
                    'name'  => esc_html__( 'Light Gray', 'artofwaxing' ),
                    'slug'  => 'light-gray',
                    'color' => $lightGray,
                ),
            )
        );

        // Add support for responsive embedded content like youtube.
        add_theme_support( 'responsive-embeds' );

        // Add support for experimental link color control.
        add_theme_support( 'experimental-link-color' );

        // Add support for experimental cover block spacing.
        add_theme_support( 'custom-spacing' );

    }

    // Add theme (ACF) options
    if( function_exists('acf_add_options_page') ) {

        acf_add_options_page(array(
            'page_title' 	=> 'General',
            'menu_title'	=> 'Theme Options',
            'menu_slug' 	=> 'theme-general-settings',
            'capability'	=> 'edit_posts',
            'redirect'		=> false
        ));

        acf_add_options_sub_page(array(
            'page_title' 	=> 'Header',
            'menu_title'	=> 'Header',
            'parent_slug'	=> 'theme-general-settings',
        ));

        acf_add_options_sub_page(array(
            'page_title' 	=> 'Archive',
            'menu_title'	=> 'Archive',
            'parent_slug'	=> 'theme-general-settings',
        ));

        acf_add_options_sub_page(array(
            'page_title' 	=> 'Footer',
            'menu_title'	=> 'Footer',
            'parent_slug'	=> 'theme-general-settings',
        ));

    }

}

add_action( 'after_setup_theme', 'artofwaxing_theme_setup' );