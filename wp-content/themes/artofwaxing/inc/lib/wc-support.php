<?php


/**
 * Add WooCommerce Support
 *
 * @since Art of Waxing 1.0
 *
 * @return void
 */

//Add support for WooCommerce

if ( ! function_exists( 'add_woocommerce_support' ) ) {
    function add_woocommerce_support() {

        add_theme_support( 'woocommerce', array(
                'thumbnail_image_width' => 800,
                'single_image_width'    => 800,
                'gallery_thumbnail_image_width' => 800,

                'product_grid'          => array(
                    'default_rows'    => 3,
                    'min_rows'        => 2,
                    'max_rows'        => 8,
                    'default_columns' => 4,
                    'min_columns'     => 2,
                    'max_columns'     => 5,
                ),
            )
        );

        add_theme_support( 'wc-product-gallery-zoom' );
        add_theme_support( 'wc-product-gallery-lightbox' );
        add_theme_support( 'wc-product-gallery-slider' );
//        add_filter( 'woocommerce_enqueue_styles', '__return_false' );

    }
}

add_action( 'after_setup_theme', 'add_woocommerce_support' );


//Add support for Gutenberg block editor & fallback for Classic editor
if ( ! function_exists( 'add_gutenberg_product' ) ) {
    function add_gutenberg_product( $can_edit, $post_type ) {
        $disable = in_array( 'classic-editor', array_keys( $_GET ) );
        if ( $post_type == 'product' && $disable != true ) {
            $can_edit = true;
        }
        return $can_edit;
    }
}

//add_filter( 'use_block_editor_for_post_type', 'add_gutenberg_product', 10, 2 );

//Add support for taxonomy with Gutenberg activated
if ( ! function_exists( 'enable_taxonomy_product' ) ) {

    function enable_taxonomy_product( $args ) {
        $args['show_in_rest'] = true;
        return $args;
    }
}

//add_filter( 'woocommerce_taxonomy_args_product_cat', 'enable_taxonomy_product' );
//add_filter( 'woocommerce_taxonomy_args_product_tag', 'enable_taxonomy_product' );


//Add register catalog boxes
if ( ! function_exists( 'register_catalog_meta_boxes' ) ) {
    function register_catalog_meta_boxes() {
        global $current_screen;
        // Make sure gutenberg is loaded before adding the metabox
        if ( method_exists( $current_screen, 'is_block_editor' ) && $current_screen->is_block_editor() ) {
            add_meta_box( 'catalog-visibility', __( 'Catalog visibility', 'textdomain' ), 'product_data_visibility', 'product', 'side' );
        }
    }
}

//add_action( 'add_meta_boxes', 'register_catalog_meta_boxes' );

//Show catalog boxes
if ( ! function_exists( 'product_data_visibility' ) ) {

    function product_data_visibility( $post ) {

        $thepostid          = $post->ID;
        $product_object     = $thepostid ? wc_get_product( $thepostid ) : new WC_Product();
        $current_visibility = $product_object->get_catalog_visibility();
        $current_featured   = wc_bool_to_string( $product_object->get_featured() );
        $visibility_options = wc_get_product_visibility_options();
        ?>
        <div class="misc-pub-section" id="catalog-visibility">
            <?php esc_html_e( 'Catalog visibility:', 'woocommerce' ); ?>
            <strong id="catalog-visibility-display">
                <?php

                echo isset( $visibility_options[ $current_visibility ] ) ? esc_html( $visibility_options[ $current_visibility ] ) : esc_html( $current_visibility );

                if ( 'yes' === $current_featured ) {
                    echo ', ' . esc_html__( 'Featured', 'woocommerce' );
                }
                ?>
            </strong>

            <a href="#catalog-visibility"
               class="edit-catalog-visibility hide-if-no-js"><?php esc_html_e( 'Edit', 'woocommerce' ); ?></a>

            <div id="catalog-visibility-select" class="hide-if-js">

                <input type="hidden" name="current_visibility" id="current_visibility"
                       value="<?php echo esc_attr( $current_visibility ); ?>" />
                <input type="hidden" name="current_featured" id="current_featured"
                       value="<?php echo esc_attr( $current_featured ); ?>" />

                <?php
                echo '<p>' . esc_html__( 'This setting determines which shop pages products will be listed on.', 'woocommerce' ) . '</p>';

                foreach ( $visibility_options as $name => $label ) {
                    echo '<input type="radio" name="_visibility" id="_visibility_' . esc_attr( $name ) . '" value="' . esc_attr( $name ) . '" ' . checked( $current_visibility, $name, false ) . ' data-label="' . esc_attr( $label ) . '" /> <label for="_visibility_' . esc_attr( $name ) . '" class="selectit">' . esc_html( $label ) . '</label><br />';
                }

                echo '<br /><input type="checkbox" name="_featured" id="_featured" ' . checked( $current_featured, 'yes', false ) . ' /> <label for="_featured">' . esc_html__( 'This is a featured product', 'woocommerce' ) . '</label><br />';
                ?>
                <p>
                    <a href="#catalog-visibility"
                       class="save-post-visibility hide-if-no-js button"><?php esc_html_e( 'OK', 'woocommerce' ); ?></a>
                    <a href="#catalog-visibility"
                       class="cancel-post-visibility hide-if-no-js"><?php esc_html_e( 'Cancel', 'woocommerce' ); ?></a>
                </p>
            </div>
        </div>
        <?php
    }
}



