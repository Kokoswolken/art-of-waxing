<?php
if (!function_exists('create_post_type')) :

    function create_post_type()
    {
        // CPT Options
        $labels = array(
            'name' => __('Testimonials'),
            'singular_name' => __('Testimonial'),
            'add_new'                   => 'Nieuwe testimonial'
        );

        $args = array(
            'label' => __('Testimonials', 'artofwaxing'),
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'has_archive' => true,
            'exclude_from_search' => true,
            'rewrite' => array('slug' => 'testimonials'),
            'show_in_nav_menus' => true,
            'supports' => array('title', 'editor'),
            'show_in_rest' => true,
        );

        register_post_type('testimonials', $args);

        // CPT Options
        $labelsTreatments = array(
            'name' => __('Behandelingen'),
            'singular_name' => __('Behandeling'),
            'add_new'                   => 'Nieuwe Behandeling'
        );

        $argsTreatments = array(
            'label' => __('Treatments', 'artofwaxing'),
            'labels' => $labelsTreatments,
            'public' => true,
            'publicly_queryable' => false,
            'exclude_from_search' => true,
            'has_archive' => false,
            'taxonomies'  => array( 'category' ),
            'rewrite' => array('slug' => 'behandelingen'),
            'show_in_nav_menus' => false,
            'supports' => array('title', 'editor', 'thumbnail'),
            'show_in_rest' => true,
        );

        register_post_type('treatments', $argsTreatments);
    }

endif;

add_action('init', 'create_post_type');