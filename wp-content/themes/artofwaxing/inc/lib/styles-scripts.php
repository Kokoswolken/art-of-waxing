<?php

/**
 * Enqueue scripts and styles.
 *
 * @since Art of Waxing 1.0
 *
 * @return void
 */

if ( ! function_exists( 'art_of_waxing_scripts' ) ) {
    function art_of_waxing_scripts()
    {
        // Enqueue styles
//        wp_enqueue_style('aow-ext', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css', 99999, wp_get_theme()->get('Version'));
//        wp_enqueue_script( 'aow-ext', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js', array(), wp_get_theme()->get( 'Version' ), false );
        wp_enqueue_script( 'aow-ext', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js', array(), wp_get_theme()->get( 'Version' ), false );
//        wp_enqueue_style( 'aow-ext', 'https://unpkg.com/swiper/swiper-bundle.min.css', array(), wp_get_theme()->get( 'Version' ), false );
//        wp_enqueue_script( 'aow-ext', 'https://unpkg.com/swiper/swiper-bundle.min.js', array(), wp_get_theme()->get( 'Version' ), false );

        wp_enqueue_style('aow-main', get_template_directory_uri() . '/assets/dist/css/main.css', 99999, wp_get_theme()->get('Version'));
        wp_enqueue_style('aow-main', get_template_directory_uri() . '/assets/dist/css/critical.css', 99999, wp_get_theme()->get('Version'));
        wp_enqueue_script('aow-main', get_template_directory_uri() . '/assets/dist/js/critical.js', wp_get_theme()->get('Version'));
      	wp_enqueue_script( 'aow-main', get_template_directory_uri() .( '/assets/dist/js/main.js' ), array(), wp_get_theme()->get( 'Version' ), false );

    }
}

add_action( 'wp_enqueue_scripts', 'art_of_waxing_scripts' );

if ( ! function_exists( 'art_of_waxing_admin_style' ) ) {

    function art_of_waxing_admin_style()
    {
        wp_register_style('aow-admin-style', get_template_directory_uri() . '/assets/dist/css/main.css', false, '1.0.0');
        wp_enqueue_style('aow-admin-style');
    }
}
//add_action( 'admin_enqueue_scripts', 'art_of_waxing_admin_style' );