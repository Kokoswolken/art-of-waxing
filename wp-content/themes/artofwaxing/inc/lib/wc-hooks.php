<?php
/**
 * Change 'add to cart' text on single product page and product archives
 */

if (!function_exists('woocommerce_custom_product_add_to_cart_text')) :

    add_filter( 'woocommerce_product_add_to_cart_text', 'woocommerce_custom_product_add_to_cart_text' );
    function woocommerce_custom_product_add_to_cart_text() {
        return __( '', 'woocommerce' );
    }
endif;

/**
 * Change order of Single Price
 */

add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 29 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );

/**
 * Add a custom product data tab
 */

add_filter( 'woocommerce_product_tabs', 'wc_my_experience_tab' );
if (!function_exists('wc_my_experience_tab')) :

    function wc_my_experience_tab($tabs)
    {
        if (get_field('personalExperience')) :

            $tabs['my_experience'] = array(
                'title' => __('Mijn Ervaring', 'woocommerce'),
                'priority' => 20,
                'callback' => 'getExperienceTabContent'
            );

        endif;
        return $tabs;

    }
endif;

/**
 * Add product brand
 */
add_filter( 'woocommerce_single_product_summary', 'wc_add_product_brand', 1 );
if (!function_exists('wc_add_product_brand')) :

    function wc_add_product_brand()
    {
        if (get_field('product_brand')) :
            echo '<p class="product-brand">'.get_field('product_brand').'</p>';

        endif;
    }
endif;

/**
 * Add sticker from Owner's name to every experience on the tab of WooCommerce
 */
if (!function_exists('getExperienceTabContent')) :
    function getExperienceTabContent()
    {
        if(get_field('personalExperience') && get_field('signature', 'option')) :
            echo get_field('personalExperience');
            echo '<span class="signature">' . get_field('signature', 'option') . '</span>';
        endif;
    }
endif;

/**
 * Add container around WooCommerce Shop Loop
 */
if(!function_exists('action_woocommerce_before_shop_loop_item')) :
    function action_woocommerce_before_shop_loop_item( ) {
        echo "<div class='container archive-products'>";
    }
    function action_woocommerce_after_shop_loop_item( ) {
        echo "</div>";
    }
endif;
add_action('woocommerce_before_shop_loop', 'action_woocommerce_before_shop_loop_item', 10, 0);
add_action('woocommerce_after_shop_loop', 'action_woocommerce_after_shop_loop_item', 10, 0);


/**
 * Add tags on WooCommerce Archive page
 */
if(!function_exists('wc_add_tags_to_archive')) :
    function wc_add_tags_to_archive( ) {
        $current_tags = get_the_terms( get_the_ID(), 'product_tag' );

        //only start if we have some tags
        if ( $current_tags && ! is_wp_error( $current_tags ) ) {

//            create a list to hold our tags
            echo '<ul class="product_tags">';

            //for each tag we create a list item
            foreach ($current_tags as $tag) {

                $tag_title = $tag->name; // tag name
                $tag_link = get_term_link( $tag );// tag archive link

//                var_dump($tag_title);
//                var_dump($tag_link);
                echo '<li><a href="'.$tag_link.'">'.$tag_title.'</a></li>';
            }

            echo '</ul>';
//            addTags();
        }
    }
endif;
add_action('woocommerce_after_shop_loop_item_title', 'wc_add_tags_to_archive', 10, 0);


/**
 * Add Flexible block to Archive Page
 */
//Add if post type is archive page
if(!function_exists('wc_add_flexible_block')) :

    function wc_add_flexible_block( ) {
        if(is_archive()) :
            include( plugin_dir_path( __FILE__ ) . '../../template-parts/block/content-archive-header.php');
        endif;
    }

endif;
add_action('woocommerce_before_main_content', 'wc_add_flexible_block', 10, 0);
//add_action('woocommerce_after_shop_loop', 'action_woocommerce_after_shop_loop_item', 10, 0);


//


/**
 *  Display the Woocommerce Discount Percentage on the Sale Badge for variable products and single products
 */
if (!function_exists('display_percentage_on_sale_badge')) :
    function display_percentage_on_sale_badge($html, $post, $product)
    {

        if ($product->is_type('variable')) {
            $percentages = array();

            // This will get all the variation prices and loop throughout them
            $prices = $product->get_variation_prices();

            foreach ($prices['price'] as $key => $price) {
                // Only on sale variations
                if ($prices['regular_price'][$key] !== $price) {
                    // Calculate and set in the array the percentage for each variation on sale
                    $percentages[] = round(100 - (floatval($prices['sale_price'][$key]) / floatval($prices['regular_price'][$key]) * 100));
                }
            }
            // Displays maximum discount value
            $percentage = max($percentages) . '%';

        } elseif ($product->is_type('grouped')) {
            $percentages = array();

            // This will get all the variation prices and loop throughout them
            $children_ids = $product->get_children();

            foreach ($children_ids as $child_id) {
                $child_product = wc_get_product($child_id);

                $regular_price = (float)$child_product->get_regular_price();
                $sale_price = (float)$child_product->get_sale_price();

                if ($sale_price != 0 || !empty($sale_price)) {
                    // Calculate and set in the array the percentage for each child on sale
                    $percentages[] = round(100 - ($sale_price / $regular_price * 100));
                }
            }
            // Displays maximum discount value
            $percentage = max($percentages) . '%';

        } else {
            $regular_price = (float)$product->get_regular_price();
            $sale_price = (float)$product->get_sale_price();

            if ($sale_price != 0 || !empty($sale_price)) {
                $percentage = round(100 - ($sale_price / $regular_price * 100)) . '%';
            } else {
                return $html;
            }
        }
        return '<span class="onsale">' . esc_html__('-', 'woocommerce') . ' ' . $percentage . '</span>'; // If needed then change or remove "up to -" text
    }
endif;

add_filter( 'woocommerce_sale_flash', 'display_percentage_on_sale_badge', 20, 3 );