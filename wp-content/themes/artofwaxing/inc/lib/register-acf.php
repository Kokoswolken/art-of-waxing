<?php

/**
 * Reqister ACF Gutenberg Blocks
 *
 * @since Art of Waxing 1.0
 *
 * @return void
 */

add_action('acf/init', 'my_acf_init');
function my_acf_init() {

    // check function exists
    if( function_exists('acf_register_block') ) {

        // register a hero block
        acf_register_block(array(
            'name'				=> 'hero',
            'title'				=> __('Hero'),
            'description'		=> __('A custom hero block.'),
            'render_callback'	=> 'my_acf_block_render_callback',
            'category'			=> 'fb-custom-cat',
            'enqueue_assets'    => '/assets/dist/css/main.css',
            'supports'          => array(
                                    'align' => array( 'full' ),
                                    ),
            'icon'				=> 'heading',
            'keywords'			=> array( 'hero', 'banner', 'header' ),
        ));

        //register best products block
        acf_register_block(array(
            'name'				=> 'best-products',
            'title'				=> __('Best Products'),
            'description'		=> __('A custom best products block.'),
            'render_callback'	=> 'my_acf_block_render_callback',
            'category'			=> 'fb-custom-cat',
            'enqueue_assets'    => '/assets/dist/css/main.css',
            'supports'          => array(
                'align' => array( 'full' ),
            ),
            'icon'				=> 'image-filter',
            'keywords'			=> array( 'best products', 'art of waxing', 'product', 'meld je aan' ),
        ));

        //register appointment cta block
        acf_register_block(array(
            'name'				=> 'appointment',
            'title'				=> __('CTA Appointment'),
            'description'		=> __('A custom CTA Appointment block.'),
            'render_callback'	=> 'my_acf_block_render_callback',
            'category'			=> 'fb-custom-cat',
            'enqueue_assets'    => '/assets/dist/css/main.css',
            'supports'          => array(
                'align' => array( 'full' ),
            ),
            'icon'				=> 'calendar-alt',
            'keywords'			=> array( 'cta', 'appointment', 'afspraak', 'content' ),
        ));

        //register about us block
        acf_register_block(array(
            'name'				=> 'about-us',
            'title'				=> __('About us'),
            'description'		=> __('A custom About us block.'),
            'render_callback'	=> 'my_acf_block_render_callback',
            'category'			=> 'fb-custom-cat',
            'enqueue_assets'    => '/assets/dist/css/main.css',
            'supports'          => array(
                'align' => array( 'full' ),
            ),
            'icon'				=> 'admin-users',
            'keywords'			=> array( 'about us', 'lieke', 'over mij', 'afbeelding', 'content' ),
        ));

        //register testimonials block
        acf_register_block(array(
            'name'				=> 'testimonials',
            'title'				=> __('Testimonials'),
            'description'		=> __('Testimonials'),
            'render_callback'	=> 'my_acf_block_render_callback',
            'category'			=> 'fb-custom-cat',
            'enqueue_assets'    => '/assets/dist/css/main.css',
            'supports'          => array(
                'align' => array( 'full' ),
            ),
            'icon'				=> 'testimonial',
            'keywords'			=> array( 'testimonials', 'review', 'stars', 'cpt', 'slider' ),
        ));

        // register a Treatment block
        acf_register_block(array(
            'name'				=> 'treatment',
            'title'				=> __('Behandeling'),
            'description'		=> __('Uitgebreide weergave prijslijst'),
            'render_callback'	=> 'my_acf_block_render_callback',
            'category'			=> 'fb-custom-cat',
            'enqueue_assets'    => '/assets/dist/css/main.css',
            'supports'          => array(
                'align' => array( 'full' ),
            ),
            'icon'				=> 'money-alt',
            'keywords'			=> array( 'behandeling', 'prijslijst', 'afbeelding' ),
        ));

        // register wc archive block
        acf_register_block(array(
            'name'				=> 'archive-header',
            'title'				=> __('Archive Header'),
            'description'		=> __('A custom archive header block.'),
            'render_callback'	=> 'my_acf_block_render_callback',
            'category'			=> 'fb-custom-cat',
            'enqueue_assets'    => '/assets/dist/css/main.css',
            'supports'          => array(
                'align' => array( 'full' ),
            ),
            'icon'				=> 'heading',
            'keywords'			=> array( 'wc', 'navigation', 'slider', 'custom', 'choices', 'buttons' ),
        ));

        // register wc archive block
        acf_register_block(array(
            'name'				=> 'wc-latest-products',
            'title'				=> __('Latest Products'),
            'description'		=> __('A custom latest products block.'),
            'render_callback'	=> 'my_acf_block_render_callback',
            'category'			=> 'fb-custom-cat',
            'enqueue_assets'    => '/assets/dist/css/main.css',
            'supports'          => array(
                'align' => array( 'full' ),
            ),
            'icon'				=> 'image-filter',
            'keywords'			=> array( 'wc', 'latest products', 'product', 'custom', 'webshop' ),
        ));
    }
}

function add_block_category( $categories, $post ) {
    return array_merge(
        $categories,
        array(
            array(
                'slug' => 'fb-custom-cat',
                'title' => __( 'Art of Waxing', 'fb-blocks' ),
                'icon'  => null,
            ),
        )
    );
}

add_filter( 'block_categories_all', 'add_block_category', 10, 2 );

function my_acf_block_render_callback( $block ) {

    // convert name ("acf/testimonial") into path friendly slug ("testimonial")
    $slug = str_replace('acf/', '', $block['name']);

    // include a template part from within the "template-parts/block" folder
    if( file_exists( get_theme_file_path("/template-parts/block/content-{$slug}.php") ) ) {
        include( get_theme_file_path("/template-parts/block/content-{$slug}.php") );
    }
}