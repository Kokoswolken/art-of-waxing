<?php

/**
 * Reqister WP Block Patterns
 *
 * @since Art of Waxing 1.0
 *
 * @return void
 */

function register_wp_pattern() {
    register_block_pattern(
        'aow',
        array(
            'title'       => __( 'Hero', 'aow' ),
            'description' => _x( 'Hero', 'Hero block of the site', 'aow' ),
            'categories'  => array('Art of Waxing'),
            'content'     => '<!-- wp:acf/hero {"id":"block_60d8755460bf7","name":"acf/hero","data":{"field_60d873b4300c7":"Love for Brazilian","field_60d873c7300c8":"Onthul een zachte en gladde huid","field_60d873d0300c9":"Wij bieden professionele wax behandelingen en kwalitatieve wax producten dat uit 98% natuurlijke ingrediënten bestaan.","field_60d87412300ca":{"title":"Ga naar de webshop","url":"http://artofwaxing.test/webshop/","target":""},"field_60d8742d300cb":{"title":"Maak een afspraak","url":"http://artofwaxing.test/contact/","target":"_blank"}},"align":"","mode":"preview"} /-->',
        )
    );
}

add_action( 'init', 'register_wp_pattern' );

function register_wp_pattern_category() {
    register_block_pattern_category(
        'Art of Waxing',
        array( 'label' => __( 'Art of Waxing', 'aow' ) )
    );
}

add_action( 'init', 'register_wp_pattern_category' );
