<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.8.0
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 *
 * @see woocommerce_default_product_tabs()
 */
$product_tabs = apply_filters('woocommerce_product_tabs', array());


if (!empty($product_tabs)) : ?>
    <div class="container tabs-and-upsells">
        <div class="row">
            <div class="col-sm-12 col-lg-6 tabs">
                <div class="woocommerce-tabs wc-tabs-wrapper accordion" id="accordionPanels">
                    <?php foreach ($product_tabs as $key => $product_tab) : ?>
                        <div class="accordion-item tab">

                            <h3 class="accordion-header tab-header" id="panel-headingOne-<?php echo $key; ?>">
                                <button class="tab-button accordion-button" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapse-<?php echo $key; ?>" aria-expanded="true"
                                        aria-controls="collapse-<?php echo $key; ?>">
                                    <?php echo wp_kses_post(apply_filters('woocommerce_product_' . $key . '_tab_title', $product_tab['title'], $key)); ?>
                                </button>
                            </h3>

                            <div id="collapse-<?php echo $key; ?>" class="accordion-collapse collapse tab-description"
                                 aria-labelledby="collapse-<?php echo $key; ?>">
                                <div class="accordion-body tab-body">
                                    <?php
                                    if (isset($product_tab['callback'])) {
                                        call_user_func($product_tab['callback'], $key, $product_tab);
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                    <?php endforeach; ?>

                    <?php do_action('woocommerce_product_after_tabs'); ?>
                </div>
            </div>
            <div class="col-sm-12 col-lg-6 upsells">
                <?php woocommerce_upsell_display(4, 1); ?>
            </div>
        </div>
    </div>
<?php endif; ?>
